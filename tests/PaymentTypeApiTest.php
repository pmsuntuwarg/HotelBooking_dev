<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PaymentTypeApiTest extends TestCase
{
    use MakePaymentTypeTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreatePaymentType()
    {
        $paymentType = $this->fakePaymentTypeData();
        $this->json('POST', '/api/v1/paymentTypes', $paymentType);

        $this->assertApiResponse($paymentType);
    }

    /**
     * @test
     */
    public function testReadPaymentType()
    {
        $paymentType = $this->makePaymentType();
        $this->json('GET', '/api/v1/paymentTypes/'.$paymentType->ptPaymentTypeID);

        $this->assertApiResponse($paymentType->toArray());
    }

    /**
     * @test
     */
    public function testUpdatePaymentType()
    {
        $paymentType = $this->makePaymentType();
        $editedPaymentType = $this->fakePaymentTypeData();

        $this->json('PUT', '/api/v1/paymentTypes/'.$paymentType->ptPaymentTypeID, $editedPaymentType);

        $this->assertApiResponse($editedPaymentType);
    }

    /**
     * @test
     */
    public function testDeletePaymentType()
    {
        $paymentType = $this->makePaymentType();
        $this->json('DELETE', '/api/v1/paymentTypes/'.$paymentType->ptPaymentTypeID);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/paymentTypes/'.$paymentType->ptPaymentTypeID);

        $this->assertResponseStatus(404);
    }
}
