<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class StaffApiTest extends TestCase
{
    use MakeStaffTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateStaff()
    {
        $staff = $this->fakeStaffData();
        $this->json('POST', '/api/v1/staff', $staff);

        $this->assertApiResponse($staff);
    }

    /**
     * @test
     */
    public function testReadStaff()
    {
        $staff = $this->makeStaff();
        $this->json('GET', '/api/v1/staff/'.$staff->id);

        $this->assertApiResponse($staff->toArray());
    }

    /**
     * @test
     */
    public function testUpdateStaff()
    {
        $staff = $this->makeStaff();
        $editedStaff = $this->fakeStaffData();

        $this->json('PUT', '/api/v1/staff/'.$staff->id, $editedStaff);

        $this->assertApiResponse($editedStaff);
    }

    /**
     * @test
     */
    public function testDeleteStaff()
    {
        $staff = $this->makeStaff();
        $this->json('DELETE', '/api/v1/staff/'.$staff->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/staff/'.$staff->id);

        $this->assertResponseStatus(404);
    }
}
