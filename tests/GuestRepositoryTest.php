<?php

use App\Models\Admin\Guest;
use App\Repositories\Admin\GuestRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class GuestRepositoryTest extends TestCase
{
    use MakeGuestTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var GuestRepository
     */
    protected $guestRepo;

    public function setUp()
    {
        parent::setUp();
        $this->guestRepo = App::make(GuestRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateGuest()
    {
        $guest = $this->fakeGuestData();
        $createdGuest = $this->guestRepo->create($guest);
        $createdGuest = $createdGuest->toArray();
        $this->assertArrayHasKey('id', $createdGuest);
        $this->assertNotNull($createdGuest['id'], 'Created Guest must have id specified');
        $this->assertNotNull(Guest::find($createdGuest['id']), 'Guest with given id must be in DB');
        $this->assertModelData($guest, $createdGuest);
    }

    /**
     * @test read
     */
    public function testReadGuest()
    {
        $guest = $this->makeGuest();
        $dbGuest = $this->guestRepo->find($guest->gGuestID);
        $dbGuest = $dbGuest->toArray();
        $this->assertModelData($guest->toArray(), $dbGuest);
    }

    /**
     * @test update
     */
    public function testUpdateGuest()
    {
        $guest = $this->makeGuest();
        $fakeGuest = $this->fakeGuestData();
        $updatedGuest = $this->guestRepo->update($fakeGuest, $guest->gGuestID);
        $this->assertModelData($fakeGuest, $updatedGuest->toArray());
        $dbGuest = $this->guestRepo->find($guest->gGuestID);
        $this->assertModelData($fakeGuest, $dbGuest->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteGuest()
    {
        $guest = $this->makeGuest();
        $resp = $this->guestRepo->delete($guest->gGuestID);
        $this->assertTrue($resp);
        $this->assertNull(Guest::find($guest->gGuestID), 'Guest should not exist in DB');
    }
}
