<?php

use App\Models\Admin\BookingStatus;
use App\Repositories\Admin\BookingStatusRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class BookingStatusRepositoryTest extends TestCase
{
    use MakeBookingStatusTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var BookingStatusRepository
     */
    protected $bookingStatusRepo;

    public function setUp()
    {
        parent::setUp();
        $this->bookingStatusRepo = App::make(BookingStatusRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateBookingStatus()
    {
        $bookingStatus = $this->fakeBookingStatusData();
        $createdBookingStatus = $this->bookingStatusRepo->create($bookingStatus);
        $createdBookingStatus = $createdBookingStatus->toArray();
        $this->assertArrayHasKey('id', $createdBookingStatus);
        $this->assertNotNull($createdBookingStatus['id'], 'Created BookingStatus must have id specified');
        $this->assertNotNull(BookingStatus::find($createdBookingStatus['id']), 'BookingStatus with given id must be in DB');
        $this->assertModelData($bookingStatus, $createdBookingStatus);
    }

    /**
     * @test read
     */
    public function testReadBookingStatus()
    {
        $bookingStatus = $this->makeBookingStatus();
        $dbBookingStatus = $this->bookingStatusRepo->find($bookingStatus->bsBokingStatusID);
        $dbBookingStatus = $dbBookingStatus->toArray();
        $this->assertModelData($bookingStatus->toArray(), $dbBookingStatus);
    }

    /**
     * @test update
     */
    public function testUpdateBookingStatus()
    {
        $bookingStatus = $this->makeBookingStatus();
        $fakeBookingStatus = $this->fakeBookingStatusData();
        $updatedBookingStatus = $this->bookingStatusRepo->update($fakeBookingStatus, $bookingStatus->bsBokingStatusID);
        $this->assertModelData($fakeBookingStatus, $updatedBookingStatus->toArray());
        $dbBookingStatus = $this->bookingStatusRepo->find($bookingStatus->bsBokingStatusID);
        $this->assertModelData($fakeBookingStatus, $dbBookingStatus->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteBookingStatus()
    {
        $bookingStatus = $this->makeBookingStatus();
        $resp = $this->bookingStatusRepo->delete($bookingStatus->bsBokingStatusID);
        $this->assertTrue($resp);
        $this->assertNull(BookingStatus::find($bookingStatus->bsBokingStatusID), 'BookingStatus should not exist in DB');
    }
}
