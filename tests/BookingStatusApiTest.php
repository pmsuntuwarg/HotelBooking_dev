<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class BookingStatusApiTest extends TestCase
{
    use MakeBookingStatusTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateBookingStatus()
    {
        $bookingStatus = $this->fakeBookingStatusData();
        $this->json('POST', '/api/v1/bookingStatuses', $bookingStatus);

        $this->assertApiResponse($bookingStatus);
    }

    /**
     * @test
     */
    public function testReadBookingStatus()
    {
        $bookingStatus = $this->makeBookingStatus();
        $this->json('GET', '/api/v1/bookingStatuses/'.$bookingStatus->bsBokingStatusID);

        $this->assertApiResponse($bookingStatus->toArray());
    }

    /**
     * @test
     */
    public function testUpdateBookingStatus()
    {
        $bookingStatus = $this->makeBookingStatus();
        $editedBookingStatus = $this->fakeBookingStatusData();

        $this->json('PUT', '/api/v1/bookingStatuses/'.$bookingStatus->bsBokingStatusID, $editedBookingStatus);

        $this->assertApiResponse($editedBookingStatus);
    }

    /**
     * @test
     */
    public function testDeleteBookingStatus()
    {
        $bookingStatus = $this->makeBookingStatus();
        $this->json('DELETE', '/api/v1/bookingStatuses/'.$bookingStatus->bsBokingStatusID);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/bookingStatuses/'.$bookingStatus->bsBokingStatusID);

        $this->assertResponseStatus(404);
    }
}
