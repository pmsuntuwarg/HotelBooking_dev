<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ReservationAgentApiTest extends TestCase
{
    use MakeReservationAgentTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateReservationAgent()
    {
        $reservationAgent = $this->fakeReservationAgentData();
        $this->json('POST', '/api/v1/reservationAgents', $reservationAgent);

        $this->assertApiResponse($reservationAgent);
    }

    /**
     * @test
     */
    public function testReadReservationAgent()
    {
        $reservationAgent = $this->makeReservationAgent();
        $this->json('GET', '/api/v1/reservationAgents/'.$reservationAgent->raReservationAgentID);

        $this->assertApiResponse($reservationAgent->toArray());
    }

    /**
     * @test
     */
    public function testUpdateReservationAgent()
    {
        $reservationAgent = $this->makeReservationAgent();
        $editedReservationAgent = $this->fakeReservationAgentData();

        $this->json('PUT', '/api/v1/reservationAgents/'.$reservationAgent->raReservationAgentID, $editedReservationAgent);

        $this->assertApiResponse($editedReservationAgent);
    }

    /**
     * @test
     */
    public function testDeleteReservationAgent()
    {
        $reservationAgent = $this->makeReservationAgent();
        $this->json('DELETE', '/api/v1/reservationAgents/'.$reservationAgent->raReservationAgentID);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/reservationAgents/'.$reservationAgent->raReservationAgentID);

        $this->assertResponseStatus(404);
    }
}
