<?php

use App\Models\Admin\Staff;
use App\Repositories\Admin\StaffRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class StaffRepositoryTest extends TestCase
{
    use MakeStaffTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var StaffRepository
     */
    protected $staffRepo;

    public function setUp()
    {
        parent::setUp();
        $this->staffRepo = App::make(StaffRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateStaff()
    {
        $staff = $this->fakeStaffData();
        $createdStaff = $this->staffRepo->create($staff);
        $createdStaff = $createdStaff->toArray();
        $this->assertArrayHasKey('id', $createdStaff);
        $this->assertNotNull($createdStaff['id'], 'Created Staff must have id specified');
        $this->assertNotNull(Staff::find($createdStaff['id']), 'Staff with given id must be in DB');
        $this->assertModelData($staff, $createdStaff);
    }

    /**
     * @test read
     */
    public function testReadStaff()
    {
        $staff = $this->makeStaff();
        $dbStaff = $this->staffRepo->find($staff->id);
        $dbStaff = $dbStaff->toArray();
        $this->assertModelData($staff->toArray(), $dbStaff);
    }

    /**
     * @test update
     */
    public function testUpdateStaff()
    {
        $staff = $this->makeStaff();
        $fakeStaff = $this->fakeStaffData();
        $updatedStaff = $this->staffRepo->update($fakeStaff, $staff->id);
        $this->assertModelData($fakeStaff, $updatedStaff->toArray());
        $dbStaff = $this->staffRepo->find($staff->id);
        $this->assertModelData($fakeStaff, $dbStaff->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteStaff()
    {
        $staff = $this->makeStaff();
        $resp = $this->staffRepo->delete($staff->id);
        $this->assertTrue($resp);
        $this->assertNull(Staff::find($staff->id), 'Staff should not exist in DB');
    }
}
