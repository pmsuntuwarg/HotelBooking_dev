<?php

use App\Models\Admin\RateType;
use App\Repositories\Admin\RateTypeRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class RateTypeRepositoryTest extends TestCase
{
    use MakeRateTypeTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var RateTypeRepository
     */
    protected $rateTypeRepo;

    public function setUp()
    {
        parent::setUp();
        $this->rateTypeRepo = App::make(RateTypeRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateRateType()
    {
        $rateType = $this->fakeRateTypeData();
        $createdRateType = $this->rateTypeRepo->create($rateType);
        $createdRateType = $createdRateType->toArray();
        $this->assertArrayHasKey('id', $createdRateType);
        $this->assertNotNull($createdRateType['id'], 'Created RateType must have id specified');
        $this->assertNotNull(RateType::find($createdRateType['id']), 'RateType with given id must be in DB');
        $this->assertModelData($rateType, $createdRateType);
    }

    /**
     * @test read
     */
    public function testReadRateType()
    {
        $rateType = $this->makeRateType();
        $dbRateType = $this->rateTypeRepo->find($rateType->rtRateTypeTypeID);
        $dbRateType = $dbRateType->toArray();
        $this->assertModelData($rateType->toArray(), $dbRateType);
    }

    /**
     * @test update
     */
    public function testUpdateRateType()
    {
        $rateType = $this->makeRateType();
        $fakeRateType = $this->fakeRateTypeData();
        $updatedRateType = $this->rateTypeRepo->update($fakeRateType, $rateType->rtRateTypeTypeID);
        $this->assertModelData($fakeRateType, $updatedRateType->toArray());
        $dbRateType = $this->rateTypeRepo->find($rateType->rtRateTypeTypeID);
        $this->assertModelData($fakeRateType, $dbRateType->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteRateType()
    {
        $rateType = $this->makeRateType();
        $resp = $this->rateTypeRepo->delete($rateType->rtRateTypeTypeID);
        $this->assertTrue($resp);
        $this->assertNull(RateType::find($rateType->rtRateTypeTypeID), 'RateType should not exist in DB');
    }
}
