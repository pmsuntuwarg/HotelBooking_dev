<?php

use App\Models\Admin\RoomBooked;
use App\Repositories\Admin\RoomBookedRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class RoomBookedRepositoryTest extends TestCase
{
    use MakeRoomBookedTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var RoomBookedRepository
     */
    protected $roomBookedRepo;

    public function setUp()
    {
        parent::setUp();
        $this->roomBookedRepo = App::make(RoomBookedRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateRoomBooked()
    {
        $roomBooked = $this->fakeRoomBookedData();
        $createdRoomBooked = $this->roomBookedRepo->create($roomBooked);
        $createdRoomBooked = $createdRoomBooked->toArray();
        $this->assertArrayHasKey('id', $createdRoomBooked);
        $this->assertNotNull($createdRoomBooked['id'], 'Created RoomBooked must have id specified');
        $this->assertNotNull(RoomBooked::find($createdRoomBooked['id']), 'RoomBooked with given id must be in DB');
        $this->assertModelData($roomBooked, $createdRoomBooked);
    }

    /**
     * @test read
     */
    public function testReadRoomBooked()
    {
        $roomBooked = $this->makeRoomBooked();
        $dbRoomBooked = $this->roomBookedRepo->find($roomBooked->rbRoomBookedID);
        $dbRoomBooked = $dbRoomBooked->toArray();
        $this->assertModelData($roomBooked->toArray(), $dbRoomBooked);
    }

    /**
     * @test update
     */
    public function testUpdateRoomBooked()
    {
        $roomBooked = $this->makeRoomBooked();
        $fakeRoomBooked = $this->fakeRoomBookedData();
        $updatedRoomBooked = $this->roomBookedRepo->update($fakeRoomBooked, $roomBooked->rbRoomBookedID);
        $this->assertModelData($fakeRoomBooked, $updatedRoomBooked->toArray());
        $dbRoomBooked = $this->roomBookedRepo->find($roomBooked->rbRoomBookedID);
        $this->assertModelData($fakeRoomBooked, $dbRoomBooked->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteRoomBooked()
    {
        $roomBooked = $this->makeRoomBooked();
        $resp = $this->roomBookedRepo->delete($roomBooked->rbRoomBookedID);
        $this->assertTrue($resp);
        $this->assertNull(RoomBooked::find($roomBooked->rbRoomBookedID), 'RoomBooked should not exist in DB');
    }
}
