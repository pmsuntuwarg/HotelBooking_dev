<?php

use Faker\Factory as Faker;
use App\Models\Admin\RoomStatus;
use App\Repositories\Admin\RoomStatusRepository;

trait MakeRoomStatusTrait
{
    /**
     * Create fake instance of RoomStatus and save it in database
     *
     * @param array $roomStatusFields
     * @return RoomStatus
     */
    public function makeRoomStatus($roomStatusFields = [])
    {
        /** @var RoomStatusRepository $roomStatusRepo */
        $roomStatusRepo = App::make(RoomStatusRepository::class);
        $theme = $this->fakeRoomStatusData($roomStatusFields);
        return $roomStatusRepo->create($theme);
    }

    /**
     * Get fake instance of RoomStatus
     *
     * @param array $roomStatusFields
     * @return RoomStatus
     */
    public function fakeRoomStatus($roomStatusFields = [])
    {
        return new RoomStatus($this->fakeRoomStatusData($roomStatusFields));
    }

    /**
     * Get fake data of RoomStatus
     *
     * @param array $postFields
     * @return array
     */
    public function fakeRoomStatusData($roomStatusFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'rsRoomStatus' => $fake->word,
            'rsDescription' => $fake->text,
            'rsSortOrder' => $fake->word,
            'rsActive' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $roomStatusFields);
    }
}
