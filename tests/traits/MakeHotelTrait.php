<?php

use Faker\Factory as Faker;
use App\Models\Admin\Hotel;
use App\Repositories\Admin\HotelRepository;

trait MakeHotelTrait
{
    /**
     * Create fake instance of Hotel and save it in database
     *
     * @param array $hotelFields
     * @return Hotel
     */
    public function makeHotel($hotelFields = [])
    {
        /** @var HotelRepository $hotelRepo */
        $hotelRepo = App::make(HotelRepository::class);
        $theme = $this->fakeHotelData($hotelFields);
        return $hotelRepo->create($theme);
    }

    /**
     * Get fake instance of Hotel
     *
     * @param array $hotelFields
     * @return Hotel
     */
    public function fakeHotel($hotelFields = [])
    {
        return new Hotel($this->fakeHotelData($hotelFields));
    }

    /**
     * Get fake data of Hotel
     *
     * @param array $postFields
     * @return array
     */
    public function fakeHotelData($hotelFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'hHotelCode' => $fake->text,
            'hName' => $fake->word,
            'hMotto' => $fake->word,
            'hAddress' => $fake->text,
            'hAddress2' => $fake->text,
            'hCity' => $fake->word,
            'hState' => $fake->word,
            'hZipCode' => $fake->word,
            'hMainPhoneNumber' => $fake->word,
            'hFaxNumber' => $fake->word,
            'hTollFreeNumber' => $fake->word,
            'hCompanyMailingAddress' => $fake->word,
            'hWebsiteAddress' => $fake->word,
            'hMain' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $hotelFields);
    }
}
