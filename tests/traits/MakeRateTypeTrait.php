<?php

use Faker\Factory as Faker;
use App\Models\Admin\RateType;
use App\Repositories\Admin\RateTypeRepository;

trait MakeRateTypeTrait
{
    /**
     * Create fake instance of RateType and save it in database
     *
     * @param array $rateTypeFields
     * @return RateType
     */
    public function makeRateType($rateTypeFields = [])
    {
        /** @var RateTypeRepository $rateTypeRepo */
        $rateTypeRepo = App::make(RateTypeRepository::class);
        $theme = $this->fakeRateTypeData($rateTypeFields);
        return $rateTypeRepo->create($theme);
    }

    /**
     * Get fake instance of RateType
     *
     * @param array $rateTypeFields
     * @return RateType
     */
    public function fakeRateType($rateTypeFields = [])
    {
        return new RateType($this->fakeRateTypeData($rateTypeFields));
    }

    /**
     * Get fake data of RateType
     *
     * @param array $postFields
     * @return array
     */
    public function fakeRateTypeData($rateTypeFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'rtRateType' => $fake->word,
            'rtDescription' => $fake->text,
            'rtSortOrder' => $fake->word,
            'rtActive' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $rateTypeFields);
    }
}
