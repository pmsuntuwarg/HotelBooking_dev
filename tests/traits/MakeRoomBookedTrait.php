<?php

use Faker\Factory as Faker;
use App\Models\Admin\RoomBooked;
use App\Repositories\Admin\RoomBookedRepository;

trait MakeRoomBookedTrait
{
    /**
     * Create fake instance of RoomBooked and save it in database
     *
     * @param array $roomBookedFields
     * @return RoomBooked
     */
    public function makeRoomBooked($roomBookedFields = [])
    {
        /** @var RoomBookedRepository $roomBookedRepo */
        $roomBookedRepo = App::make(RoomBookedRepository::class);
        $theme = $this->fakeRoomBookedData($roomBookedFields);
        return $roomBookedRepo->create($theme);
    }

    /**
     * Get fake instance of RoomBooked
     *
     * @param array $roomBookedFields
     * @return RoomBooked
     */
    public function fakeRoomBooked($roomBookedFields = [])
    {
        return new RoomBooked($this->fakeRoomBookedData($roomBookedFields));
    }

    /**
     * Get fake data of RoomBooked
     *
     * @param array $postFields
     * @return array
     */
    public function fakeRoomBookedData($roomBookedFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'rbBookingID' => $fake->word,
            'rbBookingID' => $fake->word,
            'rbRoomID' => $fake->word,
            'rbRate' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $roomBookedFields);
    }
}
