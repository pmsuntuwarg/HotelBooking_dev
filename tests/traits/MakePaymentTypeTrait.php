<?php

use Faker\Factory as Faker;
use App\Models\Admin\PaymentType;
use App\Repositories\Admin\PaymentTypeRepository;

trait MakePaymentTypeTrait
{
    /**
     * Create fake instance of PaymentType and save it in database
     *
     * @param array $paymentTypeFields
     * @return PaymentType
     */
    public function makePaymentType($paymentTypeFields = [])
    {
        /** @var PaymentTypeRepository $paymentTypeRepo */
        $paymentTypeRepo = App::make(PaymentTypeRepository::class);
        $theme = $this->fakePaymentTypeData($paymentTypeFields);
        return $paymentTypeRepo->create($theme);
    }

    /**
     * Get fake instance of PaymentType
     *
     * @param array $paymentTypeFields
     * @return PaymentType
     */
    public function fakePaymentType($paymentTypeFields = [])
    {
        return new PaymentType($this->fakePaymentTypeData($paymentTypeFields));
    }

    /**
     * Get fake data of PaymentType
     *
     * @param array $postFields
     * @return array
     */
    public function fakePaymentTypeData($paymentTypeFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'ptPaymentType' => $fake->word,
            'ptSortOrder' => $fake->word,
            'ptActive' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $paymentTypeFields);
    }
}
