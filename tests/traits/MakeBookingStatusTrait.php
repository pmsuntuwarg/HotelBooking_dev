<?php

use Faker\Factory as Faker;
use App\Models\Admin\BookingStatus;
use App\Repositories\Admin\BookingStatusRepository;

trait MakeBookingStatusTrait
{
    /**
     * Create fake instance of BookingStatus and save it in database
     *
     * @param array $bookingStatusFields
     * @return BookingStatus
     */
    public function makeBookingStatus($bookingStatusFields = [])
    {
        /** @var BookingStatusRepository $bookingStatusRepo */
        $bookingStatusRepo = App::make(BookingStatusRepository::class);
        $theme = $this->fakeBookingStatusData($bookingStatusFields);
        return $bookingStatusRepo->create($theme);
    }

    /**
     * Get fake instance of BookingStatus
     *
     * @param array $bookingStatusFields
     * @return BookingStatus
     */
    public function fakeBookingStatus($bookingStatusFields = [])
    {
        return new BookingStatus($this->fakeBookingStatusData($bookingStatusFields));
    }

    /**
     * Get fake data of BookingStatus
     *
     * @param array $postFields
     * @return array
     */
    public function fakeBookingStatusData($bookingStatusFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'bsStatus' => $fake->word,
            'bsDescription' => $fake->text,
            'bsSortOrder' => $fake->word,
            'bsActive' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $bookingStatusFields);
    }
}
