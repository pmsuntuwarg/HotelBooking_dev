<?php

use Faker\Factory as Faker;
use App\Models\Admin\Staff;
use App\Repositories\Admin\StaffRepository;

trait MakeStaffTrait
{
    /**
     * Create fake instance of Staff and save it in database
     *
     * @param array $staffFields
     * @return Staff
     */
    public function makeStaff($staffFields = [])
    {
        /** @var StaffRepository $staffRepo */
        $staffRepo = App::make(StaffRepository::class);
        $theme = $this->fakeStaffData($staffFields);
        return $staffRepo->create($theme);
    }

    /**
     * Get fake instance of Staff
     *
     * @param array $staffFields
     * @return Staff
     */
    public function fakeStaff($staffFields = [])
    {
        return new Staff($this->fakeStaffData($staffFields));
    }

    /**
     * Get fake data of Staff
     *
     * @param array $postFields
     * @return array
     */
    public function fakeStaffData($staffFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'sFirstName' => $fake->word,
            'sLastName' => $fake->word,
            'sPositionID' => $fake->randomDigitNotNull,
            'sAddress' => $fake->text,
            'sAddress2' => $fake->text,
            'sCity' => $fake->word,
            'sState' => $fake->word,
            'sZipCode' => $fake->word,
            'sCountry' => $fake->word,
            'sHomePhoneNumber' => $fake->word,
            'sCellularNumber' => $fake->word,
            'sMailAddress' => $fake->word,
            'sGender' => $fake->word,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'deleted_at' => $fake->date('Y-m-d H:i:s')
        ], $staffFields);
    }
}
