<?php

use Faker\Factory as Faker;
use App\Models\Admin\RoomType;
use App\Repositories\Admin\RoomTypeRepository;

trait MakeRoomTypeTrait
{
    /**
     * Create fake instance of RoomType and save it in database
     *
     * @param array $roomTypeFields
     * @return RoomType
     */
    public function makeRoomType($roomTypeFields = [])
    {
        /** @var RoomTypeRepository $roomTypeRepo */
        $roomTypeRepo = App::make(RoomTypeRepository::class);
        $theme = $this->fakeRoomTypeData($roomTypeFields);
        return $roomTypeRepo->create($theme);
    }

    /**
     * Get fake instance of RoomType
     *
     * @param array $roomTypeFields
     * @return RoomType
     */
    public function fakeRoomType($roomTypeFields = [])
    {
        return new RoomType($this->fakeRoomTypeData($roomTypeFields));
    }

    /**
     * Get fake data of RoomType
     *
     * @param array $postFields
     * @return array
     */
    public function fakeRoomTypeData($roomTypeFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'rtRoomType' => $fake->word,
            'rtDescription' => $fake->text,
            'rtSortOrder' => $fake->word,
            'rtActive' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $roomTypeFields);
    }
}
