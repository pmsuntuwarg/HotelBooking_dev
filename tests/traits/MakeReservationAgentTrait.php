<?php

use Faker\Factory as Faker;
use App\Models\Admin\ReservationAgent;
use App\Repositories\Admin\ReservationAgentRepository;

trait MakeReservationAgentTrait
{
    /**
     * Create fake instance of ReservationAgent and save it in database
     *
     * @param array $reservationAgentFields
     * @return ReservationAgent
     */
    public function makeReservationAgent($reservationAgentFields = [])
    {
        /** @var ReservationAgentRepository $reservationAgentRepo */
        $reservationAgentRepo = App::make(ReservationAgentRepository::class);
        $theme = $this->fakeReservationAgentData($reservationAgentFields);
        return $reservationAgentRepo->create($theme);
    }

    /**
     * Get fake instance of ReservationAgent
     *
     * @param array $reservationAgentFields
     * @return ReservationAgent
     */
    public function fakeReservationAgent($reservationAgentFields = [])
    {
        return new ReservationAgent($this->fakeReservationAgentData($reservationAgentFields));
    }

    /**
     * Get fake data of ReservationAgent
     *
     * @param array $postFields
     * @return array
     */
    public function fakeReservationAgentData($reservationAgentFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'raFirstName' => $fake->word,
            'raLastName' => $fake->word,
            'raAddress' => $fake->word,
            'raAddress2' => $fake->word,
            'raCity' => $fake->word,
            'raState' => $fake->word,
            'raZipCode' => $fake->word,
            'raCountry' => $fake->word,
            'raHomePhoneNumber' => $fake->word,
            'raCellularNumber' => $fake->word,
            'aMailAddress' => $fake->word,
            'raGender' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $reservationAgentFields);
    }
}
