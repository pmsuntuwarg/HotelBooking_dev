<?php

use Faker\Factory as Faker;
use App\Models\Admin\Guest;
use App\Repositories\Admin\GuestRepository;

trait MakeGuestTrait
{
    /**
     * Create fake instance of Guest and save it in database
     *
     * @param array $guestFields
     * @return Guest
     */
    public function makeGuest($guestFields = [])
    {
        /** @var GuestRepository $guestRepo */
        $guestRepo = App::make(GuestRepository::class);
        $theme = $this->fakeGuestData($guestFields);
        return $guestRepo->create($theme);
    }

    /**
     * Get fake instance of Guest
     *
     * @param array $guestFields
     * @return Guest
     */
    public function fakeGuest($guestFields = [])
    {
        return new Guest($this->fakeGuestData($guestFields));
    }

    /**
     * Get fake data of Guest
     *
     * @param array $postFields
     * @return array
     */
    public function fakeGuestData($guestFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'gFirstName' => $fake->word,
            'gLastName' => $fake->word,
            'gAddress' => $fake->word,
            'gAddress2' => $fake->word,
            'gCity' => $fake->word,
            'gState' => $fake->word,
            'gZipCode' => $fake->word,
            'gCountry' => $fake->word,
            'gHomePhoneNumber' => $fake->word,
            'gCellularNumber' => $fake->word,
            'gMailAddress' => $fake->word,
            'gGender' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $guestFields);
    }
}
