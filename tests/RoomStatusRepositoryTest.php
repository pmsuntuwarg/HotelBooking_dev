<?php

use App\Models\Admin\RoomStatus;
use App\Repositories\Admin\RoomStatusRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class RoomStatusRepositoryTest extends TestCase
{
    use MakeRoomStatusTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var RoomStatusRepository
     */
    protected $roomStatusRepo;

    public function setUp()
    {
        parent::setUp();
        $this->roomStatusRepo = App::make(RoomStatusRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateRoomStatus()
    {
        $roomStatus = $this->fakeRoomStatusData();
        $createdRoomStatus = $this->roomStatusRepo->create($roomStatus);
        $createdRoomStatus = $createdRoomStatus->toArray();
        $this->assertArrayHasKey('id', $createdRoomStatus);
        $this->assertNotNull($createdRoomStatus['id'], 'Created RoomStatus must have id specified');
        $this->assertNotNull(RoomStatus::find($createdRoomStatus['id']), 'RoomStatus with given id must be in DB');
        $this->assertModelData($roomStatus, $createdRoomStatus);
    }

    /**
     * @test read
     */
    public function testReadRoomStatus()
    {
        $roomStatus = $this->makeRoomStatus();
        $dbRoomStatus = $this->roomStatusRepo->find($roomStatus->rtRoomStatusID);
        $dbRoomStatus = $dbRoomStatus->toArray();
        $this->assertModelData($roomStatus->toArray(), $dbRoomStatus);
    }

    /**
     * @test update
     */
    public function testUpdateRoomStatus()
    {
        $roomStatus = $this->makeRoomStatus();
        $fakeRoomStatus = $this->fakeRoomStatusData();
        $updatedRoomStatus = $this->roomStatusRepo->update($fakeRoomStatus, $roomStatus->rtRoomStatusID);
        $this->assertModelData($fakeRoomStatus, $updatedRoomStatus->toArray());
        $dbRoomStatus = $this->roomStatusRepo->find($roomStatus->rtRoomStatusID);
        $this->assertModelData($fakeRoomStatus, $dbRoomStatus->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteRoomStatus()
    {
        $roomStatus = $this->makeRoomStatus();
        $resp = $this->roomStatusRepo->delete($roomStatus->rtRoomStatusID);
        $this->assertTrue($resp);
        $this->assertNull(RoomStatus::find($roomStatus->rtRoomStatusID), 'RoomStatus should not exist in DB');
    }
}
