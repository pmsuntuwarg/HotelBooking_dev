<?php

use App\Models\Admin\ReservationAgent;
use App\Repositories\Admin\ReservationAgentRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ReservationAgentRepositoryTest extends TestCase
{
    use MakeReservationAgentTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var ReservationAgentRepository
     */
    protected $reservationAgentRepo;

    public function setUp()
    {
        parent::setUp();
        $this->reservationAgentRepo = App::make(ReservationAgentRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateReservationAgent()
    {
        $reservationAgent = $this->fakeReservationAgentData();
        $createdReservationAgent = $this->reservationAgentRepo->create($reservationAgent);
        $createdReservationAgent = $createdReservationAgent->toArray();
        $this->assertArrayHasKey('id', $createdReservationAgent);
        $this->assertNotNull($createdReservationAgent['id'], 'Created ReservationAgent must have id specified');
        $this->assertNotNull(ReservationAgent::find($createdReservationAgent['id']), 'ReservationAgent with given id must be in DB');
        $this->assertModelData($reservationAgent, $createdReservationAgent);
    }

    /**
     * @test read
     */
    public function testReadReservationAgent()
    {
        $reservationAgent = $this->makeReservationAgent();
        $dbReservationAgent = $this->reservationAgentRepo->find($reservationAgent->raReservationAgentID);
        $dbReservationAgent = $dbReservationAgent->toArray();
        $this->assertModelData($reservationAgent->toArray(), $dbReservationAgent);
    }

    /**
     * @test update
     */
    public function testUpdateReservationAgent()
    {
        $reservationAgent = $this->makeReservationAgent();
        $fakeReservationAgent = $this->fakeReservationAgentData();
        $updatedReservationAgent = $this->reservationAgentRepo->update($fakeReservationAgent, $reservationAgent->raReservationAgentID);
        $this->assertModelData($fakeReservationAgent, $updatedReservationAgent->toArray());
        $dbReservationAgent = $this->reservationAgentRepo->find($reservationAgent->raReservationAgentID);
        $this->assertModelData($fakeReservationAgent, $dbReservationAgent->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteReservationAgent()
    {
        $reservationAgent = $this->makeReservationAgent();
        $resp = $this->reservationAgentRepo->delete($reservationAgent->raReservationAgentID);
        $this->assertTrue($resp);
        $this->assertNull(ReservationAgent::find($reservationAgent->raReservationAgentID), 'ReservationAgent should not exist in DB');
    }
}
