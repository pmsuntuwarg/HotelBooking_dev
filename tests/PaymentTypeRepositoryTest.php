<?php

use App\Models\Admin\PaymentType;
use App\Repositories\Admin\PaymentTypeRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PaymentTypeRepositoryTest extends TestCase
{
    use MakePaymentTypeTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var PaymentTypeRepository
     */
    protected $paymentTypeRepo;

    public function setUp()
    {
        parent::setUp();
        $this->paymentTypeRepo = App::make(PaymentTypeRepository::class);
    }

    /**
     * @test create
     */
    public function testCreatePaymentType()
    {
        $paymentType = $this->fakePaymentTypeData();
        $createdPaymentType = $this->paymentTypeRepo->create($paymentType);
        $createdPaymentType = $createdPaymentType->toArray();
        $this->assertArrayHasKey('id', $createdPaymentType);
        $this->assertNotNull($createdPaymentType['id'], 'Created PaymentType must have id specified');
        $this->assertNotNull(PaymentType::find($createdPaymentType['id']), 'PaymentType with given id must be in DB');
        $this->assertModelData($paymentType, $createdPaymentType);
    }

    /**
     * @test read
     */
    public function testReadPaymentType()
    {
        $paymentType = $this->makePaymentType();
        $dbPaymentType = $this->paymentTypeRepo->find($paymentType->ptPaymentTypeID);
        $dbPaymentType = $dbPaymentType->toArray();
        $this->assertModelData($paymentType->toArray(), $dbPaymentType);
    }

    /**
     * @test update
     */
    public function testUpdatePaymentType()
    {
        $paymentType = $this->makePaymentType();
        $fakePaymentType = $this->fakePaymentTypeData();
        $updatedPaymentType = $this->paymentTypeRepo->update($fakePaymentType, $paymentType->ptPaymentTypeID);
        $this->assertModelData($fakePaymentType, $updatedPaymentType->toArray());
        $dbPaymentType = $this->paymentTypeRepo->find($paymentType->ptPaymentTypeID);
        $this->assertModelData($fakePaymentType, $dbPaymentType->toArray());
    }

    /**
     * @test delete
     */
    public function testDeletePaymentType()
    {
        $paymentType = $this->makePaymentType();
        $resp = $this->paymentTypeRepo->delete($paymentType->ptPaymentTypeID);
        $this->assertTrue($resp);
        $this->assertNull(PaymentType::find($paymentType->ptPaymentTypeID), 'PaymentType should not exist in DB');
    }
}
