<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class GuestApiTest extends TestCase
{
    use MakeGuestTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateGuest()
    {
        $guest = $this->fakeGuestData();
        $this->json('POST', '/api/v1/guests', $guest);

        $this->assertApiResponse($guest);
    }

    /**
     * @test
     */
    public function testReadGuest()
    {
        $guest = $this->makeGuest();
        $this->json('GET', '/api/v1/guests/'.$guest->gGuestID);

        $this->assertApiResponse($guest->toArray());
    }

    /**
     * @test
     */
    public function testUpdateGuest()
    {
        $guest = $this->makeGuest();
        $editedGuest = $this->fakeGuestData();

        $this->json('PUT', '/api/v1/guests/'.$guest->gGuestID, $editedGuest);

        $this->assertApiResponse($editedGuest);
    }

    /**
     * @test
     */
    public function testDeleteGuest()
    {
        $guest = $this->makeGuest();
        $this->json('DELETE', '/api/v1/guests/'.$guest->gGuestID);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/guests/'.$guest->gGuestID);

        $this->assertResponseStatus(404);
    }
}
