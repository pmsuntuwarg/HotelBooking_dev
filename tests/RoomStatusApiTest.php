<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class RoomStatusApiTest extends TestCase
{
    use MakeRoomStatusTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateRoomStatus()
    {
        $roomStatus = $this->fakeRoomStatusData();
        $this->json('POST', '/api/v1/roomStatuses', $roomStatus);

        $this->assertApiResponse($roomStatus);
    }

    /**
     * @test
     */
    public function testReadRoomStatus()
    {
        $roomStatus = $this->makeRoomStatus();
        $this->json('GET', '/api/v1/roomStatuses/'.$roomStatus->rtRoomStatusID);

        $this->assertApiResponse($roomStatus->toArray());
    }

    /**
     * @test
     */
    public function testUpdateRoomStatus()
    {
        $roomStatus = $this->makeRoomStatus();
        $editedRoomStatus = $this->fakeRoomStatusData();

        $this->json('PUT', '/api/v1/roomStatuses/'.$roomStatus->rtRoomStatusID, $editedRoomStatus);

        $this->assertApiResponse($editedRoomStatus);
    }

    /**
     * @test
     */
    public function testDeleteRoomStatus()
    {
        $roomStatus = $this->makeRoomStatus();
        $this->json('DELETE', '/api/v1/roomStatuses/'.$roomStatus->rtRoomStatusID);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/roomStatuses/'.$roomStatus->rtRoomStatusID);

        $this->assertResponseStatus(404);
    }
}
