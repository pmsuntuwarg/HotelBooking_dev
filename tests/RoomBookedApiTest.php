<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class RoomBookedApiTest extends TestCase
{
    use MakeRoomBookedTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateRoomBooked()
    {
        $roomBooked = $this->fakeRoomBookedData();
        $this->json('POST', '/api/v1/roomBookeds', $roomBooked);

        $this->assertApiResponse($roomBooked);
    }

    /**
     * @test
     */
    public function testReadRoomBooked()
    {
        $roomBooked = $this->makeRoomBooked();
        $this->json('GET', '/api/v1/roomBookeds/'.$roomBooked->rbRoomBookedID);

        $this->assertApiResponse($roomBooked->toArray());
    }

    /**
     * @test
     */
    public function testUpdateRoomBooked()
    {
        $roomBooked = $this->makeRoomBooked();
        $editedRoomBooked = $this->fakeRoomBookedData();

        $this->json('PUT', '/api/v1/roomBookeds/'.$roomBooked->rbRoomBookedID, $editedRoomBooked);

        $this->assertApiResponse($editedRoomBooked);
    }

    /**
     * @test
     */
    public function testDeleteRoomBooked()
    {
        $roomBooked = $this->makeRoomBooked();
        $this->json('DELETE', '/api/v1/roomBookeds/'.$roomBooked->rbRoomBookedID);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/roomBookeds/'.$roomBooked->rbRoomBookedID);

        $this->assertResponseStatus(404);
    }
}
