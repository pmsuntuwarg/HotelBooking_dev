<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class RateTypeApiTest extends TestCase
{
    use MakeRateTypeTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateRateType()
    {
        $rateType = $this->fakeRateTypeData();
        $this->json('POST', '/api/v1/rateTypes', $rateType);

        $this->assertApiResponse($rateType);
    }

    /**
     * @test
     */
    public function testReadRateType()
    {
        $rateType = $this->makeRateType();
        $this->json('GET', '/api/v1/rateTypes/'.$rateType->rtRateTypeTypeID);

        $this->assertApiResponse($rateType->toArray());
    }

    /**
     * @test
     */
    public function testUpdateRateType()
    {
        $rateType = $this->makeRateType();
        $editedRateType = $this->fakeRateTypeData();

        $this->json('PUT', '/api/v1/rateTypes/'.$rateType->rtRateTypeTypeID, $editedRateType);

        $this->assertApiResponse($editedRateType);
    }

    /**
     * @test
     */
    public function testDeleteRateType()
    {
        $rateType = $this->makeRateType();
        $this->json('DELETE', '/api/v1/rateTypes/'.$rateType->rtRateTypeTypeID);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/rateTypes/'.$rateType->rtRateTypeTypeID);

        $this->assertResponseStatus(404);
    }
}
