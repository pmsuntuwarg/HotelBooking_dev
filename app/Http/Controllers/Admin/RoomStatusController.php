<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\CreateRoomStatusRequest;
use App\Http\Requests\Admin\UpdateRoomStatusRequest;
use App\Repositories\Admin\RoomStatusRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class RoomStatusController extends AppBaseController
{
    /** @var  RoomStatusRepository */
    private $roomStatusRepository;

    public function __construct(RoomStatusRepository $roomStatusRepo)
    {
        $this->roomStatusRepository = $roomStatusRepo;
    }

    /**
     * Display a listing of the RoomStatus.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->roomStatusRepository->pushCriteria(new RequestCriteria($request));
        $roomStatuses = $this->roomStatusRepository->all();

        return view('admin.room_statuses.index')
            ->with('roomStatuses', $roomStatuses);
    }

    /**
     * Show the form for creating a new RoomStatus.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.room_statuses.create');
    }

    /**
     * Store a newly created RoomStatus in storage.
     *
     * @param CreateRoomStatusRequest $request
     *
     * @return Response
     */
    public function store(CreateRoomStatusRequest $request)
    {
        $input = $request->all();

        $roomStatus = $this->roomStatusRepository->create($input);

        Flash::success('Room Status saved successfully.');

        return redirect(route('admin.roomStatuses.index'));
    }

    /**
     * Display the specified RoomStatus.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $roomStatus = $this->roomStatusRepository->findWithoutFail($id);

        if (empty($roomStatus)) {
            Flash::error('Room Status not found');

            return redirect(route('admin.roomStatuses.index'));
        }

        return view('admin.room_statuses.show')->with('roomStatus', $roomStatus);
    }

    /**
     * Show the form for editing the specified RoomStatus.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $roomStatus = $this->roomStatusRepository->findWithoutFail($id);

        if (empty($roomStatus)) {
            Flash::error('Room Status not found');

            return redirect(route('admin.roomStatuses.index'));
        }

        return view('admin.room_statuses.edit')->with('roomStatus', $roomStatus);
    }

    /**
     * Update the specified RoomStatus in storage.
     *
     * @param  int              $id
     * @param UpdateRoomStatusRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateRoomStatusRequest $request)
    {
        $roomStatus = $this->roomStatusRepository->findWithoutFail($id);

        if (empty($roomStatus)) {
            Flash::error('Room Status not found');

            return redirect(route('admin.roomStatuses.index'));
        }

        $roomStatus = $this->roomStatusRepository->update($request->all(), $id);

        Flash::success('Room Status updated successfully.');

        return redirect(route('admin.roomStatuses.index'));
    }

    /**
     * Remove the specified RoomStatus from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $roomStatus = $this->roomStatusRepository->findWithoutFail($id);

        if (empty($roomStatus)) {
            Flash::error('Room Status not found');

            return redirect(route('admin.roomStatuses.index'));
        }

        $this->roomStatusRepository->delete($id);

        Flash::success('Room Status deleted successfully.');

        return redirect(route('admin.roomStatuses.index'));
    }
}
