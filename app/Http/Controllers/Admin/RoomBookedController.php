<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\CreateRoomBookedRequest;
use App\Http\Requests\Admin\UpdateRoomBookedRequest;
use App\Repositories\Admin\RoomBookedRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class RoomBookedController extends AppBaseController
{
    /** @var  RoomBookedRepository */
    private $roomBookedRepository;

    public function __construct(RoomBookedRepository $roomBookedRepo)
    {
        $this->roomBookedRepository = $roomBookedRepo;
    }

    /**
     * Display a listing of the RoomBooked.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->roomBookedRepository->pushCriteria(new RequestCriteria($request));
        $roomBookeds = $this->roomBookedRepository->all();

        return view('admin.room_bookeds.index')
            ->with('roomBookeds', $roomBookeds);
    }

    /**
     * Show the form for creating a new RoomBooked.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.room_bookeds.create');
    }

    /**
     * Store a newly created RoomBooked in storage.
     *
     * @param CreateRoomBookedRequest $request
     *
     * @return Response
     */
    public function store(CreateRoomBookedRequest $request)
    {
        $input = $request->all();

        $roomBooked = $this->roomBookedRepository->create($input);

        Flash::success('Room Booked saved successfully.');

        return redirect(route('admin.roomBookeds.index'));
    }

    /**
     * Display the specified RoomBooked.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $roomBooked = $this->roomBookedRepository->findWithoutFail($id);

        if (empty($roomBooked)) {
            Flash::error('Room Booked not found');

            return redirect(route('admin.roomBookeds.index'));
        }

        return view('admin.room_bookeds.show')->with('roomBooked', $roomBooked);
    }

    /**
     * Show the form for editing the specified RoomBooked.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $roomBooked = $this->roomBookedRepository->findWithoutFail($id);

        if (empty($roomBooked)) {
            Flash::error('Room Booked not found');

            return redirect(route('admin.roomBookeds.index'));
        }

        return view('admin.room_bookeds.edit')->with('roomBooked', $roomBooked);
    }

    /**
     * Update the specified RoomBooked in storage.
     *
     * @param  int              $id
     * @param UpdateRoomBookedRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateRoomBookedRequest $request)
    {
        $roomBooked = $this->roomBookedRepository->findWithoutFail($id);

        if (empty($roomBooked)) {
            Flash::error('Room Booked not found');

            return redirect(route('admin.roomBookeds.index'));
        }

        $roomBooked = $this->roomBookedRepository->update($request->all(), $id);

        Flash::success('Room Booked updated successfully.');

        return redirect(route('admin.roomBookeds.index'));
    }

    /**
     * Remove the specified RoomBooked from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $roomBooked = $this->roomBookedRepository->findWithoutFail($id);

        if (empty($roomBooked)) {
            Flash::error('Room Booked not found');

            return redirect(route('admin.roomBookeds.index'));
        }

        $this->roomBookedRepository->delete($id);

        Flash::success('Room Booked deleted successfully.');

        return redirect(route('admin.roomBookeds.index'));
    }
}
