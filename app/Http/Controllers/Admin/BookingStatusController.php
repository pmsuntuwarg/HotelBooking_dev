<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\CreateBookingStatusRequest;
use App\Http\Requests\Admin\UpdateBookingStatusRequest;
use App\Repositories\Admin\BookingStatusRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class BookingStatusController extends AppBaseController
{
    /** @var  BookingStatusRepository */
    private $bookingStatusRepository;

    public function __construct(BookingStatusRepository $bookingStatusRepo)
    {
        $this->bookingStatusRepository = $bookingStatusRepo;
    }

    /**
     * Display a listing of the BookingStatus.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->bookingStatusRepository->pushCriteria(new RequestCriteria($request));
        $bookingStatuses = $this->bookingStatusRepository->all();

        return view('admin.booking_statuses.index')
            ->with('bookingStatuses', $bookingStatuses);
    }

    /**
     * Show the form for creating a new BookingStatus.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.booking_statuses.create');
    }

    /**
     * Store a newly created BookingStatus in storage.
     *
     * @param CreateBookingStatusRequest $request
     *
     * @return Response
     */
    public function store(CreateBookingStatusRequest $request)
    {
        $input = $request->all();

        $bookingStatus = $this->bookingStatusRepository->create($input);

        Flash::success('Booking Status saved successfully.');

        return redirect(route('admin.bookingStatuses.index'));
    }

    /**
     * Display the specified BookingStatus.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $bookingStatus = $this->bookingStatusRepository->findWithoutFail($id);

        if (empty($bookingStatus)) {
            Flash::error('Booking Status not found');

            return redirect(route('admin.bookingStatuses.index'));
        }

        return view('admin.booking_statuses.show')->with('bookingStatus', $bookingStatus);
    }

    /**
     * Show the form for editing the specified BookingStatus.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $bookingStatus = $this->bookingStatusRepository->findWithoutFail($id);

        if (empty($bookingStatus)) {
            Flash::error('Booking Status not found');

            return redirect(route('admin.bookingStatuses.index'));
        }

        return view('admin.booking_statuses.edit')->with('bookingStatus', $bookingStatus);
    }

    /**
     * Update the specified BookingStatus in storage.
     *
     * @param  int              $id
     * @param UpdateBookingStatusRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateBookingStatusRequest $request)
    {
        $bookingStatus = $this->bookingStatusRepository->findWithoutFail($id);

        if (empty($bookingStatus)) {
            Flash::error('Booking Status not found');

            return redirect(route('admin.bookingStatuses.index'));
        }

        $bookingStatus = $this->bookingStatusRepository->update($request->all(), $id);

        Flash::success('Booking Status updated successfully.');

        return redirect(route('admin.bookingStatuses.index'));
    }

    /**
     * Remove the specified BookingStatus from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $bookingStatus = $this->bookingStatusRepository->findWithoutFail($id);

        if (empty($bookingStatus)) {
            Flash::error('Booking Status not found');

            return redirect(route('admin.bookingStatuses.index'));
        }

        $this->bookingStatusRepository->delete($id);

        Flash::success('Booking Status deleted successfully.');

        return redirect(route('admin.bookingStatuses.index'));
    }
}
