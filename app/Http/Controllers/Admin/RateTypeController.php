<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\CreateRateTypeRequest;
use App\Http\Requests\Admin\UpdateRateTypeRequest;
use App\Repositories\Admin\RateTypeRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class RateTypeController extends AppBaseController
{
    /** @var  RateTypeRepository */
    private $rateTypeRepository;

    public function __construct(RateTypeRepository $rateTypeRepo)
    {
        $this->rateTypeRepository = $rateTypeRepo;
    }

    /**
     * Display a listing of the RateType.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->rateTypeRepository->pushCriteria(new RequestCriteria($request));
        $rateTypes = $this->rateTypeRepository->all();

        return view('admin.rate_types.index')
            ->with('rateTypes', $rateTypes);
    }

    /**
     * Show the form for creating a new RateType.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.rate_types.create');
    }

    /**
     * Store a newly created RateType in storage.
     *
     * @param CreateRateTypeRequest $request
     *
     * @return Response
     */
    public function store(CreateRateTypeRequest $request)
    {
        $input = $request->all();

        $rateType = $this->rateTypeRepository->create($input);

        Flash::success('Rate Type saved successfully.');

        return redirect(route('admin.rateTypes.index'));
    }

    /**
     * Display the specified RateType.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $rateType = $this->rateTypeRepository->findWithoutFail($id);

        if (empty($rateType)) {
            Flash::error('Rate Type not found');

            return redirect(route('admin.rateTypes.index'));
        }

        return view('admin.rate_types.show')->with('rateType', $rateType);
    }

    /**
     * Show the form for editing the specified RateType.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $rateType = $this->rateTypeRepository->findWithoutFail($id);

        if (empty($rateType)) {
            Flash::error('Rate Type not found');

            return redirect(route('admin.rateTypes.index'));
        }

        return view('admin.rate_types.edit')->with('rateType', $rateType);
    }

    /**
     * Update the specified RateType in storage.
     *
     * @param  int              $id
     * @param UpdateRateTypeRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateRateTypeRequest $request)
    {
        $rateType = $this->rateTypeRepository->findWithoutFail($id);

        if (empty($rateType)) {
            Flash::error('Rate Type not found');

            return redirect(route('admin.rateTypes.index'));
        }

        $rateType = $this->rateTypeRepository->update($request->all(), $id);

        Flash::success('Rate Type updated successfully.');

        return redirect(route('admin.rateTypes.index'));
    }

    /**
     * Remove the specified RateType from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $rateType = $this->rateTypeRepository->findWithoutFail($id);

        if (empty($rateType)) {
            Flash::error('Rate Type not found');

            return redirect(route('admin.rateTypes.index'));
        }

        $this->rateTypeRepository->delete($id);

        Flash::success('Rate Type deleted successfully.');

        return redirect(route('admin.rateTypes.index'));
    }
}
