<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\CreateRoomRequest;
use App\Http\Requests\Admin\UpdateRoomRequest;
use App\Models\Admin\Hotel;
use App\Models\Admin\RoomStatus;
use App\Models\Admin\RoomType;
use App\Repositories\Admin\RoomRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class RoomController extends AppBaseController
{
    /** @var  RoomRepository */
    private $roomRepository;

    public function __construct(RoomRepository $roomRepo)
    {
        $this->roomRepository = $roomRepo;
    }

    /**
     * Display a listing of the Room.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->roomRepository->pushCriteria(new RequestCriteria($request));
        $rooms = $this->roomRepository->all();

        return view('admin.rooms.index')
            ->with('rooms', $rooms);
    }

    /**
     * Show the form for creating a new Room.
     *
     * @return Response
     */
    public function create()
    {
        $hotels = app(Hotel::class)->all();
        $hotelsArray = array();
        foreach ($hotels as $hotel){
            $hotelsArray[$hotel->hHotelID] = $hotel->hName;
        }
        $roomTypes = app(RoomType::class)->all();
        $roomTypesArray= array();
        foreach ($roomTypes as $roomType){
            $roomTypesArray[$roomType->rtRoomTypeID] = $roomType->rtRoomType;
        }
        $roomStatuses = app(RoomStatus::class)->all();
        $roomStatusesArray = array();
        foreach ($roomStatuses as $roomStatus){
            $roomStatusesArray[$roomStatus->rsRoomStatusID] = $roomStatus->rsRoomStatus;
        }
        return view('admin.rooms.create',compact('hotelsArray','roomTypesArray','roomStatusesArray'));
    }

    /**
     * Store a newly created Room in storage.
     *
     * @param CreateRoomRequest $request
     *
     * @return Response
     */
    public function store(CreateRoomRequest $request)
    {
        $input = $request->all();

        $room = $this->roomRepository->create($input);

        Flash::success('Room saved successfully.');

        return redirect(route('admin.rooms.index'));
    }

    /**
     * Display the specified Room.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $room = $this->roomRepository->findWithoutFail($id);

        if (empty($room)) {
            Flash::error('Room not found');

            return redirect(route('admin.rooms.index'));
        }

        return view('admin.rooms.show')->with('room', $room);
    }

    /**
     * Show the form for editing the specified Room.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $room = $this->roomRepository->findWithoutFail($id);

        if (empty($room)) {
            Flash::error('Room not found');

            return redirect(route('admin.rooms.index'));
        }

        return view('admin.rooms.edit')->with('room', $room);
    }

    /**
     * Update the specified Room in storage.
     *
     * @param  int              $id
     * @param UpdateRoomRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateRoomRequest $request)
    {
        $room = $this->roomRepository->findWithoutFail($id);

        if (empty($room)) {
            Flash::error('Room not found');

            return redirect(route('admin.rooms.index'));
        }

        $room = $this->roomRepository->update($request->all(), $id);

        Flash::success('Room updated successfully.');

        return redirect(route('admin.rooms.index'));
    }

    /**
     * Remove the specified Room from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $room = $this->roomRepository->findWithoutFail($id);

        if (empty($room)) {
            Flash::error('Room not found');

            return redirect(route('admin.rooms.index'));
        }

        $this->roomRepository->delete($id);

        Flash::success('Room deleted successfully.');

        return redirect(route('admin.rooms.index'));
    }
}
