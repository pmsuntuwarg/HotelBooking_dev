<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\CreateGuestRequest;
use App\Http\Requests\Admin\UpdateGuestRequest;
use App\Repositories\Admin\GuestRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class GuestController extends AppBaseController
{
    /** @var  GuestRepository */
    private $guestRepository;

    public function __construct(GuestRepository $guestRepo)
    {
        $this->guestRepository = $guestRepo;
    }

    /**
     * Display a listing of the Guest.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->guestRepository->pushCriteria(new RequestCriteria($request));
        $guests = $this->guestRepository->all();

        return view('admin.guests.index')
            ->with('guests', $guests);
    }

    /**
     * Show the form for creating a new Guest.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.guests.create');
    }

    /**
     * Store a newly created Guest in storage.
     *
     * @param CreateGuestRequest $request
     *
     * @return Response
     */
    public function store(CreateGuestRequest $request)
    {
        $input = $request->all();

        $guest = $this->guestRepository->create($input);

        Flash::success('Guest saved successfully.');

        return redirect(route('admin.guests.index'));
    }

    /**
     * Display the specified Guest.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $guest = $this->guestRepository->findWithoutFail($id);

        if (empty($guest)) {
            Flash::error('Guest not found');

            return redirect(route('admin.guests.index'));
        }

        return view('admin.guests.show')->with('guest', $guest);
    }

    /**
     * Show the form for editing the specified Guest.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $guest = $this->guestRepository->findWithoutFail($id);

        if (empty($guest)) {
            Flash::error('Guest not found');

            return redirect(route('admin.guests.index'));
        }

        return view('admin.guests.edit')->with('guest', $guest);
    }

    /**
     * Update the specified Guest in storage.
     *
     * @param  int              $id
     * @param UpdateGuestRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateGuestRequest $request)
    {
        $guest = $this->guestRepository->findWithoutFail($id);

        if (empty($guest)) {
            Flash::error('Guest not found');

            return redirect(route('admin.guests.index'));
        }

        $guest = $this->guestRepository->update($request->all(), $id);

        Flash::success('Guest updated successfully.');

        return redirect(route('admin.guests.index'));
    }

    /**
     * Remove the specified Guest from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $guest = $this->guestRepository->findWithoutFail($id);

        if (empty($guest)) {
            Flash::error('Guest not found');

            return redirect(route('admin.guests.index'));
        }

        $this->guestRepository->delete($id);

        Flash::success('Guest deleted successfully.');

        return redirect(route('admin.guests.index'));
    }
}
