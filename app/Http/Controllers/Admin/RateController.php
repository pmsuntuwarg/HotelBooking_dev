<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\CreateRateRequest;
use App\Http\Requests\Admin\UpdateRateRequest;
use App\Models\Admin\RateType;
use App\Models\Admin\Room;
use App\Models\Admin\RoomType;
use App\Repositories\Admin\RateRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class RateController extends AppBaseController
{
    /** @var  RateRepository */
    private $rateRepository;

    public function __construct(RateRepository $rateRepo)
    {
        $this->rateRepository = $rateRepo;
    }

    /**
     * Display a listing of the Rate.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->rateRepository->pushCriteria(new RequestCriteria($request));
        $rates = $this->rateRepository->all();

        return view('admin.rates.index')
            ->with('rates', $rates);
    }

    /**
     * Show the form for creating a new Rate.
     *
     * @return Response
     */
    public function create()
    {

        $rooms = app(Room::class)->all();
        $roomsArray= array();
        foreach ($rooms as $room){
            $roomsArray[$room->rRoomID] = $room->rRoomNumber;
        }

        $rateTypes = app(RateType::class)->all();
        $rateTypesArray = array();
        foreach ($rateTypes as $rateType){
            $rateTypesArray[$rateType->rtRateTypeID] = $rateType->rtRateType;
        }
        return view('admin.rates.create',compact('roomsArray','rateTypesArray'));
    }

    /**
     * Store a newly created Rate in storage.
     *
     * @param CreateRateRequest $request
     *
     * @return Response
     */
    public function store(CreateRateRequest $request)
    {
        $input = $request->all();

        $rate = $this->rateRepository->create($input);

        Flash::success('Rate saved successfully.');

        return redirect(route('admin.rates.index'));
    }

    /**
     * Display the specified Rate.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $rate = $this->rateRepository->findWithoutFail($id);

        if (empty($rate)) {
            Flash::error('Rate not found');

            return redirect(route('admin.rates.index'));
        }

        return view('admin.rates.show')->with('rate', $rate);
    }

    /**
     * Show the form for editing the specified Rate.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $rate = $this->rateRepository->findWithoutFail($id);

        if (empty($rate)) {
            Flash::error('Rate not found');

            return redirect(route('admin.rates.index'));
        }

        return view('admin.rates.edit')->with('rate', $rate);
    }

    /**
     * Update the specified Rate in storage.
     *
     * @param  int              $id
     * @param UpdateRateRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateRateRequest $request)
    {
        $rate = $this->rateRepository->findWithoutFail($id);

        if (empty($rate)) {
            Flash::error('Rate not found');

            return redirect(route('admin.rates.index'));
        }

        $rate = $this->rateRepository->update($request->all(), $id);

        Flash::success('Rate updated successfully.');

        return redirect(route('admin.rates.index'));
    }

    /**
     * Remove the specified Rate from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $rate = $this->rateRepository->findWithoutFail($id);

        if (empty($rate)) {
            Flash::error('Rate not found');

            return redirect(route('admin.rates.index'));
        }

        $this->rateRepository->delete($id);

        Flash::success('Rate deleted successfully.');

        return redirect(route('admin.rates.index'));
    }
}
