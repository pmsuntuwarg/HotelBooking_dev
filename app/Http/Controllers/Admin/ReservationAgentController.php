<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\CreateReservationAgentRequest;
use App\Http\Requests\Admin\UpdateReservationAgentRequest;
use App\Repositories\Admin\ReservationAgentRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class ReservationAgentController extends AppBaseController
{
    /** @var  ReservationAgentRepository */
    private $reservationAgentRepository;

    public function __construct(ReservationAgentRepository $reservationAgentRepo)
    {
        $this->reservationAgentRepository = $reservationAgentRepo;
    }

    /**
     * Display a listing of the ReservationAgent.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->reservationAgentRepository->pushCriteria(new RequestCriteria($request));
        $reservationAgents = $this->reservationAgentRepository->all();

        return view('admin.reservation_agents.index')
            ->with('reservationAgents', $reservationAgents);
    }

    /**
     * Show the form for creating a new ReservationAgent.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.reservation_agents.create');
    }

    /**
     * Store a newly created ReservationAgent in storage.
     *
     * @param CreateReservationAgentRequest $request
     *
     * @return Response
     */
    public function store(CreateReservationAgentRequest $request)
    {
        $input = $request->all();

        $reservationAgent = $this->reservationAgentRepository->create($input);

        Flash::success('Reservation Agent saved successfully.');

        return redirect(route('admin.reservationAgents.index'));
    }

    /**
     * Display the specified ReservationAgent.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $reservationAgent = $this->reservationAgentRepository->findWithoutFail($id);

        if (empty($reservationAgent)) {
            Flash::error('Reservation Agent not found');

            return redirect(route('admin.reservationAgents.index'));
        }

        return view('admin.reservation_agents.show')->with('reservationAgent', $reservationAgent);
    }

    /**
     * Show the form for editing the specified ReservationAgent.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $reservationAgent = $this->reservationAgentRepository->findWithoutFail($id);

        if (empty($reservationAgent)) {
            Flash::error('Reservation Agent not found');

            return redirect(route('admin.reservationAgents.index'));
        }

        return view('admin.reservation_agents.edit')->with('reservationAgent', $reservationAgent);
    }

    /**
     * Update the specified ReservationAgent in storage.
     *
     * @param  int              $id
     * @param UpdateReservationAgentRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateReservationAgentRequest $request)
    {
        $reservationAgent = $this->reservationAgentRepository->findWithoutFail($id);

        if (empty($reservationAgent)) {
            Flash::error('Reservation Agent not found');

            return redirect(route('admin.reservationAgents.index'));
        }

        $reservationAgent = $this->reservationAgentRepository->update($request->all(), $id);

        Flash::success('Reservation Agent updated successfully.');

        return redirect(route('admin.reservationAgents.index'));
    }

    /**
     * Remove the specified ReservationAgent from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $reservationAgent = $this->reservationAgentRepository->findWithoutFail($id);

        if (empty($reservationAgent)) {
            Flash::error('Reservation Agent not found');

            return redirect(route('admin.reservationAgents.index'));
        }

        $this->reservationAgentRepository->delete($id);

        Flash::success('Reservation Agent deleted successfully.');

        return redirect(route('admin.reservationAgents.index'));
    }
}
