<?php

namespace App\Http\Controllers\API\Admin;

use App\Http\Requests\API\Admin\CreateStaffAPIRequest;
use App\Http\Requests\API\Admin\UpdateStaffAPIRequest;
use App\Models\Admin\Staff;
use App\Repositories\Admin\StaffRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class StaffController
 * @package App\Http\Controllers\API\Admin
 */

class StaffAPIController extends AppBaseController
{
    /** @var  StaffRepository */
    private $staffRepository;

    public function __construct(StaffRepository $staffRepo)
    {
        $this->staffRepository = $staffRepo;
    }

    /**
     * Display a listing of the Staff.
     * GET|HEAD /staff
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->staffRepository->pushCriteria(new RequestCriteria($request));
        $this->staffRepository->pushCriteria(new LimitOffsetCriteria($request));
        $staff = $this->staffRepository->all();

        return $this->sendResponse($staff->toArray(), 'Staff retrieved successfully');
    }

    /**
     * Store a newly created Staff in storage.
     * POST /staff
     *
     * @param CreateStaffAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateStaffAPIRequest $request)
    {
        $input = $request->all();

        $staff = $this->staffRepository->create($input);

        return $this->sendResponse($staff->toArray(), 'Staff saved successfully');
    }

    /**
     * Display the specified Staff.
     * GET|HEAD /staff/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Staff $staff */
        $staff = $this->staffRepository->findWithoutFail($id);

        if (empty($staff)) {
            return $this->sendError('Staff not found');
        }

        return $this->sendResponse($staff->toArray(), 'Staff retrieved successfully');
    }

    /**
     * Update the specified Staff in storage.
     * PUT/PATCH /staff/{id}
     *
     * @param  int $id
     * @param UpdateStaffAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateStaffAPIRequest $request)
    {
        $input = $request->all();

        /** @var Staff $staff */
        $staff = $this->staffRepository->findWithoutFail($id);

        if (empty($staff)) {
            return $this->sendError('Staff not found');
        }

        $staff = $this->staffRepository->update($input, $id);

        return $this->sendResponse($staff->toArray(), 'Staff updated successfully');
    }

    /**
     * Remove the specified Staff from storage.
     * DELETE /staff/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Staff $staff */
        $staff = $this->staffRepository->findWithoutFail($id);

        if (empty($staff)) {
            return $this->sendError('Staff not found');
        }

        $staff->delete();

        return $this->sendResponse($id, 'Staff deleted successfully');
    }
}
