<?php

namespace App\Http\Controllers\API\Admin;

use App\Http\Requests\API\Admin\CreateHotelAPIRequest;
use App\Http\Requests\API\Admin\UpdateHotelAPIRequest;
use App\Models\Admin\Hotel;
use App\Repositories\Admin\HotelRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class HotelController
 * @package App\Http\Controllers\API\Admin
 */

class HotelAPIController extends AppBaseController
{
    /** @var  HotelRepository */
    private $hotelRepository;

    public function __construct(HotelRepository $hotelRepo)
    {
        $this->hotelRepository = $hotelRepo;
    }

    /**
     * Display a listing of the Hotel.
     * GET|HEAD /hotels
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->hotelRepository->pushCriteria(new RequestCriteria($request));
        $this->hotelRepository->pushCriteria(new LimitOffsetCriteria($request));
        $hotels = $this->hotelRepository->all();

        return $this->sendResponse($hotels->toArray(), 'Hotels retrieved successfully');
    }

    /**
     * Store a newly created Hotel in storage.
     * POST /hotels
     *
     * @param CreateHotelAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateHotelAPIRequest $request)
    {
        $input = $request->all();

        $hotels = $this->hotelRepository->create($input);

        return $this->sendResponse($hotels->toArray(), 'Hotel saved successfully');
    }

    /**
     * Display the specified Hotel.
     * GET|HEAD /hotels/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Hotel $hotel */
        $hotel = $this->hotelRepository->findWithoutFail($id);

        if (empty($hotel)) {
            return $this->sendError('Hotel not found');
        }

        return $this->sendResponse($hotel->toArray(), 'Hotel retrieved successfully');
    }

    /**
     * Update the specified Hotel in storage.
     * PUT/PATCH /hotels/{id}
     *
     * @param  int $id
     * @param UpdateHotelAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateHotelAPIRequest $request)
    {
        $input = $request->all();

        /** @var Hotel $hotel */
        $hotel = $this->hotelRepository->findWithoutFail($id);

        if (empty($hotel)) {
            return $this->sendError('Hotel not found');
        }

        $hotel = $this->hotelRepository->update($input, $id);

        return $this->sendResponse($hotel->toArray(), 'Hotel updated successfully');
    }

    /**
     * Remove the specified Hotel from storage.
     * DELETE /hotels/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Hotel $hotel */
        $hotel = $this->hotelRepository->findWithoutFail($id);

        if (empty($hotel)) {
            return $this->sendError('Hotel not found');
        }

        $hotel->delete();

        return $this->sendResponse($id, 'Hotel deleted successfully');
    }
}
