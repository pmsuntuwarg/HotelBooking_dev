<?php

namespace App\Http\Controllers\API\Admin;

use App\Http\Requests\API\Admin\CreateRoomStatusAPIRequest;
use App\Http\Requests\API\Admin\UpdateRoomStatusAPIRequest;
use App\Models\Admin\RoomStatus;
use App\Repositories\Admin\RoomStatusRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class RoomStatusController
 * @package App\Http\Controllers\API\Admin
 */

class RoomStatusAPIController extends AppBaseController
{
    /** @var  RoomStatusRepository */
    private $roomStatusRepository;

    public function __construct(RoomStatusRepository $roomStatusRepo)
    {
        $this->roomStatusRepository = $roomStatusRepo;
    }

    /**
     * Display a listing of the RoomStatus.
     * GET|HEAD /roomStatuses
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->roomStatusRepository->pushCriteria(new RequestCriteria($request));
        $this->roomStatusRepository->pushCriteria(new LimitOffsetCriteria($request));
        $roomStatuses = $this->roomStatusRepository->all();

        return $this->sendResponse($roomStatuses->toArray(), 'Room Statuses retrieved successfully');
    }

    /**
     * Store a newly created RoomStatus in storage.
     * POST /roomStatuses
     *
     * @param CreateRoomStatusAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateRoomStatusAPIRequest $request)
    {
        $input = $request->all();

        $roomStatuses = $this->roomStatusRepository->create($input);

        return $this->sendResponse($roomStatuses->toArray(), 'Room Status saved successfully');
    }

    /**
     * Display the specified RoomStatus.
     * GET|HEAD /roomStatuses/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var RoomStatus $roomStatus */
        $roomStatus = $this->roomStatusRepository->findWithoutFail($id);

        if (empty($roomStatus)) {
            return $this->sendError('Room Status not found');
        }

        return $this->sendResponse($roomStatus->toArray(), 'Room Status retrieved successfully');
    }

    /**
     * Update the specified RoomStatus in storage.
     * PUT/PATCH /roomStatuses/{id}
     *
     * @param  int $id
     * @param UpdateRoomStatusAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateRoomStatusAPIRequest $request)
    {
        $input = $request->all();

        /** @var RoomStatus $roomStatus */
        $roomStatus = $this->roomStatusRepository->findWithoutFail($id);

        if (empty($roomStatus)) {
            return $this->sendError('Room Status not found');
        }

        $roomStatus = $this->roomStatusRepository->update($input, $id);

        return $this->sendResponse($roomStatus->toArray(), 'RoomStatus updated successfully');
    }

    /**
     * Remove the specified RoomStatus from storage.
     * DELETE /roomStatuses/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var RoomStatus $roomStatus */
        $roomStatus = $this->roomStatusRepository->findWithoutFail($id);

        if (empty($roomStatus)) {
            return $this->sendError('Room Status not found');
        }

        $roomStatus->delete();

        return $this->sendResponse($id, 'Room Status deleted successfully');
    }
}
