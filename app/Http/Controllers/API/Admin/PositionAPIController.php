<?php

namespace App\Http\Controllers\API\Admin;

use App\Http\Requests\API\Admin\CreatePositionAPIRequest;
use App\Http\Requests\API\Admin\UpdatePositionAPIRequest;
use App\Models\Admin\Position;
use App\Repositories\Admin\PositionRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class PositionController
 * @package App\Http\Controllers\API\Admin
 */

class PositionAPIController extends AppBaseController
{
    /** @var  PositionRepository */
    private $positionRepository;

    public function __construct(PositionRepository $positionRepo)
    {
        $this->positionRepository = $positionRepo;
    }

    /**
     * Display a listing of the Position.
     * GET|HEAD /positions
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->positionRepository->pushCriteria(new RequestCriteria($request));
        $this->positionRepository->pushCriteria(new LimitOffsetCriteria($request));
        $positions = $this->positionRepository->all();

        return $this->sendResponse($positions->toArray(), 'Positions retrieved successfully');
    }

    /**
     * Store a newly created Position in storage.
     * POST /positions
     *
     * @param CreatePositionAPIRequest $request
     *
     * @return Response
     */
    public function store(CreatePositionAPIRequest $request)
    {
        $input = $request->all();

        $positions = $this->positionRepository->create($input);

        return $this->sendResponse($positions->toArray(), 'Position saved successfully');
    }

    /**
     * Display the specified Position.
     * GET|HEAD /positions/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Position $position */
        $position = $this->positionRepository->findWithoutFail($id);

        if (empty($position)) {
            return $this->sendError('Position not found');
        }

        return $this->sendResponse($position->toArray(), 'Position retrieved successfully');
    }

    /**
     * Update the specified Position in storage.
     * PUT/PATCH /positions/{id}
     *
     * @param  int $id
     * @param UpdatePositionAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePositionAPIRequest $request)
    {
        $input = $request->all();

        /** @var Position $position */
        $position = $this->positionRepository->findWithoutFail($id);

        if (empty($position)) {
            return $this->sendError('Position not found');
        }

        $position = $this->positionRepository->update($input, $id);

        return $this->sendResponse($position->toArray(), 'Position updated successfully');
    }

    /**
     * Remove the specified Position from storage.
     * DELETE /positions/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Position $position */
        $position = $this->positionRepository->findWithoutFail($id);

        if (empty($position)) {
            return $this->sendError('Position not found');
        }

        $position->delete();

        return $this->sendResponse($id, 'Position deleted successfully');
    }
}
