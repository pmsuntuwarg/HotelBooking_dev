<?php

namespace App\Http\Controllers\API\Admin;

use App\Http\Requests\API\Admin\CreateRoomTypeAPIRequest;
use App\Http\Requests\API\Admin\UpdateRoomTypeAPIRequest;
use App\Models\Admin\RoomType;
use App\Repositories\Admin\RoomTypeRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class RoomTypeController
 * @package App\Http\Controllers\API\Admin
 */

class RoomTypeAPIController extends AppBaseController
{
    /** @var  RoomTypeRepository */
    private $roomTypeRepository;

    public function __construct(RoomTypeRepository $roomTypeRepo)
    {
        $this->roomTypeRepository = $roomTypeRepo;
    }

    /**
     * Display a listing of the RoomType.
     * GET|HEAD /roomTypes
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->roomTypeRepository->pushCriteria(new RequestCriteria($request));
        $this->roomTypeRepository->pushCriteria(new LimitOffsetCriteria($request));
        $roomTypes = $this->roomTypeRepository->all();

        return $this->sendResponse($roomTypes->toArray(), 'Room Types retrieved successfully');
    }

    /**
     * Store a newly created RoomType in storage.
     * POST /roomTypes
     *
     * @param CreateRoomTypeAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateRoomTypeAPIRequest $request)
    {
        $input = $request->all();

        $roomTypes = $this->roomTypeRepository->create($input);

        return $this->sendResponse($roomTypes->toArray(), 'Room Type saved successfully');
    }

    /**
     * Display the specified RoomType.
     * GET|HEAD /roomTypes/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var RoomType $roomType */
        $roomType = $this->roomTypeRepository->findWithoutFail($id);

        if (empty($roomType)) {
            return $this->sendError('Room Type not found');
        }

        return $this->sendResponse($roomType->toArray(), 'Room Type retrieved successfully');
    }

    /**
     * Update the specified RoomType in storage.
     * PUT/PATCH /roomTypes/{id}
     *
     * @param  int $id
     * @param UpdateRoomTypeAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateRoomTypeAPIRequest $request)
    {
        $input = $request->all();

        /** @var RoomType $roomType */
        $roomType = $this->roomTypeRepository->findWithoutFail($id);

        if (empty($roomType)) {
            return $this->sendError('Room Type not found');
        }

        $roomType = $this->roomTypeRepository->update($input, $id);

        return $this->sendResponse($roomType->toArray(), 'RoomType updated successfully');
    }

    /**
     * Remove the specified RoomType from storage.
     * DELETE /roomTypes/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var RoomType $roomType */
        $roomType = $this->roomTypeRepository->findWithoutFail($id);

        if (empty($roomType)) {
            return $this->sendError('Room Type not found');
        }

        $roomType->delete();

        return $this->sendResponse($id, 'Room Type deleted successfully');
    }
}
