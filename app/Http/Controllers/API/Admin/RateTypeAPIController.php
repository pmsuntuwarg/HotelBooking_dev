<?php

namespace App\Http\Controllers\API\Admin;

use App\Http\Requests\API\Admin\CreateRateTypeAPIRequest;
use App\Http\Requests\API\Admin\UpdateRateTypeAPIRequest;
use App\Models\Admin\RateType;
use App\Repositories\Admin\RateTypeRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class RateTypeController
 * @package App\Http\Controllers\API\Admin
 */

class RateTypeAPIController extends AppBaseController
{
    /** @var  RateTypeRepository */
    private $rateTypeRepository;

    public function __construct(RateTypeRepository $rateTypeRepo)
    {
        $this->rateTypeRepository = $rateTypeRepo;
    }

    /**
     * Display a listing of the RateType.
     * GET|HEAD /rateTypes
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->rateTypeRepository->pushCriteria(new RequestCriteria($request));
        $this->rateTypeRepository->pushCriteria(new LimitOffsetCriteria($request));
        $rateTypes = $this->rateTypeRepository->all();

        return $this->sendResponse($rateTypes->toArray(), 'Rate Types retrieved successfully');
    }

    /**
     * Store a newly created RateType in storage.
     * POST /rateTypes
     *
     * @param CreateRateTypeAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateRateTypeAPIRequest $request)
    {
        $input = $request->all();

        $rateTypes = $this->rateTypeRepository->create($input);

        return $this->sendResponse($rateTypes->toArray(), 'Rate Type saved successfully');
    }

    /**
     * Display the specified RateType.
     * GET|HEAD /rateTypes/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var RateType $rateType */
        $rateType = $this->rateTypeRepository->findWithoutFail($id);

        if (empty($rateType)) {
            return $this->sendError('Rate Type not found');
        }

        return $this->sendResponse($rateType->toArray(), 'Rate Type retrieved successfully');
    }

    /**
     * Update the specified RateType in storage.
     * PUT/PATCH /rateTypes/{id}
     *
     * @param  int $id
     * @param UpdateRateTypeAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateRateTypeAPIRequest $request)
    {
        $input = $request->all();

        /** @var RateType $rateType */
        $rateType = $this->rateTypeRepository->findWithoutFail($id);

        if (empty($rateType)) {
            return $this->sendError('Rate Type not found');
        }

        $rateType = $this->rateTypeRepository->update($input, $id);

        return $this->sendResponse($rateType->toArray(), 'RateType updated successfully');
    }

    /**
     * Remove the specified RateType from storage.
     * DELETE /rateTypes/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var RateType $rateType */
        $rateType = $this->rateTypeRepository->findWithoutFail($id);

        if (empty($rateType)) {
            return $this->sendError('Rate Type not found');
        }

        $rateType->delete();

        return $this->sendResponse($id, 'Rate Type deleted successfully');
    }
}
