<?php

namespace App\Http\Controllers\API\Admin;

use App\Http\Requests\API\Admin\CreateReservationAgentAPIRequest;
use App\Http\Requests\API\Admin\UpdateReservationAgentAPIRequest;
use App\Models\Admin\ReservationAgent;
use App\Repositories\Admin\ReservationAgentRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class ReservationAgentController
 * @package App\Http\Controllers\API\Admin
 */

class ReservationAgentAPIController extends AppBaseController
{
    /** @var  ReservationAgentRepository */
    private $reservationAgentRepository;

    public function __construct(ReservationAgentRepository $reservationAgentRepo)
    {
        $this->reservationAgentRepository = $reservationAgentRepo;
    }

    /**
     * Display a listing of the ReservationAgent.
     * GET|HEAD /reservationAgents
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->reservationAgentRepository->pushCriteria(new RequestCriteria($request));
        $this->reservationAgentRepository->pushCriteria(new LimitOffsetCriteria($request));
        $reservationAgents = $this->reservationAgentRepository->all();

        return $this->sendResponse($reservationAgents->toArray(), 'Reservation Agents retrieved successfully');
    }

    /**
     * Store a newly created ReservationAgent in storage.
     * POST /reservationAgents
     *
     * @param CreateReservationAgentAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateReservationAgentAPIRequest $request)
    {
        $input = $request->all();

        $reservationAgents = $this->reservationAgentRepository->create($input);

        return $this->sendResponse($reservationAgents->toArray(), 'Reservation Agent saved successfully');
    }

    /**
     * Display the specified ReservationAgent.
     * GET|HEAD /reservationAgents/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var ReservationAgent $reservationAgent */
        $reservationAgent = $this->reservationAgentRepository->findWithoutFail($id);

        if (empty($reservationAgent)) {
            return $this->sendError('Reservation Agent not found');
        }

        return $this->sendResponse($reservationAgent->toArray(), 'Reservation Agent retrieved successfully');
    }

    /**
     * Update the specified ReservationAgent in storage.
     * PUT/PATCH /reservationAgents/{id}
     *
     * @param  int $id
     * @param UpdateReservationAgentAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateReservationAgentAPIRequest $request)
    {
        $input = $request->all();

        /** @var ReservationAgent $reservationAgent */
        $reservationAgent = $this->reservationAgentRepository->findWithoutFail($id);

        if (empty($reservationAgent)) {
            return $this->sendError('Reservation Agent not found');
        }

        $reservationAgent = $this->reservationAgentRepository->update($input, $id);

        return $this->sendResponse($reservationAgent->toArray(), 'ReservationAgent updated successfully');
    }

    /**
     * Remove the specified ReservationAgent from storage.
     * DELETE /reservationAgents/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var ReservationAgent $reservationAgent */
        $reservationAgent = $this->reservationAgentRepository->findWithoutFail($id);

        if (empty($reservationAgent)) {
            return $this->sendError('Reservation Agent not found');
        }

        $reservationAgent->delete();

        return $this->sendResponse($id, 'Reservation Agent deleted successfully');
    }
}
