<?php

namespace App\Http\Controllers\API\Admin;

use App\Http\Requests\API\Admin\CreateRoomBookedAPIRequest;
use App\Http\Requests\API\Admin\UpdateRoomBookedAPIRequest;
use App\Models\Admin\RoomBooked;
use App\Repositories\Admin\RoomBookedRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class RoomBookedController
 * @package App\Http\Controllers\API\Admin
 */

class RoomBookedAPIController extends AppBaseController
{
    /** @var  RoomBookedRepository */
    private $roomBookedRepository;

    public function __construct(RoomBookedRepository $roomBookedRepo)
    {
        $this->roomBookedRepository = $roomBookedRepo;
    }

    /**
     * Display a listing of the RoomBooked.
     * GET|HEAD /roomBookeds
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->roomBookedRepository->pushCriteria(new RequestCriteria($request));
        $this->roomBookedRepository->pushCriteria(new LimitOffsetCriteria($request));
        $roomBookeds = $this->roomBookedRepository->all();

        return $this->sendResponse($roomBookeds->toArray(), 'Room Bookeds retrieved successfully');
    }

    /**
     * Store a newly created RoomBooked in storage.
     * POST /roomBookeds
     *
     * @param CreateRoomBookedAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateRoomBookedAPIRequest $request)
    {
        $input = $request->all();

        $roomBookeds = $this->roomBookedRepository->create($input);

        return $this->sendResponse($roomBookeds->toArray(), 'Room Booked saved successfully');
    }

    /**
     * Display the specified RoomBooked.
     * GET|HEAD /roomBookeds/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var RoomBooked $roomBooked */
        $roomBooked = $this->roomBookedRepository->findWithoutFail($id);

        if (empty($roomBooked)) {
            return $this->sendError('Room Booked not found');
        }

        return $this->sendResponse($roomBooked->toArray(), 'Room Booked retrieved successfully');
    }

    /**
     * Update the specified RoomBooked in storage.
     * PUT/PATCH /roomBookeds/{id}
     *
     * @param  int $id
     * @param UpdateRoomBookedAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateRoomBookedAPIRequest $request)
    {
        $input = $request->all();

        /** @var RoomBooked $roomBooked */
        $roomBooked = $this->roomBookedRepository->findWithoutFail($id);

        if (empty($roomBooked)) {
            return $this->sendError('Room Booked not found');
        }

        $roomBooked = $this->roomBookedRepository->update($input, $id);

        return $this->sendResponse($roomBooked->toArray(), 'RoomBooked updated successfully');
    }

    /**
     * Remove the specified RoomBooked from storage.
     * DELETE /roomBookeds/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var RoomBooked $roomBooked */
        $roomBooked = $this->roomBookedRepository->findWithoutFail($id);

        if (empty($roomBooked)) {
            return $this->sendError('Room Booked not found');
        }

        $roomBooked->delete();

        return $this->sendResponse($id, 'Room Booked deleted successfully');
    }
}
