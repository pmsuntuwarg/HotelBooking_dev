<?php

namespace App\Http\Controllers\API\Admin;

use App\Http\Requests\API\Admin\CreateBookingStatusAPIRequest;
use App\Http\Requests\API\Admin\UpdateBookingStatusAPIRequest;
use App\Models\Admin\BookingStatus;
use App\Repositories\Admin\BookingStatusRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class BookingStatusController
 * @package App\Http\Controllers\API\Admin
 */

class BookingStatusAPIController extends AppBaseController
{
    /** @var  BookingStatusRepository */
    private $bookingStatusRepository;

    public function __construct(BookingStatusRepository $bookingStatusRepo)
    {
        $this->bookingStatusRepository = $bookingStatusRepo;
    }

    /**
     * Display a listing of the BookingStatus.
     * GET|HEAD /bookingStatuses
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->bookingStatusRepository->pushCriteria(new RequestCriteria($request));
        $this->bookingStatusRepository->pushCriteria(new LimitOffsetCriteria($request));
        $bookingStatuses = $this->bookingStatusRepository->all();

        return $this->sendResponse($bookingStatuses->toArray(), 'Booking Statuses retrieved successfully');
    }

    /**
     * Store a newly created BookingStatus in storage.
     * POST /bookingStatuses
     *
     * @param CreateBookingStatusAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateBookingStatusAPIRequest $request)
    {
        $input = $request->all();

        $bookingStatuses = $this->bookingStatusRepository->create($input);

        return $this->sendResponse($bookingStatuses->toArray(), 'Booking Status saved successfully');
    }

    /**
     * Display the specified BookingStatus.
     * GET|HEAD /bookingStatuses/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var BookingStatus $bookingStatus */
        $bookingStatus = $this->bookingStatusRepository->findWithoutFail($id);

        if (empty($bookingStatus)) {
            return $this->sendError('Booking Status not found');
        }

        return $this->sendResponse($bookingStatus->toArray(), 'Booking Status retrieved successfully');
    }

    /**
     * Update the specified BookingStatus in storage.
     * PUT/PATCH /bookingStatuses/{id}
     *
     * @param  int $id
     * @param UpdateBookingStatusAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateBookingStatusAPIRequest $request)
    {
        $input = $request->all();

        /** @var BookingStatus $bookingStatus */
        $bookingStatus = $this->bookingStatusRepository->findWithoutFail($id);

        if (empty($bookingStatus)) {
            return $this->sendError('Booking Status not found');
        }

        $bookingStatus = $this->bookingStatusRepository->update($input, $id);

        return $this->sendResponse($bookingStatus->toArray(), 'BookingStatus updated successfully');
    }

    /**
     * Remove the specified BookingStatus from storage.
     * DELETE /bookingStatuses/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var BookingStatus $bookingStatus */
        $bookingStatus = $this->bookingStatusRepository->findWithoutFail($id);

        if (empty($bookingStatus)) {
            return $this->sendError('Booking Status not found');
        }

        $bookingStatus->delete();

        return $this->sendResponse($id, 'Booking Status deleted successfully');
    }
}
