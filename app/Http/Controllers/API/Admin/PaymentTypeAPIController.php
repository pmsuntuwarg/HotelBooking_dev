<?php

namespace App\Http\Controllers\API\Admin;

use App\Http\Requests\API\Admin\CreatePaymentTypeAPIRequest;
use App\Http\Requests\API\Admin\UpdatePaymentTypeAPIRequest;
use App\Models\Admin\PaymentType;
use App\Repositories\Admin\PaymentTypeRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class PaymentTypeController
 * @package App\Http\Controllers\API\Admin
 */

class PaymentTypeAPIController extends AppBaseController
{
    /** @var  PaymentTypeRepository */
    private $paymentTypeRepository;

    public function __construct(PaymentTypeRepository $paymentTypeRepo)
    {
        $this->paymentTypeRepository = $paymentTypeRepo;
    }

    /**
     * Display a listing of the PaymentType.
     * GET|HEAD /paymentTypes
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->paymentTypeRepository->pushCriteria(new RequestCriteria($request));
        $this->paymentTypeRepository->pushCriteria(new LimitOffsetCriteria($request));
        $paymentTypes = $this->paymentTypeRepository->all();

        return $this->sendResponse($paymentTypes->toArray(), 'Payment Types retrieved successfully');
    }

    /**
     * Store a newly created PaymentType in storage.
     * POST /paymentTypes
     *
     * @param CreatePaymentTypeAPIRequest $request
     *
     * @return Response
     */
    public function store(CreatePaymentTypeAPIRequest $request)
    {
        $input = $request->all();

        $paymentTypes = $this->paymentTypeRepository->create($input);

        return $this->sendResponse($paymentTypes->toArray(), 'Payment Type saved successfully');
    }

    /**
     * Display the specified PaymentType.
     * GET|HEAD /paymentTypes/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var PaymentType $paymentType */
        $paymentType = $this->paymentTypeRepository->findWithoutFail($id);

        if (empty($paymentType)) {
            return $this->sendError('Payment Type not found');
        }

        return $this->sendResponse($paymentType->toArray(), 'Payment Type retrieved successfully');
    }

    /**
     * Update the specified PaymentType in storage.
     * PUT/PATCH /paymentTypes/{id}
     *
     * @param  int $id
     * @param UpdatePaymentTypeAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePaymentTypeAPIRequest $request)
    {
        $input = $request->all();

        /** @var PaymentType $paymentType */
        $paymentType = $this->paymentTypeRepository->findWithoutFail($id);

        if (empty($paymentType)) {
            return $this->sendError('Payment Type not found');
        }

        $paymentType = $this->paymentTypeRepository->update($input, $id);

        return $this->sendResponse($paymentType->toArray(), 'PaymentType updated successfully');
    }

    /**
     * Remove the specified PaymentType from storage.
     * DELETE /paymentTypes/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var PaymentType $paymentType */
        $paymentType = $this->paymentTypeRepository->findWithoutFail($id);

        if (empty($paymentType)) {
            return $this->sendError('Payment Type not found');
        }

        $paymentType->delete();

        return $this->sendResponse($id, 'Payment Type deleted successfully');
    }
}
