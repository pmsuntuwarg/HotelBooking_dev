<?php

namespace App\Models\Admin;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class ReservationAgent
 * @package App\Models\Admin
 * @version March 27, 2018, 4:09 pm UTC
 *
 * @property string raFirstName
 * @property string raLastName
 * @property mediumText raAddress
 * @property mediumText raAddress2
 * @property string raCity
 * @property string raState
 * @property string raZipCode
 * @property string raCountry
 * @property string raHomePhoneNumber
 * @property string raCellularNumber
 * @property string aMailAddress
 * @property string raGender
 */
class ReservationAgent extends Model
{
    use SoftDeletes;

    public $table = 'reservation_agents';
    

    protected $dates = ['deleted_at'];

    protected $primaryKey = 'raReservationAgentID';

    public $fillable = [
        'raFirstName',
        'raLastName',
        'raAddress',
        'raAddress2',
        'raCity',
        'raState',
        'raZipCode',
        'raCountry',
        'raHomePhoneNumber',
        'raCellularNumber',
        'aMailAddress',
        'raGender'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'raFirstName' => 'string',
        'raLastName' => 'string',
        'raCity' => 'string',
        'raState' => 'string',
        'raZipCode' => 'string',
        'raCountry' => 'string',
        'raHomePhoneNumber' => 'string',
        'raCellularNumber' => 'string',
        'aMailAddress' => 'string',
        'raGender' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'raFirstName' => 'required',
        'raLastName' => 'required',
        'raAddress' => 'required',
        'raCity' => 'required',
        'raState' => 'required',
        'raZipCode' => 'required',
        'raCountry' => 'required',
        'raCellularNumber' => 'raMailAddress string:unique email',
        'aMailAddress' => 'required',
        'raGender' => 'required'
    ];

    
}
