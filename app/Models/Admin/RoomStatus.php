<?php

namespace App\Models\Admin;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class RoomStatus
 * @package App\Models\Admin
 * @version March 27, 2018, 4:20 pm UTC
 *
 * @property string rsRoomStatus
 * @property string rsDescription
 * @property smallInteger rsSortOrder
 * @property smallInteger rsActive
 */
class RoomStatus extends Model
{
    use SoftDeletes;

    public $table = 'room_statuses';
    

    protected $dates = ['deleted_at'];

    protected $primaryKey = 'rtRoomStatusID';

    public $fillable = [
        'rsRoomStatus',
        'rsDescription',
        'rsSortOrder',
        'rsActive'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'rsRoomStatus' => 'string',
        'rsDescription' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'rsRoomStatus' => 'required',
        'rsDescription' => 'required',
        'rsSortOrder' => 'required',
        'rsActive' => 'required'
    ];

    public function room(){
        return $this->belongsTo(Room::class);
    }
}
