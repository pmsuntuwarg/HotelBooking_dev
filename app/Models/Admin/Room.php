<?php

namespace App\Models\Admin;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Room
 * @package App\Models
 * @version March 28, 2018, 1:08 am UTC
 *
 * @property unsignedInteger rHotelID
 * @property string rFloor
 * @property unsignedInteger rRoomType
 * @property smallInteger rRoomNumber
 * @property string rDescription
 * @property unsignedInteger rRoomStatusID
 */
class Room extends Model
{
    use SoftDeletes;

    public $table = 'rooms';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'rHotelID',
        'rFloor',
        'rRoomTypeID',
        'rRoomNumber',
        'rDescription',
        'rRoomStatusID'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'rFloor' => 'string',
        'rDescription' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'rHotelID' => 'required',
        'rFloor' => 'required',
        'rRoomTypeID' => 'required',
        'rRoomNumber' => 'required',
        'rDescription' => 'required',
        'rRoomStatusID' => 'required'
    ];

    public function hotel(){
        return $this->belongsTo(Hotel::class,'rHotelID','hHotelID');
    }

    public function roomType(){
        return $this->hasOne(RoomType::class,'rtRoomTypeID','rRoomTypeID');
    }

    public function roomStatus(){
        return $this->hasOne(RoomStatus::class,'rsRoomStatusID','rRoomStatusID');
    }

    public function rate(){
        return $this->hasOne(Rate::class,'rRoomID','rRoomID');
    }
}
