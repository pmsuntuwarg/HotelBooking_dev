<?php

namespace App\Models\Admin;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Staff
 * @package App\Models\Admin
 * @version March 27, 2018, 11:59 pm UTC
 *
 * @property \App\Models\Admin\Position position
 * @property string sFirstName
 * @property string sLastName
 * @property integer sPositionID
 * @property string sAddress
 * @property string sAddress2
 * @property string sCity
 * @property string sState
 * @property string sZipCode
 * @property string sCountry
 * @property string sHomePhoneNumber
 * @property string sCellularNumber
 * @property string sMailAddress
 * @property string sGender
 */
class Staff extends Model
{
    use SoftDeletes;

    public $table = 'staff';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'sFirstName',
        'sLastName',
        'sPositionID',
        'sAddress',
        'sAddress2',
        'sCity',
        'sState',
        'sZipCode',
        'sCountry',
        'sHomePhoneNumber',
        'sCellularNumber',
        'sMailAddress',
        'sGender'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'sStaffID' => 'integer',
        'sFirstName' => 'string',
        'sLastName' => 'string',
        'sPositionID' => 'integer',
        'sAddress' => 'string',
        'sAddress2' => 'string',
        'sCity' => 'string',
        'sState' => 'string',
        'sZipCode' => 'string',
        'sCountry' => 'string',
        'sHomePhoneNumber' => 'string',
        'sCellularNumber' => 'string',
        'sMailAddress' => 'string',
        'sGender' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function position()
    {
        return $this->belongsTo(\App\Models\Admin\Position::class);
    }
}
