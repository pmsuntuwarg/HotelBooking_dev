<?php

namespace App\Models\Admin;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class RateType
 * @package App\Models\Admin
 * @version March 27, 2018, 4:30 pm UTC
 *
 * @property string rtRateType
 * @property string rtDescription
 * @property smallInteger rtSortOrder
 * @property smallInteger rtActive
 */
class RateType extends Model
{
    use SoftDeletes;

    public $table = 'rate_types';
    

    protected $dates = ['deleted_at'];

    protected $primaryKey = 'rtRateTypeTypeID';

    public $fillable = [
        'rtRateType',
        'rtDescription',
        'rtSortOrder',
        'rtActive'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'rtRateType' => 'string',
        'rtDescription' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'rtRateType' => 'required',
        'rtDescription' => 'required',
        'rtSortOrder' => 'required',
        'rtActive' => 'required'
    ];

    public function rate(){
        return $this->hasOne(Rate::class,'rRateTypeID','rtRateTypeID');
    }
}
