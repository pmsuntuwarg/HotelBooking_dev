<?php

namespace App\Models\Admin;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class PaymentType
 * @package App\Models\Admin
 * @version March 27, 2018, 4:27 pm UTC
 *
 * @property string ptPaymentType
 * @property smallInteger ptSortOrder
 * @property smallInteger ptActive
 */
class PaymentType extends Model
{
    use SoftDeletes;

    public $table = 'payment_types';
    

    protected $dates = ['deleted_at'];

    protected $primaryKey = 'ptPaymentTypeID';

    public $fillable = [
        'ptPaymentType',
        'ptSortOrder',
        'ptActive'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'ptPaymentType' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'ptPaymentType' => 'required',
        'ptSortOrder' => 'required',
        'ptActive' => 'required'
    ];

    
}
