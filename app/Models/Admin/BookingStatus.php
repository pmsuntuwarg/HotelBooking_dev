<?php

namespace App\Models\Admin;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class BookingStatus
 * @package App\Models\Admin
 * @version March 27, 2018, 4:01 pm UTC
 *
 * @property tinyInt bsStatus
 * @property string bsDescription
 * @property tinyInt bsSortOrder
 * @property tinyInt bsActive
 */
class BookingStatus extends Model
{
    use SoftDeletes;

    public $table = 'booking_statuses';
    

    protected $dates = ['deleted_at'];

    protected $primaryKey = 'bsBokingStatusID';

    public $fillable = [
        'bsStatus',
        'bsDescription',
        'bsSortOrder',
        'bsActive'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'bsDescription' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'bsStatus' => 'required',
        'bsDescription' => 'required',
        'bsSortOrder' => 'required',
        'bsActive' => 'required'
    ];

    
}
