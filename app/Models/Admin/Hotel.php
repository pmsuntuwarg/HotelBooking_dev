<?php

namespace App\Models\Admin;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Hotel
 * @package App\Models\Admin
 * @version March 27, 2018, 3:55 pm UTC
 *
 * @property string hHotelCode
 * @property string hName
 * @property string hMotto
 * @property string hAddress
 * @property string hAddress2
 * @property string hCity
 * @property string hState
 * @property string hZipCode
 * @property string hMainPhoneNumber
 * @property string hFaxNumber
 * @property string hTollFreeNumber
 * @property string hCompanyMailingAddress
 * @property string hWebsiteAddress
 * @property string hMain
 */
class Hotel extends Model
{
    use SoftDeletes;

    public $table = 'hotels';
    

    protected $dates = ['deleted_at'];

    protected $primaryKey = 'hHotelID';

    public $fillable = [
        'hHotelCode',
        'hName',
        'hMotto',
        'hAddress',
        'hAddress2',
        'hCity',
        'hState',
        'hZipCode',
        'hMainPhoneNumber',
        'hFaxNumber',
        'hTollFreeNumber',
        'hCompanyMailingAddress',
        'hWebsiteAddress',
        'hMain'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'hHotelCode' => 'string',
        'hName' => 'string',
        'hMotto' => 'string',
        'hAddress' => 'string',
        'hAddress2' => 'string',
        'hCity' => 'string',
        'hState' => 'string',
        'hZipCode' => 'string',
        'hMainPhoneNumber' => 'string',
        'hFaxNumber' => 'string',
        'hTollFreeNumber' => 'string',
        'hCompanyMailingAddress' => 'string',
        'hWebsiteAddress' => 'string',
        'hMain' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'hHotelCode' => 'required',
        'hName' => 'required',
        'hMotto' => 'required',
        'hAddress' => 'required',
        'hCity' => 'required',
        'hState' => 'required',
        'hZipCode' => 'required',
        'hCompanyMailingAddress' => 'required',
        'hMain' => 'required'
    ];

    public function rooms(){

        return $this->hasMany(Room::class,'rHotelID','hHotelID');
    }
}
