<?php

namespace App\Models\Admin;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class RoomBooked
 * @package App\Models\Admin
 * @version March 28, 2018, 2:41 am UTC
 *
 * @property unsignedInteger rbBookingID
 * @property unsignedInteger rbRoomID
 * @property Integer rbRate
 */
class RoomBooked extends Model
{
    use SoftDeletes;

    public $table = 'room_booked';
    

    protected $dates = ['deleted_at'];

    protected $primaryKey = 'rbRoomBookedID';

    public $fillable = [
        'rbBookingID',
        'rbBookingID',
        'rbRoomID',
        'rbRate'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'rbBookingID' => 'required',
        'rbBookingID' => 'required',
        'rbRoomID' => 'required',
        'rbRate' => 'required'
    ];

    
}
