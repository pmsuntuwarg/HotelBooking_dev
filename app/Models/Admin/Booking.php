<?php

namespace App\Models\Admin;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Booking
 * @package App\Models\Admin
 * @version March 28, 2018, 2:48 am UTC
 *
 * @property unsignedInteger bHotelID
 * @property unsignedInteger bGuestID
 * @property unsignedInteger bReservationAgentID
 * @property date bDateFrom
 * @property date bDateTo
 * @property integer bRoomCount
 * @property unsignedInteger bBookingStatusID
 */
class Booking extends Model
{
    use SoftDeletes;

    public $table = 'bookings';
    

    protected $dates = ['deleted_at'];

    protected $primaryKey = 'bBookingID';

    public $fillable = [
        'bHotelID',
        'bGuestID',
        'bReservationAgentID',
        'bDateFrom',
        'bDateTo',
        'bRoomCount',
        'bBookingStatusID'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'bDateFrom' => 'date',
        'bDateTo' => 'date',
        'bRoomCount' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'bHotelID' => 'required',
        'bGuestID' => 'required',
        'bReservationAgentID' => 'required',
        'bDateFrom' => 'required',
        'bDateTo' => 'required',
        'bRoomCount' => 'required',
        'bBookingStatusID' => 'required'
    ];

    
}
