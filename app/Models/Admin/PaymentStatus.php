<?php

namespace App\Models\Admin;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class PaymentStatus
 * @package App\Models\Admin
 * @version March 27, 2018, 4:26 pm UTC
 *
 * @property smallInteger psStatus
 * @property string psDescription
 * @property smallInteger psSortOrder
 * @property smallInteger psActive
 */
class PaymentStatus extends Model
{
    use SoftDeletes;

    public $table = 'payment_statuses';
    

    protected $dates = ['deleted_at'];

    protected $primaryKey = 'psPaymentStatusID';

    public $fillable = [
        'psStatus',
        'psDescription',
        'psSortOrder',
        'psActive'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'psDescription' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'psStatus' => 'required',
        'psDescription' => 'required',
        'psSortOrder' => 'required',
        'psActive' => 'required'
    ];

    
}
