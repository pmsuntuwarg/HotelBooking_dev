<?php

namespace App\Models\Admin;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Guest
 * @package App\Models\Admin
 * @version March 27, 2018, 4:14 pm UTC
 *
 * @property string gFirstName
 * @property string gLastName
 * @property mediumText gAddress
 * @property mediumText gAddress2
 * @property string gCity
 * @property string gState
 * @property string gZipCode
 * @property string gCountry
 * @property string gHomePhoneNumber
 * @property string gCellularNumber
 * @property string gMailAddress
 * @property string gGender
 */
class Guest extends Model
{
    use SoftDeletes;

    public $table = 'guests';
    

    protected $dates = ['deleted_at'];

    protected $primaryKey = 'gGuestID';

    public $fillable = [
        'gFirstName',
        'gLastName',
        'gAddress',
        'gAddress2',
        'gCity',
        'gState',
        'gZipCode',
        'gCountry',
        'gHomePhoneNumber',
        'gCellularNumber',
        'gMailAddress',
        'gGender'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'gFirstName' => 'string',
        'gLastName' => 'string',
        'gCity' => 'string',
        'gState' => 'string',
        'gZipCode' => 'string',
        'gCountry' => 'string',
        'gHomePhoneNumber' => 'string',
        'gCellularNumber' => 'string',
        'gMailAddress' => 'string',
        'gGender' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'gFirstName' => 'required',
        'gLastName' => 'required',
        'gAddress' => 'required',
        'gCity' => 'required',
        'gState' => 'required',
        'gZipCode' => 'required',
        'gCountry' => 'required',
        'gHomePhoneNumber' => 'required',
        'gMailAddress' => 'required',
        'gGender' => 'required'
    ];

    
}
