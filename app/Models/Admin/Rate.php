<?php

namespace App\Models\Admin;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Rate
 * @package App\Models\Admin
 * @version March 28, 2018, 2:17 am UTC
 *
 * @property unsignedInteger rRoomID
 * @property smallInteger rRate
 * @property date rFromDate
 * @property date rToDate
 * @property unsignedInteger rRateTypeID
 */
class Rate extends Model
{
    use SoftDeletes;

    public $table = 'rates';
    

    protected $dates = ['deleted_at'];

    protected $primaryKey = 'rRateID';

    public $fillable = [
        'rRoomID',
        'rRate',
        'rFromDate',
        'rToDate',
        'rRateTypeID'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'rFromDate' => 'date',
        'rToDate' => 'date'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'rRoomID' => 'required',
        'rRate' => 'required',
        'rFromDate' => 'required',
        'rToDate' => 'required',
        'rRateTypeID' => 'required'
    ];

    public function room(){
        return $this->belongsTo(Room::class, 'rRoomID','rRoomID');
    }

    public function rateType(){
        return $this->belongsTo(RateType::class,'rRateTypeID','rtRateTypeID');
    }
    
}
