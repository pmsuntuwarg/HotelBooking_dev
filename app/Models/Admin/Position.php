<?php

namespace App\Models\Admin;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Position
 * @package App\Models\Admin
 * @version March 27, 2018, 4:31 pm UTC
 *
 * @property string pPosition
 * @property smallInteger pSortOrder
 * @property smallInteger pActive
 */
class Position extends Model
{
    use SoftDeletes;

    public $table = 'positions';
    

    protected $dates = ['deleted_at'];

    protected $primaryKey = 'pPositionID';

    public $fillable = [
        'pPosition',
        'pSortOrder',
        'pActive'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'pPosition' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'pPosition' => 'required',
        'pSortOrder' => 'required',
        'pActive' => 'required'
    ];
    /**
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     **/
    public function staffs()
    {
        return $this->hasMany(\App\Models\Admin\Staff::class);
    }
    
}
