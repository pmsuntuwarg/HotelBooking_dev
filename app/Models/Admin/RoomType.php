<?php

namespace App\Models\Admin;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class RoomType
 * @package App\Models\Admin
 * @version March 27, 2018, 4:18 pm UTC
 *
 * @property string rtRoomType
 * @property string rtDescription
 * @property smallInteger rtSortOrder
 * @property smallInteger rtActive
 */
class RoomType extends Model
{
    use SoftDeletes;

    public $table = 'room_types';
    

    protected $dates = ['deleted_at'];

    protected $primaryKey = 'rtRoomTypeID';

    public $fillable = [
        'rtRoomType',
        'rtDescription',
        'rtSortOrder',
        'rtActive'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'rtRoomType' => 'string',
        'rtDescription' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'rtRoomType' => 'required',
        'rtDescription' => 'required',
        'rtSortOrder' => 'required',
        'rtActive' => 'required'
    ];
    public function room(){
        return $this->hasOne(Room::class,'rRoomID','rtRoomID');
    }
    
}
