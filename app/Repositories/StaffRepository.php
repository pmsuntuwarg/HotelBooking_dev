<?php

namespace App\Repositories;

use App\Models\Staff;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class StaffRepository
 * @package App\Repositories
 * @version March 27, 2018, 11:49 pm UTC
 *
 * @method Staff findWithoutFail($id, $columns = ['*'])
 * @method Staff find($id, $columns = ['*'])
 * @method Staff first($columns = ['*'])
*/
class StaffRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'sFirstName',
        'sLastName',
        'sPositionID',
        'sAddress',
        'sAddress2',
        'sCity',
        'sState',
        'sZipCode',
        'sCountry',
        'sHomePhoneNumber',
        'sCellularNumber',
        'sMailAddress',
        'sGender'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Staff::class;
    }
}
