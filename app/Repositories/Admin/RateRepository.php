<?php

namespace App\Repositories\Admin;

use App\Models\Admin\Rate;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class RateRepository
 * @package App\Repositories\Admin
 * @version March 28, 2018, 2:17 am UTC
 *
 * @method Rate findWithoutFail($id, $columns = ['*'])
 * @method Rate find($id, $columns = ['*'])
 * @method Rate first($columns = ['*'])
*/
class RateRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'rRoomID',
        'rRate',
        'rFromDate',
        'rToDate',
        'rRateTypeID'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Rate::class;
    }
}
