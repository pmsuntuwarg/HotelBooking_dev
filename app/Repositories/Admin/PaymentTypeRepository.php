<?php

namespace App\Repositories\Admin;

use App\Models\Admin\PaymentType;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class PaymentTypeRepository
 * @package App\Repositories\Admin
 * @version March 27, 2018, 4:27 pm UTC
 *
 * @method PaymentType findWithoutFail($id, $columns = ['*'])
 * @method PaymentType find($id, $columns = ['*'])
 * @method PaymentType first($columns = ['*'])
*/
class PaymentTypeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'ptPaymentType',
        'ptSortOrder',
        'ptActive'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return PaymentType::class;
    }
}
