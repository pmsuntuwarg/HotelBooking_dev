<?php

namespace App\Repositories\Admin;

use App\Models\Admin\BookingStatus;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class BookingStatusRepository
 * @package App\Repositories\Admin
 * @version March 27, 2018, 4:01 pm UTC
 *
 * @method BookingStatus findWithoutFail($id, $columns = ['*'])
 * @method BookingStatus find($id, $columns = ['*'])
 * @method BookingStatus first($columns = ['*'])
*/
class BookingStatusRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'bsStatus',
        'bsDescription',
        'bsSortOrder',
        'bsActive'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return BookingStatus::class;
    }
}
