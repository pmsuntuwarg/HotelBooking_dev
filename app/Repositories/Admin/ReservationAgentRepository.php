<?php

namespace App\Repositories\Admin;

use App\Models\Admin\ReservationAgent;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class ReservationAgentRepository
 * @package App\Repositories\Admin
 * @version March 27, 2018, 4:09 pm UTC
 *
 * @method ReservationAgent findWithoutFail($id, $columns = ['*'])
 * @method ReservationAgent find($id, $columns = ['*'])
 * @method ReservationAgent first($columns = ['*'])
*/
class ReservationAgentRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'raFirstName',
        'raLastName',
        'raAddress',
        'raAddress2',
        'raCity',
        'raState',
        'raZipCode',
        'raCountry',
        'raHomePhoneNumber',
        'raCellularNumber',
        'aMailAddress',
        'raGender'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ReservationAgent::class;
    }
}
