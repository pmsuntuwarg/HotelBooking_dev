<?php

namespace App\Repositories\Admin;

use App\Models\Admin\RoomBooked;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class RoomBookedRepository
 * @package App\Repositories\Admin
 * @version March 28, 2018, 2:41 am UTC
 *
 * @method RoomBooked findWithoutFail($id, $columns = ['*'])
 * @method RoomBooked find($id, $columns = ['*'])
 * @method RoomBooked first($columns = ['*'])
*/
class RoomBookedRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'rbBookingID',
        'rbBookingID',
        'rbRoomID',
        'rbRate'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return RoomBooked::class;
    }
}
