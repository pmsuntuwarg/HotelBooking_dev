<?php

namespace App\Repositories\Admin;

use App\Models\Admin\RateType;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class RateTypeRepository
 * @package App\Repositories\Admin
 * @version March 27, 2018, 4:30 pm UTC
 *
 * @method RateType findWithoutFail($id, $columns = ['*'])
 * @method RateType find($id, $columns = ['*'])
 * @method RateType first($columns = ['*'])
*/
class RateTypeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'rtRateType',
        'rtDescription',
        'rtSortOrder',
        'rtActive'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return RateType::class;
    }
}
