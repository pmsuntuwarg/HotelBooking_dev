<?php

namespace App\Repositories\Admin;

use App\Models\Admin\RoomType;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class RoomTypeRepository
 * @package App\Repositories\Admin
 * @version March 27, 2018, 4:18 pm UTC
 *
 * @method RoomType findWithoutFail($id, $columns = ['*'])
 * @method RoomType find($id, $columns = ['*'])
 * @method RoomType first($columns = ['*'])
*/
class RoomTypeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'rtRoomType',
        'rtDescription',
        'rtSortOrder',
        'rtActive'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return RoomType::class;
    }
}
