<?php

namespace App\Repositories\Admin;

use App\Models\Admin\Room;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class RoomRepository
 * @package App\Repositories\Admin
 * @version March 28, 2018, 1:11 am UTC
 *
 * @method Room findWithoutFail($id, $columns = ['*'])
 * @method Room find($id, $columns = ['*'])
 * @method Room first($columns = ['*'])
*/
class RoomRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'rHotelID',
        'rFloor',
        'rRoomTypeID',
        'rRoomNumber',
        'rDescription',
        'rRoomStatusID'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Room::class;
    }
}
