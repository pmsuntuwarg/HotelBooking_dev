<?php

namespace App\Repositories\Admin;

use App\Models\Admin\Hotel;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class HotelRepository
 * @package App\Repositories\Admin
 * @version March 27, 2018, 3:55 pm UTC
 *
 * @method Hotel findWithoutFail($id, $columns = ['*'])
 * @method Hotel find($id, $columns = ['*'])
 * @method Hotel first($columns = ['*'])
*/
class HotelRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'hHotelCode',
        'hName',
        'hMotto',
        'hAddress',
        'hAddress2',
        'hCity',
        'hState',
        'hZipCode',
        'hMainPhoneNumber',
        'hFaxNumber',
        'hTollFreeNumber',
        'hCompanyMailingAddress',
        'hWebsiteAddress',
        'hMain'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Hotel::class;
    }
}
