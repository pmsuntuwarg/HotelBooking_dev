<?php

namespace App\Repositories\Admin;

use App\Models\Admin\Guest;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class GuestRepository
 * @package App\Repositories\Admin
 * @version March 27, 2018, 4:14 pm UTC
 *
 * @method Guest findWithoutFail($id, $columns = ['*'])
 * @method Guest find($id, $columns = ['*'])
 * @method Guest first($columns = ['*'])
*/
class GuestRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'gFirstName',
        'gLastName',
        'gAddress',
        'gAddress2',
        'gCity',
        'gState',
        'gZipCode',
        'gCountry',
        'gHomePhoneNumber',
        'gCellularNumber',
        'gMailAddress',
        'gGender'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Guest::class;
    }
}
