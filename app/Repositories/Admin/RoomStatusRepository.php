<?php

namespace App\Repositories\Admin;

use App\Models\Admin\RoomStatus;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class RoomStatusRepository
 * @package App\Repositories\Admin
 * @version March 27, 2018, 4:20 pm UTC
 *
 * @method RoomStatus findWithoutFail($id, $columns = ['*'])
 * @method RoomStatus find($id, $columns = ['*'])
 * @method RoomStatus first($columns = ['*'])
*/
class RoomStatusRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'rsRoomStatus',
        'rsDescription',
        'rsSortOrder',
        'rsActive'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return RoomStatus::class;
    }
}
