<?php

namespace App\Repositories\Admin;

use App\Models\Admin\Position;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class PositionRepository
 * @package App\Repositories\Admin
 * @version March 27, 2018, 4:31 pm UTC
 *
 * @method Position findWithoutFail($id, $columns = ['*'])
 * @method Position find($id, $columns = ['*'])
 * @method Position first($columns = ['*'])
*/
class PositionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'pPosition',
        'pSortOrder',
        'pActive'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Position::class;
    }
}
