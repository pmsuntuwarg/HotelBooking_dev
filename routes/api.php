<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('admin/hotels', 'Admin\HotelAPIController@index');
Route::post('admin/hotels', 'Admin\HotelAPIController@store');
Route::get('admin/hotels/{hotels}', 'Admin\HotelAPIController@show');
Route::put('admin/hotels/{hotels}', 'Admin\HotelAPIController@update');
Route::patch('admin/hotels/{hotels}', 'Admin\HotelAPIController@update');
Route::delete('admin/hotels{hotels}', 'Admin\HotelAPIController@destroy');

Route::get('admin/booking_statuses', 'Admin\BookingStatusAPIController@index');
Route::post('admin/booking_statuses', 'Admin\BookingStatusAPIController@store');
Route::get('admin/booking_statuses/{booking_statuses}', 'Admin\BookingStatusAPIController@show');
Route::put('admin/booking_statuses/{booking_statuses}', 'Admin\BookingStatusAPIController@update');
Route::patch('admin/booking_statuses/{booking_statuses}', 'Admin\BookingStatusAPIController@update');
Route::delete('admin/booking_statuses{booking_statuses}', 'Admin\BookingStatusAPIController@destroy');

Route::get('admin/reservation_agents', 'Admin\ReservationAgentAPIController@index');
Route::post('admin/reservation_agents', 'Admin\ReservationAgentAPIController@store');
Route::get('admin/reservation_agents/{reservation_agents}', 'Admin\ReservationAgentAPIController@show');
Route::put('admin/reservation_agents/{reservation_agents}', 'Admin\ReservationAgentAPIController@update');
Route::patch('admin/reservation_agents/{reservation_agents}', 'Admin\ReservationAgentAPIController@update');
Route::delete('admin/reservation_agents{reservation_agents}', 'Admin\ReservationAgentAPIController@destroy');

Route::get('admin/guests', 'Admin\GuestAPIController@index');
Route::post('admin/guests', 'Admin\GuestAPIController@store');
Route::get('admin/guests/{guests}', 'Admin\GuestAPIController@show');
Route::put('admin/guests/{guests}', 'Admin\GuestAPIController@update');
Route::patch('admin/guests/{guests}', 'Admin\GuestAPIController@update');
Route::delete('admin/guests{guests}', 'Admin\GuestAPIController@destroy');

Route::get('admin/room_types', 'Admin\RoomTypeAPIController@index');
Route::post('admin/room_types', 'Admin\RoomTypeAPIController@store');
Route::get('admin/room_types/{room_types}', 'Admin\RoomTypeAPIController@show');
Route::put('admin/room_types/{room_types}', 'Admin\RoomTypeAPIController@update');
Route::patch('admin/room_types/{room_types}', 'Admin\RoomTypeAPIController@update');
Route::delete('admin/room_types{room_types}', 'Admin\RoomTypeAPIController@destroy');

Route::get('admin/room_statuses', 'Admin\RoomStatusAPIController@index');
Route::post('admin/room_statuses', 'Admin\RoomStatusAPIController@store');
Route::get('admin/room_statuses/{room_statuses}', 'Admin\RoomStatusAPIController@show');
Route::put('admin/room_statuses/{room_statuses}', 'Admin\RoomStatusAPIController@update');
Route::patch('admin/room_statuses/{room_statuses}', 'Admin\RoomStatusAPIController@update');
Route::delete('admin/room_statuses{room_statuses}', 'Admin\RoomStatusAPIController@destroy');

Route::get('admin/payment_statuses', 'Admin\PaymentStatusAPIController@index');
Route::post('admin/payment_statuses', 'Admin\PaymentStatusAPIController@store');
Route::get('admin/payment_statuses/{payment_statuses}', 'Admin\PaymentStatusAPIController@show');
Route::put('admin/payment_statuses/{payment_statuses}', 'Admin\PaymentStatusAPIController@update');
Route::patch('admin/payment_statuses/{payment_statuses}', 'Admin\PaymentStatusAPIController@update');
Route::delete('admin/payment_statuses{payment_statuses}', 'Admin\PaymentStatusAPIController@destroy');

Route::get('admin/payment_types', 'Admin\PaymentTypeAPIController@index');
Route::post('admin/payment_types', 'Admin\PaymentTypeAPIController@store');
Route::get('admin/payment_types/{payment_types}', 'Admin\PaymentTypeAPIController@show');
Route::put('admin/payment_types/{payment_types}', 'Admin\PaymentTypeAPIController@update');
Route::patch('admin/payment_types/{payment_types}', 'Admin\PaymentTypeAPIController@update');
Route::delete('admin/payment_types{payment_types}', 'Admin\PaymentTypeAPIController@destroy');

Route::get('admin/rate_types', 'Admin\RateTypeAPIController@index');
Route::post('admin/rate_types', 'Admin\RateTypeAPIController@store');
Route::get('admin/rate_types/{rate_types}', 'Admin\RateTypeAPIController@show');
Route::put('admin/rate_types/{rate_types}', 'Admin\RateTypeAPIController@update');
Route::patch('admin/rate_types/{rate_types}', 'Admin\RateTypeAPIController@update');
Route::delete('admin/rate_types{rate_types}', 'Admin\RateTypeAPIController@destroy');

Route::get('admin/positions', 'Admin\PositionAPIController@index');
Route::post('admin/positions', 'Admin\PositionAPIController@store');
Route::get('admin/positions/{positions}', 'Admin\PositionAPIController@show');
Route::put('admin/positions/{positions}', 'Admin\PositionAPIController@update');
Route::patch('admin/positions/{positions}', 'Admin\PositionAPIController@update');
Route::delete('admin/positions{positions}', 'Admin\PositionAPIController@destroy');

Route::get('admin/staff', 'Admin\StaffAPIController@index');
Route::post('admin/staff', 'Admin\StaffAPIController@store');
Route::get('admin/staff/{staff}', 'Admin\StaffAPIController@show');
Route::put('admin/staff/{staff}', 'Admin\StaffAPIController@update');
Route::patch('admin/staff/{staff}', 'Admin\StaffAPIController@update');
Route::delete('admin/staff{staff}', 'Admin\StaffAPIController@destroy');

Route::resource('staff', 'StaffAPIController');

Route::resource('staff', 'StaffAPIController');

Route::get('admin/staff', 'Admin\StaffAPIController@index');
Route::post('admin/staff', 'Admin\StaffAPIController@store');
Route::get('admin/staff/{staff}', 'Admin\StaffAPIController@show');
Route::put('admin/staff/{staff}', 'Admin\StaffAPIController@update');
Route::patch('admin/staff/{staff}', 'Admin\StaffAPIController@update');
Route::delete('admin/staff{staff}', 'Admin\StaffAPIController@destroy');

Route::get('admin/staff', 'Admin\StaffAPIController@index');
Route::post('admin/staff', 'Admin\StaffAPIController@store');
Route::get('admin/staff/{staff}', 'Admin\StaffAPIController@show');
Route::put('admin/staff/{staff}', 'Admin\StaffAPIController@update');
Route::patch('admin/staff/{staff}', 'Admin\StaffAPIController@update');
Route::delete('admin/staff{staff}', 'Admin\StaffAPIController@destroy');

Route::get('admin/staff', 'Admin\StaffAPIController@index');
Route::post('admin/staff', 'Admin\StaffAPIController@store');
Route::get('admin/staff/{staff}', 'Admin\StaffAPIController@show');
Route::put('admin/staff/{staff}', 'Admin\StaffAPIController@update');
Route::patch('admin/staff/{staff}', 'Admin\StaffAPIController@update');
Route::delete('admin/staff{staff}', 'Admin\StaffAPIController@destroy');

Route::get('admin/staff', 'Admin\StaffAPIController@index');
Route::post('admin/staff', 'Admin\StaffAPIController@store');
Route::get('admin/staff/{staff}', 'Admin\StaffAPIController@show');
Route::put('admin/staff/{staff}', 'Admin\StaffAPIController@update');
Route::patch('admin/staff/{staff}', 'Admin\StaffAPIController@update');
Route::delete('admin/staff{staff}', 'Admin\StaffAPIController@destroy');

Route::get('admin/rooms', 'Admin\RoomAPIController@index');
Route::post('admin/rooms', 'Admin\RoomAPIController@store');
Route::get('admin/rooms/{rooms}', 'Admin\RoomAPIController@show');
Route::put('admin/rooms/{rooms}', 'Admin\RoomAPIController@update');
Route::patch('admin/rooms/{rooms}', 'Admin\RoomAPIController@update');
Route::delete('admin/rooms{rooms}', 'Admin\RoomAPIController@destroy');

Route::get('admin/rates', 'Admin\RateAPIController@index');
Route::post('admin/rates', 'Admin\RateAPIController@store');
Route::get('admin/rates/{rates}', 'Admin\RateAPIController@show');
Route::put('admin/rates/{rates}', 'Admin\RateAPIController@update');
Route::patch('admin/rates/{rates}', 'Admin\RateAPIController@update');
Route::delete('admin/rates{rates}', 'Admin\RateAPIController@destroy');

Route::get('admin/room_bookeds', 'Admin\RoomBookedAPIController@index');
Route::post('admin/room_bookeds', 'Admin\RoomBookedAPIController@store');
Route::get('admin/room_bookeds/{room_bookeds}', 'Admin\RoomBookedAPIController@show');
Route::put('admin/room_bookeds/{room_bookeds}', 'Admin\RoomBookedAPIController@update');
Route::patch('admin/room_bookeds/{room_bookeds}', 'Admin\RoomBookedAPIController@update');
Route::delete('admin/room_bookeds{room_bookeds}', 'Admin\RoomBookedAPIController@destroy');

Route::get('admin/bookings', 'Admin\BookingAPIController@index');
Route::post('admin/bookings', 'Admin\BookingAPIController@store');
Route::get('admin/bookings/{bookings}', 'Admin\BookingAPIController@show');
Route::put('admin/bookings/{bookings}', 'Admin\BookingAPIController@update');
Route::patch('admin/bookings/{bookings}', 'Admin\BookingAPIController@update');
Route::delete('admin/bookings{bookings}', 'Admin\BookingAPIController@destroy');