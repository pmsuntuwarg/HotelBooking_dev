<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();

Route::get('/', 'HomeController@index');
Route::get('/home', 'HomeController@index');

Route::get('admin/hotels', ['as'=> 'admin.hotels.index', 'uses' => 'Admin\HotelController@index']);
Route::post('admin/hotels', ['as'=> 'admin.hotels.store', 'uses' => 'Admin\HotelController@store']);
Route::get('admin/hotels/create', ['as'=> 'admin.hotels.create', 'uses' => 'Admin\HotelController@create']);
Route::put('admin/hotels/{hotels}', ['as'=> 'admin.hotels.update', 'uses' => 'Admin\HotelController@update']);
Route::patch('admin/hotels/{hotels}', ['as'=> 'admin.hotels.update', 'uses' => 'Admin\HotelController@update']);
Route::delete('admin/hotels/{hotels}', ['as'=> 'admin.hotels.destroy', 'uses' => 'Admin\HotelController@destroy']);
Route::get('admin/hotels/{hotels}', ['as'=> 'admin.hotels.show', 'uses' => 'Admin\HotelController@show']);
Route::get('admin/hotels/{hotels}/edit', ['as'=> 'admin.hotels.edit', 'uses' => 'Admin\HotelController@edit']);


Route::get('admin/bookingStatuses', ['as'=> 'admin.bookingStatuses.index', 'uses' => 'Admin\BookingStatusController@index']);
Route::post('admin/bookingStatuses', ['as'=> 'admin.bookingStatuses.store', 'uses' => 'Admin\BookingStatusController@store']);
Route::get('admin/bookingStatuses/create', ['as'=> 'admin.bookingStatuses.create', 'uses' => 'Admin\BookingStatusController@create']);
Route::put('admin/bookingStatuses/{bookingStatuses}', ['as'=> 'admin.bookingStatuses.update', 'uses' => 'Admin\BookingStatusController@update']);
Route::patch('admin/bookingStatuses/{bookingStatuses}', ['as'=> 'admin.bookingStatuses.update', 'uses' => 'Admin\BookingStatusController@update']);
Route::delete('admin/bookingStatuses/{bookingStatuses}', ['as'=> 'admin.bookingStatuses.destroy', 'uses' => 'Admin\BookingStatusController@destroy']);
Route::get('admin/bookingStatuses/{bookingStatuses}', ['as'=> 'admin.bookingStatuses.show', 'uses' => 'Admin\BookingStatusController@show']);
Route::get('admin/bookingStatuses/{bookingStatuses}/edit', ['as'=> 'admin.bookingStatuses.edit', 'uses' => 'Admin\BookingStatusController@edit']);


Route::get('admin/reservationAgents', ['as'=> 'admin.reservationAgents.index', 'uses' => 'Admin\ReservationAgentController@index']);
Route::post('admin/reservationAgents', ['as'=> 'admin.reservationAgents.store', 'uses' => 'Admin\ReservationAgentController@store']);
Route::get('admin/reservationAgents/create', ['as'=> 'admin.reservationAgents.create', 'uses' => 'Admin\ReservationAgentController@create']);
Route::put('admin/reservationAgents/{reservationAgents}', ['as'=> 'admin.reservationAgents.update', 'uses' => 'Admin\ReservationAgentController@update']);
Route::patch('admin/reservationAgents/{reservationAgents}', ['as'=> 'admin.reservationAgents.update', 'uses' => 'Admin\ReservationAgentController@update']);
Route::delete('admin/reservationAgents/{reservationAgents}', ['as'=> 'admin.reservationAgents.destroy', 'uses' => 'Admin\ReservationAgentController@destroy']);
Route::get('admin/reservationAgents/{reservationAgents}', ['as'=> 'admin.reservationAgents.show', 'uses' => 'Admin\ReservationAgentController@show']);
Route::get('admin/reservationAgents/{reservationAgents}/edit', ['as'=> 'admin.reservationAgents.edit', 'uses' => 'Admin\ReservationAgentController@edit']);


Route::get('admin/guests', ['as'=> 'admin.guests.index', 'uses' => 'Admin\GuestController@index']);
Route::post('admin/guests', ['as'=> 'admin.guests.store', 'uses' => 'Admin\GuestController@store']);
Route::get('admin/guests/create', ['as'=> 'admin.guests.create', 'uses' => 'Admin\GuestController@create']);
Route::put('admin/guests/{guests}', ['as'=> 'admin.guests.update', 'uses' => 'Admin\GuestController@update']);
Route::patch('admin/guests/{guests}', ['as'=> 'admin.guests.update', 'uses' => 'Admin\GuestController@update']);
Route::delete('admin/guests/{guests}', ['as'=> 'admin.guests.destroy', 'uses' => 'Admin\GuestController@destroy']);
Route::get('admin/guests/{guests}', ['as'=> 'admin.guests.show', 'uses' => 'Admin\GuestController@show']);
Route::get('admin/guests/{guests}/edit', ['as'=> 'admin.guests.edit', 'uses' => 'Admin\GuestController@edit']);


Route::get('admin/roomTypes', ['as'=> 'admin.roomTypes.index', 'uses' => 'Admin\RoomTypeController@index']);
Route::post('admin/roomTypes', ['as'=> 'admin.roomTypes.store', 'uses' => 'Admin\RoomTypeController@store']);
Route::get('admin/roomTypes/create', ['as'=> 'admin.roomTypes.create', 'uses' => 'Admin\RoomTypeController@create']);
Route::put('admin/roomTypes/{roomTypes}', ['as'=> 'admin.roomTypes.update', 'uses' => 'Admin\RoomTypeController@update']);
Route::patch('admin/roomTypes/{roomTypes}', ['as'=> 'admin.roomTypes.update', 'uses' => 'Admin\RoomTypeController@update']);
Route::delete('admin/roomTypes/{roomTypes}', ['as'=> 'admin.roomTypes.destroy', 'uses' => 'Admin\RoomTypeController@destroy']);
Route::get('admin/roomTypes/{roomTypes}', ['as'=> 'admin.roomTypes.show', 'uses' => 'Admin\RoomTypeController@show']);
Route::get('admin/roomTypes/{roomTypes}/edit', ['as'=> 'admin.roomTypes.edit', 'uses' => 'Admin\RoomTypeController@edit']);


Route::get('admin/roomStatuses', ['as'=> 'admin.roomStatuses.index', 'uses' => 'Admin\RoomStatusController@index']);
Route::post('admin/roomStatuses', ['as'=> 'admin.roomStatuses.store', 'uses' => 'Admin\RoomStatusController@store']);
Route::get('admin/roomStatuses/create', ['as'=> 'admin.roomStatuses.create', 'uses' => 'Admin\RoomStatusController@create']);
Route::put('admin/roomStatuses/{roomStatuses}', ['as'=> 'admin.roomStatuses.update', 'uses' => 'Admin\RoomStatusController@update']);
Route::patch('admin/roomStatuses/{roomStatuses}', ['as'=> 'admin.roomStatuses.update', 'uses' => 'Admin\RoomStatusController@update']);
Route::delete('admin/roomStatuses/{roomStatuses}', ['as'=> 'admin.roomStatuses.destroy', 'uses' => 'Admin\RoomStatusController@destroy']);
Route::get('admin/roomStatuses/{roomStatuses}', ['as'=> 'admin.roomStatuses.show', 'uses' => 'Admin\RoomStatusController@show']);
Route::get('admin/roomStatuses/{roomStatuses}/edit', ['as'=> 'admin.roomStatuses.edit', 'uses' => 'Admin\RoomStatusController@edit']);


Route::get('admin/paymentStatuses', ['as'=> 'admin.paymentStatuses.index', 'uses' => 'Admin\PaymentStatusController@index']);
Route::post('admin/paymentStatuses', ['as'=> 'admin.paymentStatuses.store', 'uses' => 'Admin\PaymentStatusController@store']);
Route::get('admin/paymentStatuses/create', ['as'=> 'admin.paymentStatuses.create', 'uses' => 'Admin\PaymentStatusController@create']);
Route::put('admin/paymentStatuses/{paymentStatuses}', ['as'=> 'admin.paymentStatuses.update', 'uses' => 'Admin\PaymentStatusController@update']);
Route::patch('admin/paymentStatuses/{paymentStatuses}', ['as'=> 'admin.paymentStatuses.update', 'uses' => 'Admin\PaymentStatusController@update']);
Route::delete('admin/paymentStatuses/{paymentStatuses}', ['as'=> 'admin.paymentStatuses.destroy', 'uses' => 'Admin\PaymentStatusController@destroy']);
Route::get('admin/paymentStatuses/{paymentStatuses}', ['as'=> 'admin.paymentStatuses.show', 'uses' => 'Admin\PaymentStatusController@show']);
Route::get('admin/paymentStatuses/{paymentStatuses}/edit', ['as'=> 'admin.paymentStatuses.edit', 'uses' => 'Admin\PaymentStatusController@edit']);


Route::get('admin/paymentTypes', ['as'=> 'admin.paymentTypes.index', 'uses' => 'Admin\PaymentTypeController@index']);
Route::post('admin/paymentTypes', ['as'=> 'admin.paymentTypes.store', 'uses' => 'Admin\PaymentTypeController@store']);
Route::get('admin/paymentTypes/create', ['as'=> 'admin.paymentTypes.create', 'uses' => 'Admin\PaymentTypeController@create']);
Route::put('admin/paymentTypes/{paymentTypes}', ['as'=> 'admin.paymentTypes.update', 'uses' => 'Admin\PaymentTypeController@update']);
Route::patch('admin/paymentTypes/{paymentTypes}', ['as'=> 'admin.paymentTypes.update', 'uses' => 'Admin\PaymentTypeController@update']);
Route::delete('admin/paymentTypes/{paymentTypes}', ['as'=> 'admin.paymentTypes.destroy', 'uses' => 'Admin\PaymentTypeController@destroy']);
Route::get('admin/paymentTypes/{paymentTypes}', ['as'=> 'admin.paymentTypes.show', 'uses' => 'Admin\PaymentTypeController@show']);
Route::get('admin/paymentTypes/{paymentTypes}/edit', ['as'=> 'admin.paymentTypes.edit', 'uses' => 'Admin\PaymentTypeController@edit']);


Route::get('admin/rateTypes', ['as'=> 'admin.rateTypes.index', 'uses' => 'Admin\RateTypeController@index']);
Route::post('admin/rateTypes', ['as'=> 'admin.rateTypes.store', 'uses' => 'Admin\RateTypeController@store']);
Route::get('admin/rateTypes/create', ['as'=> 'admin.rateTypes.create', 'uses' => 'Admin\RateTypeController@create']);
Route::put('admin/rateTypes/{rateTypes}', ['as'=> 'admin.rateTypes.update', 'uses' => 'Admin\RateTypeController@update']);
Route::patch('admin/rateTypes/{rateTypes}', ['as'=> 'admin.rateTypes.update', 'uses' => 'Admin\RateTypeController@update']);
Route::delete('admin/rateTypes/{rateTypes}', ['as'=> 'admin.rateTypes.destroy', 'uses' => 'Admin\RateTypeController@destroy']);
Route::get('admin/rateTypes/{rateTypes}', ['as'=> 'admin.rateTypes.show', 'uses' => 'Admin\RateTypeController@show']);
Route::get('admin/rateTypes/{rateTypes}/edit', ['as'=> 'admin.rateTypes.edit', 'uses' => 'Admin\RateTypeController@edit']);


Route::get('admin/positions', ['as'=> 'admin.positions.index', 'uses' => 'Admin\PositionController@index']);
Route::post('admin/positions', ['as'=> 'admin.positions.store', 'uses' => 'Admin\PositionController@store']);
Route::get('admin/positions/create', ['as'=> 'admin.positions.create', 'uses' => 'Admin\PositionController@create']);
Route::put('admin/positions/{positions}', ['as'=> 'admin.positions.update', 'uses' => 'Admin\PositionController@update']);
Route::patch('admin/positions/{positions}', ['as'=> 'admin.positions.update', 'uses' => 'Admin\PositionController@update']);
Route::delete('admin/positions/{positions}', ['as'=> 'admin.positions.destroy', 'uses' => 'Admin\PositionController@destroy']);
Route::get('admin/positions/{positions}', ['as'=> 'admin.positions.show', 'uses' => 'Admin\PositionController@show']);
Route::get('admin/positions/{positions}/edit', ['as'=> 'admin.positions.edit', 'uses' => 'Admin\PositionController@edit']);

Route::get('admin/staff', ['as'=> 'admin.staff.index', 'uses' => 'Admin\StaffController@index']);
Route::post('admin/staff', ['as'=> 'admin.staff.store', 'uses' => 'Admin\StaffController@store']);
Route::get('admin/staff/create', ['as'=> 'admin.staff.create', 'uses' => 'Admin\StaffController@create']);
Route::put('admin/staff/{staff}', ['as'=> 'admin.staff.update', 'uses' => 'Admin\StaffController@update']);
Route::patch('admin/staff/{staff}', ['as'=> 'admin.staff.update', 'uses' => 'Admin\StaffController@update']);
Route::delete('admin/staff/{staff}', ['as'=> 'admin.staff.destroy', 'uses' => 'Admin\StaffController@destroy']);
Route::get('admin/staff/{staff}', ['as'=> 'admin.staff.show', 'uses' => 'Admin\StaffController@show']);
Route::get('admin/staff/{staff}/edit', ['as'=> 'admin.staff.edit', 'uses' => 'Admin\StaffController@edit']);


Route::get('admin/staff', ['as'=> 'admin.staff.index', 'uses' => 'Admin\StaffController@index']);
Route::post('admin/staff', ['as'=> 'admin.staff.store', 'uses' => 'Admin\StaffController@store']);
Route::get('admin/staff/create', ['as'=> 'admin.staff.create', 'uses' => 'Admin\StaffController@create']);
Route::put('admin/staff/{staff}', ['as'=> 'admin.staff.update', 'uses' => 'Admin\StaffController@update']);
Route::patch('admin/staff/{staff}', ['as'=> 'admin.staff.update', 'uses' => 'Admin\StaffController@update']);
Route::delete('admin/staff/{staff}', ['as'=> 'admin.staff.destroy', 'uses' => 'Admin\StaffController@destroy']);
Route::get('admin/staff/{staff}', ['as'=> 'admin.staff.show', 'uses' => 'Admin\StaffController@show']);
Route::get('admin/staff/{staff}/edit', ['as'=> 'admin.staff.edit', 'uses' => 'Admin\StaffController@edit']);


Route::get('admin/staff', ['as'=> 'admin.staff.index', 'uses' => 'Admin\StaffController@index']);
Route::post('admin/staff', ['as'=> 'admin.staff.store', 'uses' => 'Admin\StaffController@store']);
Route::get('admin/staff/create', ['as'=> 'admin.staff.create', 'uses' => 'Admin\StaffController@create']);
Route::put('admin/staff/{staff}', ['as'=> 'admin.staff.update', 'uses' => 'Admin\StaffController@update']);
Route::patch('admin/staff/{staff}', ['as'=> 'admin.staff.update', 'uses' => 'Admin\StaffController@update']);
Route::delete('admin/staff/{staff}', ['as'=> 'admin.staff.destroy', 'uses' => 'Admin\StaffController@destroy']);
Route::get('admin/staff/{staff}', ['as'=> 'admin.staff.show', 'uses' => 'Admin\StaffController@show']);
Route::get('admin/staff/{staff}/edit', ['as'=> 'admin.staff.edit', 'uses' => 'Admin\StaffController@edit']);


Route::get('admin/staff', ['as'=> 'admin.staff.index', 'uses' => 'Admin\StaffController@index']);
Route::post('admin/staff', ['as'=> 'admin.staff.store', 'uses' => 'Admin\StaffController@store']);
Route::get('admin/staff/create', ['as'=> 'admin.staff.create', 'uses' => 'Admin\StaffController@create']);
Route::put('admin/staff/{staff}', ['as'=> 'admin.staff.update', 'uses' => 'Admin\StaffController@update']);
Route::patch('admin/staff/{staff}', ['as'=> 'admin.staff.update', 'uses' => 'Admin\StaffController@update']);
Route::delete('admin/staff/{staff}', ['as'=> 'admin.staff.destroy', 'uses' => 'Admin\StaffController@destroy']);
Route::get('admin/staff/{staff}', ['as'=> 'admin.staff.show', 'uses' => 'Admin\StaffController@show']);
Route::get('admin/staff/{staff}/edit', ['as'=> 'admin.staff.edit', 'uses' => 'Admin\StaffController@edit']);


Route::get('admin/rooms', ['as'=> 'admin.rooms.index', 'uses' => 'Admin\RoomController@index']);
Route::post('admin/rooms', ['as'=> 'admin.rooms.store', 'uses' => 'Admin\RoomController@store']);
Route::get('admin/rooms/create', ['as'=> 'admin.rooms.create', 'uses' => 'Admin\RoomController@create']);
Route::put('admin/rooms/{rooms}', ['as'=> 'admin.rooms.update', 'uses' => 'Admin\RoomController@update']);
Route::patch('admin/rooms/{rooms}', ['as'=> 'admin.rooms.update', 'uses' => 'Admin\RoomController@update']);
Route::delete('admin/rooms/{rooms}', ['as'=> 'admin.rooms.destroy', 'uses' => 'Admin\RoomController@destroy']);
Route::get('admin/rooms/{rooms}', ['as'=> 'admin.rooms.show', 'uses' => 'Admin\RoomController@show']);
Route::get('admin/rooms/{rooms}/edit', ['as'=> 'admin.rooms.edit', 'uses' => 'Admin\RoomController@edit']);


Route::get('admin/rates', ['as'=> 'admin.rates.index', 'uses' => 'Admin\RateController@index']);
Route::post('admin/rates', ['as'=> 'admin.rates.store', 'uses' => 'Admin\RateController@store']);
Route::get('admin/rates/create', ['as'=> 'admin.rates.create', 'uses' => 'Admin\RateController@create']);
Route::put('admin/rates/{rates}', ['as'=> 'admin.rates.update', 'uses' => 'Admin\RateController@update']);
Route::patch('admin/rates/{rates}', ['as'=> 'admin.rates.update', 'uses' => 'Admin\RateController@update']);
Route::delete('admin/rates/{rates}', ['as'=> 'admin.rates.destroy', 'uses' => 'Admin\RateController@destroy']);
Route::get('admin/rates/{rates}', ['as'=> 'admin.rates.show', 'uses' => 'Admin\RateController@show']);
Route::get('admin/rates/{rates}/edit', ['as'=> 'admin.rates.edit', 'uses' => 'Admin\RateController@edit']);


Route::get('admin/roomBookeds', ['as'=> 'admin.roomBookeds.index', 'uses' => 'Admin\RoomBookedController@index']);
Route::post('admin/roomBookeds', ['as'=> 'admin.roomBookeds.store', 'uses' => 'Admin\RoomBookedController@store']);
Route::get('admin/roomBookeds/create', ['as'=> 'admin.roomBookeds.create', 'uses' => 'Admin\RoomBookedController@create']);
Route::put('admin/roomBookeds/{roomBookeds}', ['as'=> 'admin.roomBookeds.update', 'uses' => 'Admin\RoomBookedController@update']);
Route::patch('admin/roomBookeds/{roomBookeds}', ['as'=> 'admin.roomBookeds.update', 'uses' => 'Admin\RoomBookedController@update']);
Route::delete('admin/roomBookeds/{roomBookeds}', ['as'=> 'admin.roomBookeds.destroy', 'uses' => 'Admin\RoomBookedController@destroy']);
Route::get('admin/roomBookeds/{roomBookeds}', ['as'=> 'admin.roomBookeds.show', 'uses' => 'Admin\RoomBookedController@show']);
Route::get('admin/roomBookeds/{roomBookeds}/edit', ['as'=> 'admin.roomBookeds.edit', 'uses' => 'Admin\RoomBookedController@edit']);


Route::get('admin/bookings', ['as'=> 'admin.bookings.index', 'uses' => 'Admin\BookingController@index']);
Route::post('admin/bookings', ['as'=> 'admin.bookings.store', 'uses' => 'Admin\BookingController@store']);
Route::get('admin/bookings/create', ['as'=> 'admin.bookings.create', 'uses' => 'Admin\BookingController@create']);
Route::put('admin/bookings/{bookings}', ['as'=> 'admin.bookings.update', 'uses' => 'Admin\BookingController@update']);
Route::patch('admin/bookings/{bookings}', ['as'=> 'admin.bookings.update', 'uses' => 'Admin\BookingController@update']);
Route::delete('admin/bookings/{bookings}', ['as'=> 'admin.bookings.destroy', 'uses' => 'Admin\BookingController@destroy']);
Route::get('admin/bookings/{bookings}', ['as'=> 'admin.bookings.show', 'uses' => 'Admin\BookingController@show']);
Route::get('admin/bookings/{bookings}/edit', ['as'=> 'admin.bookings.edit', 'uses' => 'Admin\BookingController@edit']);
