<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateGuestsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('guests', function (Blueprint $table) {
            $table->increments('gGuestID');
            $table->string('gFirstName', 30);
            $table->string('gLastName', 30);
            $table->mediumText('gAddress');
            $table->mediumText('gAddress2');
            $table->string('gCity', 30);
            $table->string('gState', 30);
            $table->string('gZipCode', 15);
            $table->string('gCountry', 50);
            $table->string('gHomePhoneNumber', 20);
            $table->string('gCellularNumber', 20);
            $table->string('gMailAddress')->unique();
            $table->string('gGender', 6);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('guests');
    }
}
