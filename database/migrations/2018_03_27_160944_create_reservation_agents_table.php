<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateReservationAgentsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservation_agents', function (Blueprint $table) {
            $table->increments('raReservationAgentID');
            $table->string('raFirstName', 30);
            $table->string('raLastName', 30);
            $table->mediumText('raAddress');
            $table->mediumText('raAddress2');
            $table->string('raCity', 30);
            $table->string('raState', 30);
            $table->string('raZipCode', 15);
            $table->string('raCountry', 50);
            $table->string('raHomePhoneNumber', 20);
            $table->string('raCellularNumber', 20);
            $table->string('aMailAddress')->unique();
            $table->string('raGender', 6);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('reservation_agents');
    }
}
