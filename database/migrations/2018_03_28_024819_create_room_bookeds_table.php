<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRoomBookedsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('room_booked', function (Blueprint $table) {
            $table->increments('rbRoomBookedID');
            $table->unsignedInteger('rbBookingID');
            $table->foreign('rbBookingID')->references('bBookingID')->on('bookings');
            $table->unsignedInteger('rbRoomID');
            $table->foreign('rbRoomID')->references('rRoomID')->on('rooms');
            $table->Integer('rbRate');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('room_booked');
    }
}
