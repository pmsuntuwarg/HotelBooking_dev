<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStaffTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('staff', function (Blueprint $table) {
            $table->increments('sStaffID');
            $table->string('sFirstName', 30);
            $table->string('sLastName', 30);
            $table->unsignedInteger('sPositionID');
            $table->foreign('sPositionID')->references('pPositionID')->on('positions');
            $table->text('sAddress');
            $table->text('sAddress2');
            $table->string('sCity', 30);
            $table->string('sState', 30);
            $table->string('sZipCode', 15);
            $table->string('sCountry', 30);
            $table->string('sHomePhoneNumber', 15);
            $table->string('sCellularNumber', 15);
            $table->string('sMailAddress')->unique();
            $table->string('sGender', 6);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('staff');
    }
}
