<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBookingsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function (Blueprint $table) {
            $table->increments('bBookingID');
            $table->unsignedInteger('bHotelID');
            $table->foreign('bHotelID')->references('hHotelID')->on('hotels');
            $table->unsignedInteger('bGuestID');
            $table->foreign('bGuestID')->references('gGuestID')->on('guests');
            $table->unsignedInteger('bReservationAgentID');
            $table->foreign('bReservationAgentID')->references('raReservationAgentID')->on('reservation_agents');
            $table->date('bDateFrom');
            $table->date('bDateTo');
            $table->integer('bRoomCount');
            $table->unsignedInteger('bBookingStatusID');
            $table->foreign('bBookingStatusID')->references('bsBookingStatusID')->on('booking_statuses');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('bookings');
    }
}
