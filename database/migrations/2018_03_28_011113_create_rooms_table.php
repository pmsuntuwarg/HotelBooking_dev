<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRoomsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rooms', function (Blueprint $table) {
            $table->increments('rRoomID');
            $table->unsignedInteger('rHotelID');
            $table->foreign('rHotelID')->references('hHotelID')->on('hotels');
            $table->text('rFloor');
            $table->unsignedInteger('rRoomTypeID');
            $table->foreign('rRoomTypeID')->references('rtRoomTypeID')->on('room_types');
            $table->smallInteger('rRoomNumber');
            $table->text('rDescription');
            $table->unsignedInteger('rRoomStatusID');
            $table->foreign('rRoomStatusID')->references('rsRoomStatusID')->on('room_statuses');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('rooms');
    }
}
