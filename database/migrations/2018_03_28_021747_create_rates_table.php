<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRatesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rates', function (Blueprint $table) {
            $table->increments('rRateID');
            $table->unsignedInteger('rRoomID');
            $table->foreign('rRoomID')->references('rRoomID')->on('rooms');
            $table->mediumInteger('rRate');
            $table->date('rFromDate');
            $table->date('rToDate');
            $table->unsignedInteger('rRateTypeID');
            $table->foreign('rRateTypeID')->references('rtRateTypeID')->on('rate_types');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('rates');
    }
}
