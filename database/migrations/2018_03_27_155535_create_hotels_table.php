<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateHotelsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hotels', function (Blueprint $table) {
            $table->increments('hHotelID');
            $table->text('hHotelCode');
            $table->string('hName', 100);
            $table->string('hMotto');
            $table->text('hAddress');
            $table->text('hAddress2');
            $table->string('hCity', 30);
            $table->string('hState', 30);
            $table->string('hZipCode', 15);
            $table->string('hMainPhoneNumber', 15);
            $table->string('hFaxNumber', 15);
            $table->string('hTollFreeNumber', 20);
            $table->string('hCompanyMailingAddress')->unique();
            $table->string('hWebsiteAddress');
            $table->string('hMain');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('hotels');
    }
}
