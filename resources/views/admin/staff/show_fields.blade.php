<!-- Sstaffid Field -->
<div class="form-group">
    {!! Form::label('sStaffID', 'Sstaffid:') !!}
    <p>{!! $staff->sStaffID !!}</p>
</div>

<!-- Sfirstname Field -->
<div class="form-group">
    {!! Form::label('sFirstName', 'Sfirstname:') !!}
    <p>{!! $staff->sFirstName !!}</p>
</div>

<!-- Slastname Field -->
<div class="form-group">
    {!! Form::label('sLastName', 'Slastname:') !!}
    <p>{!! $staff->sLastName !!}</p>
</div>

<!-- Spositionid Field -->
<div class="form-group">
    {!! Form::label('sPositionID', 'Spositionid:') !!}
    <p>{!! $staff->sPositionID !!}</p>
</div>

<!-- Saddress Field -->
<div class="form-group">
    {!! Form::label('sAddress', 'Saddress:') !!}
    <p>{!! $staff->sAddress !!}</p>
</div>

<!-- Saddress2 Field -->
<div class="form-group">
    {!! Form::label('sAddress2', 'Saddress2:') !!}
    <p>{!! $staff->sAddress2 !!}</p>
</div>

<!-- Scity Field -->
<div class="form-group">
    {!! Form::label('sCity', 'Scity:') !!}
    <p>{!! $staff->sCity !!}</p>
</div>

<!-- Sstate Field -->
<div class="form-group">
    {!! Form::label('sState', 'Sstate:') !!}
    <p>{!! $staff->sState !!}</p>
</div>

<!-- Szipcode Field -->
<div class="form-group">
    {!! Form::label('sZipCode', 'Szipcode:') !!}
    <p>{!! $staff->sZipCode !!}</p>
</div>

<!-- Scountry Field -->
<div class="form-group">
    {!! Form::label('sCountry', 'Scountry:') !!}
    <p>{!! $staff->sCountry !!}</p>
</div>

<!-- Shomephonenumber Field -->
<div class="form-group">
    {!! Form::label('sHomePhoneNumber', 'Shomephonenumber:') !!}
    <p>{!! $staff->sHomePhoneNumber !!}</p>
</div>

<!-- Scellularnumber Field -->
<div class="form-group">
    {!! Form::label('sCellularNumber', 'Scellularnumber:') !!}
    <p>{!! $staff->sCellularNumber !!}</p>
</div>

<!-- Smailaddress Field -->
<div class="form-group">
    {!! Form::label('sMailAddress', 'Smailaddress:') !!}
    <p>{!! $staff->sMailAddress !!}</p>
</div>

<!-- Sgender Field -->
<div class="form-group">
    {!! Form::label('sGender', 'Sgender:') !!}
    <p>{!! $staff->sGender !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $staff->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $staff->updated_at !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $staff->deleted_at !!}</p>
</div>

