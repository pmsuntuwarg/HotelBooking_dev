<!-- Sfirstname Field -->
<div class="form-group col-sm-6">
    {!! Form::label('sFirstName', 'Sfirstname:') !!}
    {!! Form::text('sFirstName', null, ['class' => 'form-control']) !!}
</div>

<!-- Slastname Field -->
<div class="form-group col-sm-6">
    {!! Form::label('sLastName', 'Slastname:') !!}
    {!! Form::text('sLastName', null, ['class' => 'form-control']) !!}
</div>

<!-- Spositionid Field -->
<div class="form-group col-sm-6">
    {!! Form::label('sPositionID', 'Position ') !!}
    {!! Form::select('sPositionID',$positionsArray , ['class' => 'form-control']) !!}
</div>

<!-- Saddress Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('sAddress', 'Address:') !!}
    {!! Form::text('sAddress', null, ['class' => 'form-control']) !!}
</div>

<!-- Saddress2 Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('sAddress2', 'Address:') !!}
    {!! Form::text('sAddress2', null, ['class' => 'form-control']) !!}
</div>

<!-- Scity Field -->
<div class="form-group col-sm-6">
    {!! Form::label('sCity', 'Scity:') !!}
    {!! Form::text('sCity', null, ['class' => 'form-control']) !!}
</div>

<!-- Sstate Field -->
<div class="form-group col-sm-6">
    {!! Form::label('sState', 'Sstate:') !!}
    {!! Form::text('sState', null, ['class' => 'form-control']) !!}
</div>

<!-- Szipcode Field -->
<div class="form-group col-sm-6">
    {!! Form::label('sZipCode', 'Szipcode:') !!}
    {!! Form::text('sZipCode', null, ['class' => 'form-control']) !!}
</div>

<!-- Scountry Field -->
<div class="form-group col-sm-6">
    {!! Form::label('sCountry', 'Scountry:') !!}
    {!! Form::text('sCountry', null, ['class' => 'form-control']) !!}
</div>

<!-- Shomephonenumber Field -->
<div class="form-group col-sm-6">
    {!! Form::label('sHomePhoneNumber', 'Shomephonenumber:') !!}
    {!! Form::text('sHomePhoneNumber', null, ['class' => 'form-control']) !!}
</div>

<!-- Scellularnumber Field -->
<div class="form-group col-sm-6">
    {!! Form::label('sCellularNumber', 'Scellularnumber:') !!}
    {!! Form::text('sCellularNumber', null, ['class' => 'form-control']) !!}
</div>

<!-- Smailaddress Field -->
<div class="form-group col-sm-6">
    {!! Form::label('sMailAddress', 'Smailaddress:') !!}
    {!! Form::text('sMailAddress', null, ['class' => 'form-control']) !!}
</div>

<!-- Sgender Field -->
<div class="form-group col-sm-6">
    {!! Form::label('sGender', 'Sgender:') !!}
    {!! Form::text('sGender', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.staff.index') !!}" class="btn btn-default">Cancel</a>
</div>
