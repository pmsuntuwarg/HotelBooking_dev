<table class="table table-responsive" id="staff-table">
    <thead>
        <tr>
            <th>Sfirstname</th>
        <th>Slastname</th>
        <th>Spositionid</th>
        <th>Saddress</th>
        <th>Saddress2</th>
        <th>Scity</th>
        <th>Sstate</th>
        <th>Szipcode</th>
        <th>Scountry</th>
        <th>Shomephonenumber</th>
        <th>Scellularnumber</th>
        <th>Smailaddress</th>
        <th>Sgender</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($staff as $staff)
        <tr>
            <td>{!! $staff->sFirstName !!}</td>
            <td>{!! $staff->sLastName !!}</td>
            <td>{!! $staff->sPositionID !!}</td>
            <td>{!! $staff->sAddress !!}</td>
            <td>{!! $staff->sAddress2 !!}</td>
            <td>{!! $staff->sCity !!}</td>
            <td>{!! $staff->sState !!}</td>
            <td>{!! $staff->sZipCode !!}</td>
            <td>{!! $staff->sCountry !!}</td>
            <td>{!! $staff->sHomePhoneNumber !!}</td>
            <td>{!! $staff->sCellularNumber !!}</td>
            <td>{!! $staff->sMailAddress !!}</td>
            <td>{!! $staff->sGender !!}</td>
            <td>
                {!! Form::open(['route' => ['admin.staff.destroy', $staff->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('admin.staff.show', [$staff->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('admin.staff.edit', [$staff->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>