@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Staff
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($staff, ['route' => ['admin.staff.update', $staff->id], 'method' => 'patch']) !!}

                        @include('admin.staff.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection