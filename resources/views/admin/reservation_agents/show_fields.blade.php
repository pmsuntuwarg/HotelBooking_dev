<!-- Rareservationagentid Field -->
<div class="form-group">
    {!! Form::label('raReservationAgentID', 'Rareservationagentid:') !!}
    <p>{!! $reservationAgent->raReservationAgentID !!}</p>
</div>

<!-- Rafirstname Field -->
<div class="form-group">
    {!! Form::label('raFirstName', 'Rafirstname:') !!}
    <p>{!! $reservationAgent->raFirstName !!}</p>
</div>

<!-- Ralastname Field -->
<div class="form-group">
    {!! Form::label('raLastName', 'Ralastname:') !!}
    <p>{!! $reservationAgent->raLastName !!}</p>
</div>

<!-- Raaddress Field -->
<div class="form-group">
    {!! Form::label('raAddress', 'Raaddress:') !!}
    <p>{!! $reservationAgent->raAddress !!}</p>
</div>

<!-- Raaddress2 Field -->
<div class="form-group">
    {!! Form::label('raAddress2', 'Raaddress2:') !!}
    <p>{!! $reservationAgent->raAddress2 !!}</p>
</div>

<!-- Racity Field -->
<div class="form-group">
    {!! Form::label('raCity', 'Racity:') !!}
    <p>{!! $reservationAgent->raCity !!}</p>
</div>

<!-- Rastate Field -->
<div class="form-group">
    {!! Form::label('raState', 'Rastate:') !!}
    <p>{!! $reservationAgent->raState !!}</p>
</div>

<!-- Razipcode Field -->
<div class="form-group">
    {!! Form::label('raZipCode', 'Razipcode:') !!}
    <p>{!! $reservationAgent->raZipCode !!}</p>
</div>

<!-- Racountry Field -->
<div class="form-group">
    {!! Form::label('raCountry', 'Racountry:') !!}
    <p>{!! $reservationAgent->raCountry !!}</p>
</div>

<!-- Rahomephonenumber Field -->
<div class="form-group">
    {!! Form::label('raHomePhoneNumber', 'Rahomephonenumber:') !!}
    <p>{!! $reservationAgent->raHomePhoneNumber !!}</p>
</div>

<!-- Racellularnumber Field -->
<div class="form-group">
    {!! Form::label('raCellularNumber', 'Racellularnumber:') !!}
    <p>{!! $reservationAgent->raCellularNumber !!}</p>
</div>

<!-- Amailaddress Field -->
<div class="form-group">
    {!! Form::label('aMailAddress', 'Amailaddress:') !!}
    <p>{!! $reservationAgent->aMailAddress !!}</p>
</div>

<!-- Ragender Field -->
<div class="form-group">
    {!! Form::label('raGender', 'Ragender:') !!}
    <p>{!! $reservationAgent->raGender !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $reservationAgent->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $reservationAgent->updated_at !!}</p>
</div>

