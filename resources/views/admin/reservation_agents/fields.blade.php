<!-- Rafirstname Field -->
<div class="form-group col-sm-6">
    {!! Form::label('raFirstName', 'Rafirstname:') !!}
    {!! Form::text('raFirstName', null, ['class' => 'form-control']) !!}
</div>

<!-- Ralastname Field -->
<div class="form-group col-sm-6">
    {!! Form::label('raLastName', 'Ralastname:') !!}
    {!! Form::text('raLastName', null, ['class' => 'form-control']) !!}
</div>

<!-- Raaddress Field -->
<div class="form-group col-sm-6">
    {!! Form::label('raAddress', 'Raaddress:') !!}
    {!! Form::text('raAddress', null, ['class' => 'form-control']) !!}
</div>

<!-- Raaddress2 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('raAddress2', 'Raaddress2:') !!}
    {!! Form::text('raAddress2', null, ['class' => 'form-control']) !!}
</div>

<!-- Racity Field -->
<div class="form-group col-sm-6">
    {!! Form::label('raCity', 'Racity:') !!}
    {!! Form::text('raCity', null, ['class' => 'form-control']) !!}
</div>

<!-- Rastate Field -->
<div class="form-group col-sm-6">
    {!! Form::label('raState', 'Rastate:') !!}
    {!! Form::text('raState', null, ['class' => 'form-control']) !!}
</div>

<!-- Razipcode Field -->
<div class="form-group col-sm-6">
    {!! Form::label('raZipCode', 'Razipcode:') !!}
    {!! Form::text('raZipCode', null, ['class' => 'form-control']) !!}
</div>

<!-- Racountry Field -->
<div class="form-group col-sm-6">
    {!! Form::label('raCountry', 'Racountry:') !!}
    {!! Form::text('raCountry', null, ['class' => 'form-control']) !!}
</div>

<!-- Rahomephonenumber Field -->
<div class="form-group col-sm-6">
    {!! Form::label('raHomePhoneNumber', 'Rahomephonenumber:') !!}
    {!! Form::text('raHomePhoneNumber', null, ['class' => 'form-control']) !!}
</div>

<!-- Racellularnumber Field -->
<div class="form-group col-sm-6">
    {!! Form::label('raCellularNumber', 'Racellularnumber:') !!}
    {!! Form::text('raCellularNumber', null, ['class' => 'form-control']) !!}
</div>

<!-- Amailaddress Field -->
<div class="form-group col-sm-6">
    {!! Form::label('aMailAddress', 'Amailaddress:') !!}
    {!! Form::email('aMailAddress', null, ['class' => 'form-control']) !!}
</div>

<!-- Ragender Field -->
<div class="form-group col-sm-6">
    {!! Form::label('raGender', 'Ragender:') !!}
    {!! Form::text('raGender', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.reservationAgents.index') !!}" class="btn btn-default">Cancel</a>
</div>
