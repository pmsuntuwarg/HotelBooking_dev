<table class="table table-responsive" id="reservationAgents-table">
    <thead>
        <tr>
            <th>Rafirstname</th>
        <th>Ralastname</th>
        <th>Raaddress</th>
        <th>Raaddress2</th>
        <th>Racity</th>
        <th>Rastate</th>
        <th>Razipcode</th>
        <th>Racountry</th>
        <th>Rahomephonenumber</th>
        <th>Racellularnumber</th>
        <th>Amailaddress</th>
        <th>Ragender</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($reservationAgents as $reservationAgent)
        <tr>
            <td>{!! $reservationAgent->raFirstName !!}</td>
            <td>{!! $reservationAgent->raLastName !!}</td>
            <td>{!! $reservationAgent->raAddress !!}</td>
            <td>{!! $reservationAgent->raAddress2 !!}</td>
            <td>{!! $reservationAgent->raCity !!}</td>
            <td>{!! $reservationAgent->raState !!}</td>
            <td>{!! $reservationAgent->raZipCode !!}</td>
            <td>{!! $reservationAgent->raCountry !!}</td>
            <td>{!! $reservationAgent->raHomePhoneNumber !!}</td>
            <td>{!! $reservationAgent->raCellularNumber !!}</td>
            <td>{!! $reservationAgent->aMailAddress !!}</td>
            <td>{!! $reservationAgent->raGender !!}</td>
            <td>
                {!! Form::open(['route' => ['admin.reservationAgents.destroy', $reservationAgent->raReservationAgentID], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('admin.reservationAgents.show', [$reservationAgent->raReservationAgentID]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('admin.reservationAgents.edit', [$reservationAgent->raReservationAgentID]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>