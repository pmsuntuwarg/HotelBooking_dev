@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Reservation Agent
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($reservationAgent, ['route' => ['admin.reservationAgents.update', $reservationAgent->raReservationAgentID], 'method' => 'patch']) !!}

                        @include('admin.reservation_agents.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection