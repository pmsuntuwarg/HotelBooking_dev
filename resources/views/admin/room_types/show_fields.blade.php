<!-- Rtroomtypeid Field -->
<div class="form-group">
    {!! Form::label('rtRoomTypeID', 'Rtroomtypeid:') !!}
    <p>{!! $roomType->rtRoomTypeID !!}</p>
</div>

<!-- Rtroomtype Field -->
<div class="form-group">
    {!! Form::label('rtRoomType', 'Rtroomtype:') !!}
    <p>{!! $roomType->rtRoomType !!}</p>
</div>

<!-- Rtdescription Field -->
<div class="form-group">
    {!! Form::label('rtDescription', 'Rtdescription:') !!}
    <p>{!! $roomType->rtDescription !!}</p>
</div>

<!-- Rtsortorder Field -->
<div class="form-group">
    {!! Form::label('rtSortOrder', 'Rtsortorder:') !!}
    <p>{!! $roomType->rtSortOrder !!}</p>
</div>

<!-- Rtactive Field -->
<div class="form-group">
    {!! Form::label('rtActive', 'Rtactive:') !!}
    <p>{!! $roomType->rtActive !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $roomType->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $roomType->updated_at !!}</p>
</div>

