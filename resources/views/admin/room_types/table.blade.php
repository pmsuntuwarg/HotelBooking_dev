<table class="table table-responsive" id="roomTypes-table">
    <thead>
        <tr>
            <th>Rtroomtype</th>
        <th>Rtdescription</th>
        <th>Rtsortorder</th>
        <th>Rtactive</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($roomTypes as $roomType)
        <tr>
            <td>{!! $roomType->rtRoomType !!}</td>
            <td>{!! $roomType->rtDescription !!}</td>
            <td>{!! $roomType->rtSortOrder !!}</td>
            <td>{!! $roomType->rtActive !!}</td>
            <td>
                {!! Form::open(['route' => ['admin.roomTypes.destroy', $roomType->rtRoomTypeID], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('admin.roomTypes.show', [$roomType->rtRoomTypeID]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('admin.roomTypes.edit', [$roomType->rtRoomTypeID]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>