<!-- Rtroomtype Field -->
<div class="form-group col-sm-6">
    {!! Form::label('rtRoomType', 'Rtroomtype:') !!}
    {!! Form::text('rtRoomType', null, ['class' => 'form-control']) !!}
</div>

<!-- Rtdescription Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('rtDescription', 'Rtdescription:') !!}
    {!! Form::textarea('rtDescription', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('rtSortOrder', 'Sort Order:') !!}
    {!! Form::number('rtSortOrder', null, ['class' => 'form-control']) !!}
</div><

<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('rtActive', 'Active:') !!}
    {!! Form::number('rtActive', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.roomTypes.index') !!}" class="btn btn-default">Cancel</a>
</div>
