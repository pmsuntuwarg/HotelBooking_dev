<table class="table table-responsive" id="rooms-table">
    <thead>
        <tr>
            <th>Rhotelid</th>
        <th>Rfloor</th>
        <th>Rroomtypeid</th>
        <th>Rroomnumber</th>
        <th>Rdescription</th>
        <th>Rroomstatusid</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($rooms as $room)
        <tr>
            <td>{!! $room->rHotelID !!}</td>
            <td>{!! $room->rFloor !!}</td>
            <td>{!! $room->rRoomTypeID !!}</td>
            <td>{!! $room->rRoomNumber !!}</td>
            <td>{!! $room->rDescription !!}</td>
            <td>{!! $room->rRoomStatusID !!}</td>
            <td>
                {!! Form::open(['route' => ['admin.rooms.destroy', $room->rRoomID], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('admin.rooms.show', [$room->rRoomID]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('admin.rooms.edit', [$room->rRoomID]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>