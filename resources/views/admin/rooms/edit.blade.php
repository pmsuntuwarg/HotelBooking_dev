@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Room
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($room, ['route' => ['admin.rooms.update', $room->rRoomID], 'method' => 'patch']) !!}

                        @include('admin.rooms.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection