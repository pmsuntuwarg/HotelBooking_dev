<!-- Rhotelid Field -->
<div class="form-group col-sm-6">
    {!! Form::label('rHotelID', 'Hotel') !!}
    {!! Form::select('rHotelID', $hotelsArray, ['class' => 'form-control']) !!}
</div>

<!-- Rfloor Field -->
<div class="form-group col-sm-6">
    {!! Form::label('rFloor', 'Rfloor:') !!}
    {!! Form::text('rFloor', null, ['class' => 'form-control']) !!}
</div>

<!-- Rroomtypeid Field -->
<div class="form-group col-sm-6">
    {!! Form::label('rRoomTypeID', 'Room Type:') !!}
    {!! Form::select('rRoomTypeID', $roomTypesArray, ['class' => 'form-control']) !!}
</div>

<!-- Rdescription Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('rDescription', 'Rdescription:') !!}
    {!! Form::textarea('rDescription', null, ['class' => 'form-control']) !!}
</div>

<!-- Rroomstatusid Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('rRoomStatusID', 'Rroomstatusid:') !!}
    {!! Form::select('rRoomStatusID', $roomStatusesArray, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('rRoomNumber', 'Room Number:') !!}
    {!! Form::number('rRoomNumber', null , ['class' => 'form-control']) !!}
</div>



<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.rooms.index') !!}" class="btn btn-default">Cancel</a>
</div>
