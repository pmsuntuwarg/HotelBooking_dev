<!-- Rroomid Field -->
<div class="form-group">
    {!! Form::label('rRoomID', 'Rroomid:') !!}
    <p>{!! $room->rRoomID !!}</p>
</div>

<!-- Rhotelid Field -->
<div class="form-group">
    {!! Form::label('rHotelID', 'Rhotelid:') !!}
    <p>{!! $room->rHotelID !!}</p>
</div>

<!-- Rfloor Field -->
<div class="form-group">
    {!! Form::label('rFloor', 'Rfloor:') !!}
    <p>{!! $room->rFloor !!}</p>
</div>

<!-- Rroomtypeid Field -->
<div class="form-group">
    {!! Form::label('rRoomTypeID', 'Rroomtypeid:') !!}
    <p>{!! $room->rRoomTypeID !!}</p>
</div>

<!-- Rroomnumber Field -->
<div class="form-group">
    {!! Form::label('rRoomNumber', 'Rroomnumber:') !!}
    <p>{!! $room->rRoomNumber !!}</p>
</div>

<!-- Rdescription Field -->
<div class="form-group">
    {!! Form::label('rDescription', 'Rdescription:') !!}
    <p>{!! $room->rDescription !!}</p>
</div>

<!-- Rroomstatusid Field -->
<div class="form-group">
    {!! Form::label('rRoomStatusID', 'Rroomstatusid:') !!}
    <p>{!! $room->rRoomStatusID !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $room->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $room->updated_at !!}</p>
</div>

