<table class="table table-responsive" id="roomStatuses-table">
    <thead>
        <tr>
            <th>Rsroomstatus</th>
        <th>Rsdescription</th>
        <th>Rssortorder</th>
        <th>Rsactive</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($roomStatuses as $roomStatus)
        <tr>
            <td>{!! $roomStatus->rsRoomStatus !!}</td>
            <td>{!! $roomStatus->rsDescription !!}</td>
            <td>{!! $roomStatus->rsSortOrder !!}</td>
            <td>{!! $roomStatus->rsActive !!}</td>
            <td>
                {!! Form::open(['route' => ['admin.roomStatuses.destroy', $roomStatus->rtRoomStatusID], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('admin.roomStatuses.show', [$roomStatus->rtRoomStatusID]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('admin.roomStatuses.edit', [$roomStatus->rtRoomStatusID]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>