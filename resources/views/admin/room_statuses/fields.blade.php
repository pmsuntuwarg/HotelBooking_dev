<!-- Rsroomstatus Field -->
<div class="form-group col-sm-6">
    {!! Form::label('rsRoomStatus', 'Rsroomstatus:') !!}
    {!! Form::text('rsRoomStatus', null, ['class' => 'form-control']) !!}
</div>

<!-- Rsdescription Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('rsDescription', 'Rsdescription:') !!}
    {!! Form::textarea('rsDescription', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('rsSortOrder', 'Sort Order:') !!}
    {!! Form::number('rsSortOrder', null, ['class' => 'form-control']) !!}
</div><

<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('rsActive', 'Active:') !!}
    {!! Form::number('rsActive', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.roomStatuses.index') !!}" class="btn btn-default">Cancel</a>
</div>
