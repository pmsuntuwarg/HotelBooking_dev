<!-- Rtroomstatusid Field -->
<div class="form-group">
    {!! Form::label('rtRoomStatusID', 'Rtroomstatusid:') !!}
    <p>{!! $roomStatus->rtRoomStatusID !!}</p>
</div>

<!-- Rsroomstatus Field -->
<div class="form-group">
    {!! Form::label('rsRoomStatus', 'Rsroomstatus:') !!}
    <p>{!! $roomStatus->rsRoomStatus !!}</p>
</div>

<!-- Rsdescription Field -->
<div class="form-group">
    {!! Form::label('rsDescription', 'Rsdescription:') !!}
    <p>{!! $roomStatus->rsDescription !!}</p>
</div>

<!-- Rssortorder Field -->
<div class="form-group">
    {!! Form::label('rsSortOrder', 'Rssortorder:') !!}
    <p>{!! $roomStatus->rsSortOrder !!}</p>
</div>

<!-- Rsactive Field -->
<div class="form-group">
    {!! Form::label('rsActive', 'Rsactive:') !!}
    <p>{!! $roomStatus->rsActive !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $roomStatus->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $roomStatus->updated_at !!}</p>
</div>

