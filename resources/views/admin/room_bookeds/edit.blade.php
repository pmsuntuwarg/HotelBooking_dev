@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Room Booked
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($roomBooked, ['route' => ['admin.roomBookeds.update', $roomBooked->rbRoomBookedID], 'method' => 'patch']) !!}

                        @include('admin.room_bookeds.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection