<table class="table table-responsive" id="roomBookeds-table">
    <thead>
        <tr>
            <th>Rbbookingid</th>
        <th>Rbbookingid</th>
        <th>Rbroomid</th>
        <th>Rbrate</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($roomBookeds as $roomBooked)
        <tr>
            <td>{!! $roomBooked->rbBookingID !!}</td>
            <td>{!! $roomBooked->rbBookingID !!}</td>
            <td>{!! $roomBooked->rbRoomID !!}</td>
            <td>{!! $roomBooked->rbRate !!}</td>
            <td>
                {!! Form::open(['route' => ['admin.roomBookeds.destroy', $roomBooked->rbRoomBookedID], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('admin.roomBookeds.show', [$roomBooked->rbRoomBookedID]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('admin.roomBookeds.edit', [$roomBooked->rbRoomBookedID]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>