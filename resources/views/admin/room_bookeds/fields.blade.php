<!-- Rbbookingid Field -->
<div class="form-group col-sm-6">
    {!! Form::label('rbBookingID', 'Rbbookingid:') !!}
    {!! Form::text('rbBookingID', null, ['class' => 'form-control']) !!}
</div>

<!-- Rbbookingid Field -->
<div class="form-group col-sm-6">
    {!! Form::label('rbBookingID', 'Rbbookingid:') !!}
    {!! Form::text('rbBookingID', null, ['class' => 'form-control']) !!}
</div>

<!-- Rbroomid Field -->
<div class="form-group col-sm-6">
    {!! Form::label('rbRoomID', 'Rbroomid:') !!}
    {!! Form::text('rbRoomID', null, ['class' => 'form-control']) !!}
</div>

<!-- Rbrate Field -->
<div class="form-group col-sm-6">
    {!! Form::label('rbRate', 'Rbrate:') !!}
    {!! Form::number('rbRate', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.roomBookeds.index') !!}" class="btn btn-default">Cancel</a>
</div>
