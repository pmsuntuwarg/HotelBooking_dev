<table class="table table-responsive" id="paymentStatuses-table">
    <thead>
        <tr>
            <th>Psstatus</th>
        <th>Psdescription</th>
        <th>Pssortorder</th>
        <th>Psactive</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($paymentStatuses as $paymentStatus)
        <tr>
            <td>{!! $paymentStatus->psStatus !!}</td>
            <td>{!! $paymentStatus->psDescription !!}</td>
            <td>{!! $paymentStatus->psSortOrder !!}</td>
            <td>{!! $paymentStatus->psActive !!}</td>
            <td>
                {!! Form::open(['route' => ['admin.paymentStatuses.destroy', $paymentStatus->psPaymentStatusID], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('admin.paymentStatuses.show', [$paymentStatus->psPaymentStatusID]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('admin.paymentStatuses.edit', [$paymentStatus->psPaymentStatusID]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>