<!-- Pspaymentstatusid Field -->
<div class="form-group">
    {!! Form::label('psPaymentStatusID', 'Pspaymentstatusid:') !!}
    <p>{!! $paymentStatus->psPaymentStatusID !!}</p>
</div>

<!-- Psstatus Field -->
<div class="form-group">
    {!! Form::label('psStatus', 'Psstatus:') !!}
    <p>{!! $paymentStatus->psStatus !!}</p>
</div>

<!-- Psdescription Field -->
<div class="form-group">
    {!! Form::label('psDescription', 'Psdescription:') !!}
    <p>{!! $paymentStatus->psDescription !!}</p>
</div>

<!-- Pssortorder Field -->
<div class="form-group">
    {!! Form::label('psSortOrder', 'Pssortorder:') !!}
    <p>{!! $paymentStatus->psSortOrder !!}</p>
</div>

<!-- Psactive Field -->
<div class="form-group">
    {!! Form::label('psActive', 'Psactive:') !!}
    <p>{!! $paymentStatus->psActive !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $paymentStatus->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $paymentStatus->updated_at !!}</p>
</div>

