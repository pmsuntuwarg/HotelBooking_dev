<!-- Psdescription Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('psDescription', 'Psdescription:') !!}
    {!! Form::textarea('psDescription', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.paymentStatuses.index') !!}" class="btn btn-default">Cancel</a>
</div>
