@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Rate Type
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($rateType, ['route' => ['admin.rateTypes.update', $rateType->rtRateTypeTypeID], 'method' => 'patch']) !!}

                        @include('admin.rate_types.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection