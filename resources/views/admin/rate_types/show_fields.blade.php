<!-- Rtratetypetypeid Field -->
<div class="form-group">
    {!! Form::label('rtRateTypeTypeID', 'Rtratetypetypeid:') !!}
    <p>{!! $rateType->rtRateTypeTypeID !!}</p>
</div>

<!-- Rtratetype Field -->
<div class="form-group">
    {!! Form::label('rtRateType', 'Rtratetype:') !!}
    <p>{!! $rateType->rtRateType !!}</p>
</div>

<!-- Rtdescription Field -->
<div class="form-group">
    {!! Form::label('rtDescription', 'Rtdescription:') !!}
    <p>{!! $rateType->rtDescription !!}</p>
</div>

<!-- Rtsortorder Field -->
<div class="form-group">
    {!! Form::label('rtSortOrder', 'Rtsortorder:') !!}
    <p>{!! $rateType->rtSortOrder !!}</p>
</div>

<!-- Rtactive Field -->
<div class="form-group">
    {!! Form::label('rtActive', 'Rtactive:') !!}
    <p>{!! $rateType->rtActive !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $rateType->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $rateType->updated_at !!}</p>
</div>

