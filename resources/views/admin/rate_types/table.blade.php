<table class="table table-responsive" id="rateTypes-table">
    <thead>
        <tr>
            <th>Rtratetype</th>
        <th>Rtdescription</th>
        <th>Rtsortorder</th>
        <th>Rtactive</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($rateTypes as $rateType)
        <tr>
            <td>{!! $rateType->rtRateType !!}</td>
            <td>{!! $rateType->rtDescription !!}</td>
            <td>{!! $rateType->rtSortOrder !!}</td>
            <td>{!! $rateType->rtActive !!}</td>
            <td>
                {!! Form::open(['route' => ['admin.rateTypes.destroy', $rateType->rtRateTypeTypeID], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('admin.rateTypes.show', [$rateType->rtRateTypeTypeID]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('admin.rateTypes.edit', [$rateType->rtRateTypeTypeID]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>