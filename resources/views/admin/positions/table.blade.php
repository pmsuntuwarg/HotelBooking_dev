<table class="table table-responsive" id="positions-table">
    <thead>
        <tr>
            <th>Pposition</th>
        <th>Psortorder</th>
        <th>Pactive</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($positions as $position)
        <tr>
            <td>{!! $position->pPosition !!}</td>
            <td>{!! $position->pSortOrder !!}</td>
            <td>{!! $position->pActive !!}</td>
            <td>
                {!! Form::open(['route' => ['admin.positions.destroy', $position->pPositionID], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('admin.positions.show', [$position->pPositionID]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('admin.positions.edit', [$position->pPositionID]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>