<!-- Ppositionid Field -->
<div class="form-group">
    {!! Form::label('pPositionID', 'Ppositionid:') !!}
    <p>{!! $position->pPositionID !!}</p>
</div>

<!-- Pposition Field -->
<div class="form-group">
    {!! Form::label('pPosition', 'Pposition:') !!}
    <p>{!! $position->pPosition !!}</p>
</div>

<!-- Psortorder Field -->
<div class="form-group">
    {!! Form::label('pSortOrder', 'Psortorder:') !!}
    <p>{!! $position->pSortOrder !!}</p>
</div>

<!-- Pactive Field -->
<div class="form-group">
    {!! Form::label('pActive', 'Pactive:') !!}
    <p>{!! $position->pActive !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $position->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $position->updated_at !!}</p>
</div>

