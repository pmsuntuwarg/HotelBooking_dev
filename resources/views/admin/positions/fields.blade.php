<!-- Pposition Field -->
<div class="form-group col-sm-6">
    {!! Form::label('pPosition', 'Pposition:') !!}
    {!! Form::text('pPosition', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.positions.index') !!}" class="btn btn-default">Cancel</a>
</div>
