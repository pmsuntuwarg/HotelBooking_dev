<table class="table table-responsive" id="bookingStatuses-table">
    <thead>
        <tr>
            <th>Bsstatus</th>
        <th>Bsdescription</th>
        <th>Bssortorder</th>
        <th>Bsactive</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($bookingStatuses as $bookingStatus)
        <tr>
            <td>{!! $bookingStatus->bsStatus !!}</td>
            <td>{!! $bookingStatus->bsDescription !!}</td>
            <td>{!! $bookingStatus->bsSortOrder !!}</td>
            <td>{!! $bookingStatus->bsActive !!}</td>
            <td>
                {!! Form::open(['route' => ['admin.bookingStatuses.destroy', $bookingStatus->bsBokingStatusID], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('admin.bookingStatuses.show', [$bookingStatus->bsBokingStatusID]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('admin.bookingStatuses.edit', [$bookingStatus->bsBokingStatusID]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>