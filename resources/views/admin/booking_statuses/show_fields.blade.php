<!-- Bsbokingstatusid Field -->
<div class="form-group">
    {!! Form::label('bsBokingStatusID', 'Bsbokingstatusid:') !!}
    <p>{!! $bookingStatus->bsBokingStatusID !!}</p>
</div>

<!-- Bsstatus Field -->
<div class="form-group">
    {!! Form::label('bsStatus', 'Bsstatus:') !!}
    <p>{!! $bookingStatus->bsStatus !!}</p>
</div>

<!-- Bsdescription Field -->
<div class="form-group">
    {!! Form::label('bsDescription', 'Bsdescription:') !!}
    <p>{!! $bookingStatus->bsDescription !!}</p>
</div>

<!-- Bssortorder Field -->
<div class="form-group">
    {!! Form::label('bsSortOrder', 'Bssortorder:') !!}
    <p>{!! $bookingStatus->bsSortOrder !!}</p>
</div>

<!-- Bsactive Field -->
<div class="form-group">
    {!! Form::label('bsActive', 'Bsactive:') !!}
    <p>{!! $bookingStatus->bsActive !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $bookingStatus->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $bookingStatus->updated_at !!}</p>
</div>

