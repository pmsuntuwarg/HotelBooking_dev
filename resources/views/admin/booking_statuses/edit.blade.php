@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Booking Status
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($bookingStatus, ['route' => ['admin.bookingStatuses.update', $bookingStatus->bsBokingStatusID], 'method' => 'patch']) !!}

                        @include('admin.booking_statuses.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection