<!-- Rroomid Field -->
<div class="form-group col-sm-6">
    {!! Form::label('rRoomID', 'Rroomid:') !!}
    {!! Form::select('rRoomID', $roomsArray, ['class' => 'form-control']) !!}
</div>

<!-- Rrate Field -->
<div class="form-group col-sm-6">
    {!! Form::label('rRate', 'Rrate:') !!}
    {!! Form::number('rRate', null, ['class' => 'form-control']) !!}
</div>

<!-- Rfromdate Field -->
<div class="form-group col-sm-6">
    {!! Form::label('rFromDate', 'Rfromdate:') !!}
    {!! Form::date('rFromDate', null, ['class' => 'form-control']) !!}
</div>

<!-- Rtodate Field -->
<div class="form-group col-sm-6">
    {!! Form::label('rToDate', 'Rtodate:') !!}
    {!! Form::date('rToDate', null, ['class' => 'form-control']) !!}
</div>

<!-- Rratetypeid Field -->
<div class="form-group col-sm-6">
    {!! Form::label('rRateTypeID', 'Rratetypeid:') !!}
    {!! Form::select('rRateTypeID', $rateTypesArray, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.rates.index') !!}" class="btn btn-default">Cancel</a>
</div>
