<table class="table table-responsive" id="rates-table">
    <thead>
        <tr>
            <th>Rroomid</th>
        <th>Rrate</th>
        <th>Rfromdate</th>
        <th>Rtodate</th>
        <th>Rratetypeid</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($rates as $rate)
        <tr>
            <td>{!! $rate->rRoomID !!}</td>
            <td>{!! $rate->rRate !!}</td>
            <td>{!! $rate->rFromDate !!}</td>
            <td>{!! $rate->rToDate !!}</td>
            <td>{!! $rate->rRateTypeID !!}</td>
            <td>
                {!! Form::open(['route' => ['admin.rates.destroy', $rate->rRateID], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('admin.rates.show', [$rate->rRateID]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('admin.rates.edit', [$rate->rRateID]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>