<!-- Rrateid Field -->
<div class="form-group">
    {!! Form::label('rRateID', 'Rrateid:') !!}
    <p>{!! $rate->rRateID !!}</p>
</div>

<!-- Rroomid Field -->
<div class="form-group">
    {!! Form::label('rRoomID', 'Rroomid:') !!}
    <p>{!! $rate->rRoomID !!}</p>
</div>

<!-- Rrate Field -->
<div class="form-group">
    {!! Form::label('rRate', 'Rrate:') !!}
    <p>{!! $rate->rRate !!}</p>
</div>

<!-- Rfromdate Field -->
<div class="form-group">
    {!! Form::label('rFromDate', 'Rfromdate:') !!}
    <p>{!! $rate->rFromDate !!}</p>
</div>

<!-- Rtodate Field -->
<div class="form-group">
    {!! Form::label('rToDate', 'Rtodate:') !!}
    <p>{!! $rate->rToDate !!}</p>
</div>

<!-- Rratetypeid Field -->
<div class="form-group">
    {!! Form::label('rRateTypeID', 'Rratetypeid:') !!}
    <p>{!! $rate->rRateTypeID !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $rate->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $rate->updated_at !!}</p>
</div>

