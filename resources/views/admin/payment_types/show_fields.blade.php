<!-- Ptpaymenttypeid Field -->
<div class="form-group">
    {!! Form::label('ptPaymentTypeID', 'Ptpaymenttypeid:') !!}
    <p>{!! $paymentType->ptPaymentTypeID !!}</p>
</div>

<!-- Ptpaymenttype Field -->
<div class="form-group">
    {!! Form::label('ptPaymentType', 'Ptpaymenttype:') !!}
    <p>{!! $paymentType->ptPaymentType !!}</p>
</div>

<!-- Ptsortorder Field -->
<div class="form-group">
    {!! Form::label('ptSortOrder', 'Ptsortorder:') !!}
    <p>{!! $paymentType->ptSortOrder !!}</p>
</div>

<!-- Ptactive Field -->
<div class="form-group">
    {!! Form::label('ptActive', 'Ptactive:') !!}
    <p>{!! $paymentType->ptActive !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $paymentType->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $paymentType->updated_at !!}</p>
</div>

