<table class="table table-responsive" id="paymentTypes-table">
    <thead>
        <tr>
            <th>Ptpaymenttype</th>
        <th>Ptsortorder</th>
        <th>Ptactive</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($paymentTypes as $paymentType)
        <tr>
            <td>{!! $paymentType->ptPaymentType !!}</td>
            <td>{!! $paymentType->ptSortOrder !!}</td>
            <td>{!! $paymentType->ptActive !!}</td>
            <td>
                {!! Form::open(['route' => ['admin.paymentTypes.destroy', $paymentType->ptPaymentTypeID], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('admin.paymentTypes.show', [$paymentType->ptPaymentTypeID]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('admin.paymentTypes.edit', [$paymentType->ptPaymentTypeID]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>