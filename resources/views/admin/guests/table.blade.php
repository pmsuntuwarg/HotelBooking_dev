<table class="table table-responsive" id="guests-table">
    <thead>
        <tr>
            <th>Gfirstname</th>
        <th>Glastname</th>
        <th>Gaddress</th>
        <th>Gaddress2</th>
        <th>Gcity</th>
        <th>Gstate</th>
        <th>Gzipcode</th>
        <th>Gcountry</th>
        <th>Ghomephonenumber</th>
        <th>Gcellularnumber</th>
        <th>Gmailaddress</th>
        <th>Ggender</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($guests as $guest)
        <tr>
            <td>{!! $guest->gFirstName !!}</td>
            <td>{!! $guest->gLastName !!}</td>
            <td>{!! $guest->gAddress !!}</td>
            <td>{!! $guest->gAddress2 !!}</td>
            <td>{!! $guest->gCity !!}</td>
            <td>{!! $guest->gState !!}</td>
            <td>{!! $guest->gZipCode !!}</td>
            <td>{!! $guest->gCountry !!}</td>
            <td>{!! $guest->gHomePhoneNumber !!}</td>
            <td>{!! $guest->gCellularNumber !!}</td>
            <td>{!! $guest->gMailAddress !!}</td>
            <td>{!! $guest->gGender !!}</td>
            <td>
                {!! Form::open(['route' => ['admin.guests.destroy', $guest->gGuestID], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('admin.guests.show', [$guest->gGuestID]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('admin.guests.edit', [$guest->gGuestID]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>