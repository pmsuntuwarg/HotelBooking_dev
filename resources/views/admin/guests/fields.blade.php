<!-- Gfirstname Field -->
<div class="form-group col-sm-6">
    {!! Form::label('gFirstName', 'Gfirstname:') !!}
    {!! Form::text('gFirstName', null, ['class' => 'form-control']) !!}
</div>

<!-- Glastname Field -->
<div class="form-group col-sm-6">
    {!! Form::label('gLastName', 'Glastname:') !!}
    {!! Form::text('gLastName', null, ['class' => 'form-control']) !!}
</div>

<!-- Gaddress Field -->
<div class="form-group col-sm-6">
    {!! Form::label('gAddress', 'Gaddress:') !!}
    {!! Form::text('gAddress', null, ['class' => 'form-control']) !!}
</div>

<!-- Gaddress2 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('gAddress2', 'Gaddress2:') !!}
    {!! Form::text('gAddress2', null, ['class' => 'form-control']) !!}
</div>

<!-- Gcity Field -->
<div class="form-group col-sm-6">
    {!! Form::label('gCity', 'Gcity:') !!}
    {!! Form::text('gCity', null, ['class' => 'form-control']) !!}
</div>

<!-- Gstate Field -->
<div class="form-group col-sm-6">
    {!! Form::label('gState', 'Gstate:') !!}
    {!! Form::text('gState', null, ['class' => 'form-control']) !!}
</div>

<!-- Gzipcode Field -->
<div class="form-group col-sm-6">
    {!! Form::label('gZipCode', 'Gzipcode:') !!}
    {!! Form::text('gZipCode', null, ['class' => 'form-control']) !!}
</div>

<!-- Gcountry Field -->
<div class="form-group col-sm-6">
    {!! Form::label('gCountry', 'Gcountry:') !!}
    {!! Form::text('gCountry', null, ['class' => 'form-control']) !!}
</div>

<!-- Ghomephonenumber Field -->
<div class="form-group col-sm-6">
    {!! Form::label('gHomePhoneNumber', 'Ghomephonenumber:') !!}
    {!! Form::text('gHomePhoneNumber', null, ['class' => 'form-control']) !!}
</div>

<!-- Gcellularnumber Field -->
<div class="form-group col-sm-6">
    {!! Form::label('gCellularNumber', 'Gcellularnumber:') !!}
    {!! Form::text('gCellularNumber', null, ['class' => 'form-control']) !!}
</div>

<!-- Gmailaddress Field -->
<div class="form-group col-sm-6">
    {!! Form::label('gMailAddress', 'Gmailaddress:') !!}
    {!! Form::email('gMailAddress', null, ['class' => 'form-control']) !!}
</div>

<!-- Ggender Field -->
<div class="form-group col-sm-6">
    {!! Form::label('gGender', 'Ggender:') !!}
    {!! Form::text('gGender', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.guests.index') !!}" class="btn btn-default">Cancel</a>
</div>
