<!-- Gguestid Field -->
<div class="form-group">
    {!! Form::label('gGuestID', 'Gguestid:') !!}
    <p>{!! $guest->gGuestID !!}</p>
</div>

<!-- Gfirstname Field -->
<div class="form-group">
    {!! Form::label('gFirstName', 'Gfirstname:') !!}
    <p>{!! $guest->gFirstName !!}</p>
</div>

<!-- Glastname Field -->
<div class="form-group">
    {!! Form::label('gLastName', 'Glastname:') !!}
    <p>{!! $guest->gLastName !!}</p>
</div>

<!-- Gaddress Field -->
<div class="form-group">
    {!! Form::label('gAddress', 'Gaddress:') !!}
    <p>{!! $guest->gAddress !!}</p>
</div>

<!-- Gaddress2 Field -->
<div class="form-group">
    {!! Form::label('gAddress2', 'Gaddress2:') !!}
    <p>{!! $guest->gAddress2 !!}</p>
</div>

<!-- Gcity Field -->
<div class="form-group">
    {!! Form::label('gCity', 'Gcity:') !!}
    <p>{!! $guest->gCity !!}</p>
</div>

<!-- Gstate Field -->
<div class="form-group">
    {!! Form::label('gState', 'Gstate:') !!}
    <p>{!! $guest->gState !!}</p>
</div>

<!-- Gzipcode Field -->
<div class="form-group">
    {!! Form::label('gZipCode', 'Gzipcode:') !!}
    <p>{!! $guest->gZipCode !!}</p>
</div>

<!-- Gcountry Field -->
<div class="form-group">
    {!! Form::label('gCountry', 'Gcountry:') !!}
    <p>{!! $guest->gCountry !!}</p>
</div>

<!-- Ghomephonenumber Field -->
<div class="form-group">
    {!! Form::label('gHomePhoneNumber', 'Ghomephonenumber:') !!}
    <p>{!! $guest->gHomePhoneNumber !!}</p>
</div>

<!-- Gcellularnumber Field -->
<div class="form-group">
    {!! Form::label('gCellularNumber', 'Gcellularnumber:') !!}
    <p>{!! $guest->gCellularNumber !!}</p>
</div>

<!-- Gmailaddress Field -->
<div class="form-group">
    {!! Form::label('gMailAddress', 'Gmailaddress:') !!}
    <p>{!! $guest->gMailAddress !!}</p>
</div>

<!-- Ggender Field -->
<div class="form-group">
    {!! Form::label('gGender', 'Ggender:') !!}
    <p>{!! $guest->gGender !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $guest->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $guest->updated_at !!}</p>
</div>

