<!-- Bhotelid Field -->
<div class="form-group col-sm-6">
    {!! Form::label('bHotelID', 'Bhotelid:') !!}
    {!! Form::text('bHotelID', null, ['class' => 'form-control']) !!}
</div>

<!-- Bguestid Field -->
<div class="form-group col-sm-6">
    {!! Form::label('bGuestID', 'Bguestid:') !!}
    {!! Form::text('bGuestID', null, ['class' => 'form-control']) !!}
</div>

<!-- Breservationagentid Field -->
<div class="form-group col-sm-6">
    {!! Form::label('bReservationAgentID', 'Breservationagentid:') !!}
    {!! Form::text('bReservationAgentID', null, ['class' => 'form-control']) !!}
</div>

<!-- Bdatefrom Field -->
<div class="form-group col-sm-6">
    {!! Form::label('bDateFrom', 'Bdatefrom:') !!}
    {!! Form::date('bDateFrom', null, ['class' => 'form-control']) !!}
</div>

<!-- Bdateto Field -->
<div class="form-group col-sm-6">
    {!! Form::label('bDateTo', 'Bdateto:') !!}
    {!! Form::date('bDateTo', null, ['class' => 'form-control']) !!}
</div>

<!-- Broomcount Field -->
<div class="form-group col-sm-6">
    {!! Form::label('bRoomCount', 'Broomcount:') !!}
    {!! Form::number('bRoomCount', null, ['class' => 'form-control']) !!}
</div>

<!-- Bbookingstatusid Field -->
<div class="form-group col-sm-6">
    {!! Form::label('bBookingStatusID', 'Bbookingstatusid:') !!}
    {!! Form::text('bBookingStatusID', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.bookings.index') !!}" class="btn btn-default">Cancel</a>
</div>
