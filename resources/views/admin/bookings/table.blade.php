<table class="table table-responsive" id="bookings-table">
    <thead>
        <tr>
            <th>Bhotelid</th>
        <th>Bguestid</th>
        <th>Breservationagentid</th>
        <th>Bdatefrom</th>
        <th>Bdateto</th>
        <th>Broomcount</th>
        <th>Bbookingstatusid</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($bookings as $booking)
        <tr>
            <td>{!! $booking->bHotelID !!}</td>
            <td>{!! $booking->bGuestID !!}</td>
            <td>{!! $booking->bReservationAgentID !!}</td>
            <td>{!! $booking->bDateFrom !!}</td>
            <td>{!! $booking->bDateTo !!}</td>
            <td>{!! $booking->bRoomCount !!}</td>
            <td>{!! $booking->bBookingStatusID !!}</td>
            <td>
                {!! Form::open(['route' => ['admin.bookings.destroy', $booking->bBookingID], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('admin.bookings.show', [$booking->bBookingID]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('admin.bookings.edit', [$booking->bBookingID]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>