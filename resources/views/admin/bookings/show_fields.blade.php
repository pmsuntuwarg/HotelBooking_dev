<!-- Bbookingid Field -->
<div class="form-group">
    {!! Form::label('bBookingID', 'Bbookingid:') !!}
    <p>{!! $booking->bBookingID !!}</p>
</div>

<!-- Bhotelid Field -->
<div class="form-group">
    {!! Form::label('bHotelID', 'Bhotelid:') !!}
    <p>{!! $booking->bHotelID !!}</p>
</div>

<!-- Bguestid Field -->
<div class="form-group">
    {!! Form::label('bGuestID', 'Bguestid:') !!}
    <p>{!! $booking->bGuestID !!}</p>
</div>

<!-- Breservationagentid Field -->
<div class="form-group">
    {!! Form::label('bReservationAgentID', 'Breservationagentid:') !!}
    <p>{!! $booking->bReservationAgentID !!}</p>
</div>

<!-- Bdatefrom Field -->
<div class="form-group">
    {!! Form::label('bDateFrom', 'Bdatefrom:') !!}
    <p>{!! $booking->bDateFrom !!}</p>
</div>

<!-- Bdateto Field -->
<div class="form-group">
    {!! Form::label('bDateTo', 'Bdateto:') !!}
    <p>{!! $booking->bDateTo !!}</p>
</div>

<!-- Broomcount Field -->
<div class="form-group">
    {!! Form::label('bRoomCount', 'Broomcount:') !!}
    <p>{!! $booking->bRoomCount !!}</p>
</div>

<!-- Bbookingstatusid Field -->
<div class="form-group">
    {!! Form::label('bBookingStatusID', 'Bbookingstatusid:') !!}
    <p>{!! $booking->bBookingStatusID !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $booking->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $booking->updated_at !!}</p>
</div>

