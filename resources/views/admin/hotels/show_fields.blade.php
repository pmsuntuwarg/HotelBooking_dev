<!-- Hhotelid Field -->
<div class="form-group">
    {!! Form::label('hHotelID', 'Hhotelid:') !!}
    <p>{!! $hotel->hHotelID !!}</p>
</div>

<!-- Hhotelcode Field -->
<div class="form-group">
    {!! Form::label('hHotelCode', 'Hhotelcode:') !!}
    <p>{!! $hotel->hHotelCode !!}</p>
</div>

<!-- Hname Field -->
<div class="form-group">
    {!! Form::label('hName', 'Hname:') !!}
    <p>{!! $hotel->hName !!}</p>
</div>

<!-- Hmotto Field -->
<div class="form-group">
    {!! Form::label('hMotto', 'Hmotto:') !!}
    <p>{!! $hotel->hMotto !!}</p>
</div>

<!-- Haddress Field -->
<div class="form-group">
    {!! Form::label('hAddress', 'Haddress:') !!}
    <p>{!! $hotel->hAddress !!}</p>
</div>

<!-- Haddress2 Field -->
<div class="form-group">
    {!! Form::label('hAddress2', 'Haddress2:') !!}
    <p>{!! $hotel->hAddress2 !!}</p>
</div>

<!-- Hcity Field -->
<div class="form-group">
    {!! Form::label('hCity', 'Hcity:') !!}
    <p>{!! $hotel->hCity !!}</p>
</div>

<!-- Hstate Field -->
<div class="form-group">
    {!! Form::label('hState', 'Hstate:') !!}
    <p>{!! $hotel->hState !!}</p>
</div>

<!-- Hzipcode Field -->
<div class="form-group">
    {!! Form::label('hZipCode', 'Hzipcode:') !!}
    <p>{!! $hotel->hZipCode !!}</p>
</div>

<!-- Hmainphonenumber Field -->
<div class="form-group">
    {!! Form::label('hMainPhoneNumber', 'Hmainphonenumber:') !!}
    <p>{!! $hotel->hMainPhoneNumber !!}</p>
</div>

<!-- Hfaxnumber Field -->
<div class="form-group">
    {!! Form::label('hFaxNumber', 'Hfaxnumber:') !!}
    <p>{!! $hotel->hFaxNumber !!}</p>
</div>

<!-- Htollfreenumber Field -->
<div class="form-group">
    {!! Form::label('hTollFreeNumber', 'Htollfreenumber:') !!}
    <p>{!! $hotel->hTollFreeNumber !!}</p>
</div>

<!-- Hcompanymailingaddress Field -->
<div class="form-group">
    {!! Form::label('hCompanyMailingAddress', 'Hcompanymailingaddress:') !!}
    <p>{!! $hotel->hCompanyMailingAddress !!}</p>
</div>

<!-- Hwebsiteaddress Field -->
<div class="form-group">
    {!! Form::label('hWebsiteAddress', 'Hwebsiteaddress:') !!}
    <p>{!! $hotel->hWebsiteAddress !!}</p>
</div>

<!-- Hmain Field -->
<div class="form-group">
    {!! Form::label('hMain', 'Hmain:') !!}
    <p>{!! $hotel->hMain !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $hotel->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $hotel->updated_at !!}</p>
</div>

