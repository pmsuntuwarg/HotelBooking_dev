@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Hotel
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($hotel, ['route' => ['admin.hotels.update', $hotel->hHotelID], 'method' => 'patch']) !!}

                        @include('admin.hotels.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection