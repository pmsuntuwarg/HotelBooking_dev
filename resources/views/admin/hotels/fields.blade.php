<!-- Hhotelcode Field -->
<div class="form-group col-sm-6">
    {!! Form::label('hHotelCode', 'Hhotelcode:') !!}
    {!! Form::text('hHotelCode', null, ['class' => 'form-control']) !!}
</div>

<!-- Hname Field -->
<div class="form-group col-sm-6">
    {!! Form::label('hName', 'Hname:') !!}
    {!! Form::text('hName', null, ['class' => 'form-control']) !!}
</div>

<!-- Hmotto Field -->
<div class="form-group col-sm-6">
    {!! Form::label('hMotto', 'Hmotto:') !!}
    {!! Form::text('hMotto', null, ['class' => 'form-control']) !!}
</div>

<!-- Haddress Field -->
<div class="form-group col-sm-6">
    {!! Form::label('hAddress', 'Haddress:') !!}
    {!! Form::text('hAddress', null, ['class' => 'form-control']) !!}
</div>

<!-- Haddress2 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('hAddress2', 'Haddress2:') !!}
    {!! Form::text('hAddress2', null, ['class' => 'form-control']) !!}
</div>

<!-- Hcity Field -->
<div class="form-group col-sm-6">
    {!! Form::label('hCity', 'Hcity:') !!}
    {!! Form::text('hCity', null, ['class' => 'form-control']) !!}
</div>

<!-- Hstate Field -->
<div class="form-group col-sm-6">
    {!! Form::label('hState', 'Hstate:') !!}
    {!! Form::text('hState', null, ['class' => 'form-control']) !!}
</div>

<!-- Hzipcode Field -->
<div class="form-group col-sm-6">
    {!! Form::label('hZipCode', 'Hzipcode:') !!}
    {!! Form::text('hZipCode', null, ['class' => 'form-control']) !!}
</div>

<!-- Hmainphonenumber Field -->
<div class="form-group col-sm-6">
    {!! Form::label('hMainPhoneNumber', 'Hmainphonenumber:') !!}
    {!! Form::text('hMainPhoneNumber', null, ['class' => 'form-control']) !!}
</div>

<!-- Hfaxnumber Field -->
<div class="form-group col-sm-6">
    {!! Form::label('hFaxNumber', 'Hfaxnumber:') !!}
    {!! Form::text('hFaxNumber', null, ['class' => 'form-control']) !!}
</div>

<!-- Htollfreenumber Field -->
<div class="form-group col-sm-6">
    {!! Form::label('hTollFreeNumber', 'Htollfreenumber:') !!}
    {!! Form::text('hTollFreeNumber', null, ['class' => 'form-control']) !!}
</div>

<!-- Hcompanymailingaddress Field -->
<div class="form-group col-sm-6">
    {!! Form::label('hCompanyMailingAddress', 'Hcompanymailingaddress:') !!}
    {!! Form::email('hCompanyMailingAddress', null, ['class' => 'form-control']) !!}
</div>

<!-- Hwebsiteaddress Field -->
<div class="form-group col-sm-6">
    {!! Form::label('hWebsiteAddress', 'Hwebsiteaddress:') !!}
    {!! Form::text('hWebsiteAddress', null, ['class' => 'form-control']) !!}
</div>

<!-- Hmain Field -->
<div class="form-group col-sm-6">
    {!! Form::label('hMain', 'Hmain:') !!}
    {!! Form::text('hMain', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.hotels.index') !!}" class="btn btn-default">Cancel</a>
</div>
