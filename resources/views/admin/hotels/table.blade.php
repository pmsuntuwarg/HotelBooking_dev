<table class="table table-responsive" id="hotels-table">
    <thead>
        <tr>
            <th>Hhotelcode</th>
        <th>Hname</th>
        <th>Hmotto</th>
        <th>Haddress</th>
        <th>Haddress2</th>
        <th>Hcity</th>
        <th>Hstate</th>
        <th>Hzipcode</th>
        <th>Hmainphonenumber</th>
        <th>Hfaxnumber</th>
        <th>Htollfreenumber</th>
        <th>Hcompanymailingaddress</th>
        <th>Hwebsiteaddress</th>
        <th>Hmain</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($hotels as $hotel)
        <tr>
            <td>{!! $hotel->hHotelCode !!}</td>
            <td>{!! $hotel->hName !!}</td>
            <td>{!! $hotel->hMotto !!}</td>
            <td>{!! $hotel->hAddress !!}</td>
            <td>{!! $hotel->hAddress2 !!}</td>
            <td>{!! $hotel->hCity !!}</td>
            <td>{!! $hotel->hState !!}</td>
            <td>{!! $hotel->hZipCode !!}</td>
            <td>{!! $hotel->hMainPhoneNumber !!}</td>
            <td>{!! $hotel->hFaxNumber !!}</td>
            <td>{!! $hotel->hTollFreeNumber !!}</td>
            <td>{!! $hotel->hCompanyMailingAddress !!}</td>
            <td>{!! $hotel->hWebsiteAddress !!}</td>
            <td>{!! $hotel->hMain !!}</td>
            <td>
                {!! Form::open(['route' => ['admin.hotels.destroy', $hotel->hHotelID], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('admin.hotels.show', [$hotel->hHotelID]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('admin.hotels.edit', [$hotel->hHotelID]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>