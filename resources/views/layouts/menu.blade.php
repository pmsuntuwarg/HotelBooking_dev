<li class="{{ Request::is('hotels*') ? 'active' : '' }}">
    <a href="{!! route('admin.hotels.index') !!}"><i class="fa fa-edit"></i><span>Hotels</span></a>
</li>

<li class="{{ Request::is('bookingStatuses*') ? 'active' : '' }}">
    <a href="{!! route('admin.bookingStatuses.index') !!}"><i class="fa fa-edit"></i><span>Booking Statuses</span></a>
</li>

<li class="{{ Request::is('reservationAgents*') ? 'active' : '' }}">
    <a href="{!! route('admin.reservationAgents.index') !!}"><i class="fa fa-edit"></i><span>Reservation Agents</span></a>
</li>

<li class="{{ Request::is('guests*') ? 'active' : '' }}">
    <a href="{!! route('admin.guests.index') !!}"><i class="fa fa-edit"></i><span>Guests</span></a>
</li>

<li class="{{ Request::is('roomTypes*') ? 'active' : '' }}">
    <a href="{!! route('admin.roomTypes.index') !!}"><i class="fa fa-edit"></i><span>Room Types</span></a>
</li>

<li class="{{ Request::is('roomStatuses*') ? 'active' : '' }}">
    <a href="{!! route('admin.roomStatuses.index') !!}"><i class="fa fa-edit"></i><span>Room Statuses</span></a>
</li>

<li class="{{ Request::is('paymentStatuses*') ? 'active' : '' }}">
    <a href="{!! route('admin.paymentStatuses.index') !!}"><i class="fa fa-edit"></i><span>Payment Statuses</span></a>
</li>

<li class="{{ Request::is('paymentTypes*') ? 'active' : '' }}">
    <a href="{!! route('admin.paymentTypes.index') !!}"><i class="fa fa-edit"></i><span>Payment Types</span></a>
</li>

<li class="{{ Request::is('rateTypes*') ? 'active' : '' }}">
    <a href="{!! route('admin.rateTypes.index') !!}"><i class="fa fa-edit"></i><span>Rate Types</span></a>
</li>

<li class="{{ Request::is('positions*') ? 'active' : '' }}">
    <a href="{!! route('admin.positions.index') !!}"><i class="fa fa-edit"></i><span>Positions</span></a>
</li>

<li class="{{ Request::is('staff*') ? 'active' : '' }}">
    <a href="{!! route('admin.staff.index') !!}"><i class="fa fa-edit"></i><span>Staff</span></a>
</li><li class="{{ Request::is('rooms*') ? 'active' : '' }}">
    <a href="{!! route('admin.rooms.index') !!}"><i class="fa fa-edit"></i><span>Rooms</span></a>
</li>

<li class="{{ Request::is('rates*') ? 'active' : '' }}">
    <a href="{!! route('admin.rates.index') !!}"><i class="fa fa-edit"></i><span>Rates</span></a>
</li>

<li class="{{ Request::is('roomBookeds*') ? 'active' : '' }}">
    <a href="{!! route('admin.roomBookeds.index') !!}"><i class="fa fa-edit"></i><span>Room Bookeds</span></a>
</li>

<li class="{{ Request::is('bookings*') ? 'active' : '' }}">
    <a href="{!! route('admin.bookings.index') !!}"><i class="fa fa-edit"></i><span>Bookings</span></a>
</li>

